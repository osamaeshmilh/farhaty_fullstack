package ly.com.farhaty.config;

import ly.com.farhaty.service.util.JasperReportsUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JaperReportsConfig {

	@Bean
	public JasperReportsUtil jasperReportsUtil() {
		return new JasperReportsUtil("reports/");
	}
}
