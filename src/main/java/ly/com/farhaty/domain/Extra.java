package ly.com.farhaty.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Extra.
 */
@Entity
@Table(name = "extra")
public class Extra implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "price")
    private Float price;

    @NotNull
    @Column(name = "active", nullable = false)
    private Boolean active;

    @ManyToOne(optional = false)
    @JsonIgnoreProperties("extras")
    private Hall hall;

    @ManyToMany(mappedBy = "extras")
    @JsonIgnore
    private Set<Reservation> extras = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Extra name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public Extra price(Float price) {
        this.price = price;
        return this;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Boolean isActive() {
        return active;
    }

    public Extra active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Hall getHall() {
        return hall;
    }

    public Extra hall(Hall hall) {
        this.hall = hall;
        return this;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Set<Reservation> getExtras() {
        return extras;
    }

    public Extra extras(Set<Reservation> reservations) {
        this.extras = reservations;
        return this;
    }

    public Extra addExtras(Reservation reservation) {
        this.extras.add(reservation);
        reservation.getExtras().add(this);
        return this;
    }

    public Extra removeExtras(Reservation reservation) {
        this.extras.remove(reservation);
        reservation.getExtras().remove(this);
        return this;
    }

    public void setExtras(Set<Reservation> reservations) {
        this.extras = reservations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Extra)) {
            return false;
        }
        return id != null && id.equals(((Extra) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Extra{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", price=" + getPrice() +
            ", active='" + isActive() + "'" +
            "}";
    }
}
