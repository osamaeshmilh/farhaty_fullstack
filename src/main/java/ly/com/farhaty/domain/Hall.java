package ly.com.farhaty.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * A Hall.
 */
@Entity
@Table(name = "hall")
public class Hall extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "address", nullable = false)
    private String address;

    @NotNull
    @Column(name = "phone", nullable = false)
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "facebook")
    private String facebook;

    @Column(name = "whatsup")
    private String whatsup;

    @Column(name = "viper")
    private String viper;

    @Column(name = "twitter")
    private String twitter;

    @Column(name = "instagram")
    private String instagram;

    @Column(name = "snapchat")
    private String snapchat;

    @Column(name = "lat")
    private Float lat;

    @Column(name = "lng")
    private Float lng;

    @Lob
    @Column(name = "rules")
    private String rules;

    @Column(name = "vip")
    private Boolean vip;

    @Column(name = "logo_url")
    private String logoUrl;

    @Lob
    @Column(name = "logo")
    private byte[] logo;

    @Column(name = "logo_content_type")
    private String logoContentType;

    @Column(name = "available_from")
    private LocalDate availableFrom;

    @Column(name = "available_to")
    private LocalDate availableTo;

    @Column(name = "pending_appointment_period")
    private Integer pendingAppointmentPeriod;

    @Column(name = "two_reservations")
    private Boolean twoReservations;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "view_count")
    private Integer viewCount;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "hall", fetch = FetchType.EAGER)
    private Set<Reservation> reservations = new HashSet<>();

    @OneToMany(mappedBy = "hall" , fetch = FetchType.EAGER)
    private Set<Type> types = new HashSet<>();

    @OneToMany(mappedBy = "hall", fetch = FetchType.EAGER)
    private Set<Room> rooms = new HashSet<>();

    @OneToMany(mappedBy = "hall", fetch = FetchType.EAGER)
    private Set<Extra> extras = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Hall name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public Hall address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public Hall phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public Hall email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Float getLat() {
        return lat;
    }

    public Hall lat(Float lat) {
        this.lat = lat;
        return this;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public Hall lng(Float lng) {
        this.lng = lng;
        return this;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public String getRules() {
        return rules;
    }

    public Hall rules(String rules) {
        this.rules = rules;
        return this;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public Boolean isVip() {
        return vip;
    }

    public Hall vip(Boolean vip) {
        this.vip = vip;
        return this;
    }

    public void setVip(Boolean vip) {
        this.vip = vip;
    }

    public User getUser() {
        return user;
    }

    public Hall user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.reservations = reservations;
    }

    public Hall appointments(Set<Reservation> reservations) {
        this.reservations = reservations;
        return this;
    }

    public Hall addAppointment(Reservation reservation) {
        this.reservations.add(reservation);
        reservation.setHall(this);
        return this;
    }

    public Hall removeAppointment(Reservation reservation) {
        this.reservations.remove(reservation);
        reservation.setHall(null);
        return this;
    }


    public Set<Type> getTypes() {
        return types;
    }

    public Hall types(Set<Type> types) {
        this.types = types;
        return this;
    }

    public Hall addType(Type type) {
        this.types.add(type);
        type.setHall(this);
        return this;
    }

    public Hall removeType(Type type) {
        this.types.remove(type);
        type.setHall(null);
        return this;
    }

    public void setTypes(Set<Type> types) {
        this.types = types;
    }

    public Set<Room> getRooms() {
        return rooms;
    }

    public Hall rooms(Set<Room> rooms) {
        this.rooms = rooms;
        return this;
    }

    public Hall addRoom(Room room) {
        this.rooms.add(room);
        room.setHall(this);
        return this;
    }

    public Hall removeRoom(Room room) {
        this.rooms.remove(room);
        room.setHall(null);
        return this;
    }

    public void setRooms(Set<Room> rooms) {
        this.rooms = rooms;
    }

    public Set<Extra> getExtras() {
        return extras;
    }

    public Hall extras(Set<Extra> extras) {
        this.extras = extras;
        return this;
    }

    public Hall addExtra(Extra extra) {
        this.extras.add(extra);
        extra.setHall(this);
        return this;
    }

    public Hall removeExtra(Extra extra) {
        this.extras.remove(extra);
        extra.setHall(null);
        return this;
    }

    public void setExtras(Set<Extra> extras) {
        this.extras = extras;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getLogoContentType() {
        return logoContentType;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }

    public LocalDate getAvailableFrom() {
        return availableFrom;
    }

    public void setAvailableFrom(LocalDate availableFrom) {
        this.availableFrom = availableFrom;
    }

    public LocalDate getAvailableTo() {
        return availableTo;
    }

    public void setAvailableTo(LocalDate availableTo) {
        this.availableTo = availableTo;
    }

    public Integer getPendingAppointmentPeriod() {
        return pendingAppointmentPeriod;
    }

    public void setPendingAppointmentPeriod(Integer pendingAppointmentPeriod) {
        this.pendingAppointmentPeriod = pendingAppointmentPeriod;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getWhatsup() {
        return whatsup;
    }

    public void setWhatsup(String whatsup) {
        this.whatsup = whatsup;
    }

    public String getViper() {
        return viper;
    }

    public void setViper(String viper) {
        this.viper = viper;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getSnapchat() {
        return snapchat;
    }

    public void setSnapchat(String snapchat) {
        this.snapchat = snapchat;
    }

    public Boolean getTwoReservations() {
        return twoReservations;
    }

    public void setTwoReservations(Boolean twoReservations) {
        this.twoReservations = twoReservations;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Hall)) {
            return false;
        }
        return id != null && id.equals(((Hall) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Hall{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", address='" + address + '\'' +
            ", phone='" + phone + '\'' +
            ", email='" + email + '\'' +
            ", lat=" + lat +
            ", lng=" + lng +
            ", rules='" + rules + '\'' +
            ", vip=" + vip +
            ", logo=" + Arrays.toString(logo) +
            ", logoContentType='" + logoContentType + '\'' +
            ", availableFrom=" + availableFrom +
            ", availableTo=" + availableTo +
            ", pendingAppointmentPeriod=" + pendingAppointmentPeriod +
            ", active=" + active +
            ", user=" + user +
            ", reservations=" + reservations +
            ", types=" + types.toString() +
            ", rooms=" + rooms +
            ", extras=" + extras +
            '}';
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
