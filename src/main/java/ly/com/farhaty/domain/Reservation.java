package ly.com.farhaty.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import ly.com.farhaty.domain.enumeration.AttendeesType;

import ly.com.farhaty.domain.enumeration.Period;
import ly.com.farhaty.domain.enumeration.ReservationStatus;

/**
 * A Reservation.
 */
@Entity
@Table(name = "reservation")
public class Reservation extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "attendees_type")
    private AttendeesType attendeesType;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "reservation_status", nullable = false)
    private ReservationStatus reservationStatus;

    @Column(name = "attendees_no")
    private Integer attendeesNo;

    @Column(name = "total")
    private Float total;

    @Column(name = "room_total")
    private Float roomTotal;

    @Column(name = "type_total")
    private Float typeTotal;

    @Column(name = "extras_total")
    private Float extrasTotal;

    @Column(name = "notes")
    private String notes;

    @Column(name = "customer_name")
    private String customerName;

    @Column(name = "customer_phone")
    private String customerPhone;

    @Column(name = "reservation_date")
    private LocalDate reservationDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "period")
    private Period period;

    @OneToMany(mappedBy = "reservation", fetch = FetchType.EAGER)
    private Set<Invoice> invoices = new HashSet<>();

    @ManyToOne()
    @JsonIgnoreProperties("reservations")
    private User user;

    @ManyToMany
    @JoinTable(name = "reservation_extra",
        joinColumns = @JoinColumn(name = "reservation_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "extra_id", referencedColumnName = "id"))
    private Set<Extra> extras = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("reservations")
    private Type type;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("reservations")
    private Hall hall;

    @ManyToOne()
    @JsonIgnoreProperties("reservations")
    private Room room;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AttendeesType getAttendeesType() {
        return attendeesType;
    }

    public Reservation attendeesType(AttendeesType attendeesType) {
        this.attendeesType = attendeesType;
        return this;
    }

    public void setAttendeesType(AttendeesType attendeesType) {
        this.attendeesType = attendeesType;
    }

    public ReservationStatus getReservationStatus() {
        return reservationStatus;
    }

    public Reservation reservationStatus(ReservationStatus reservationStatus) {
        this.reservationStatus = reservationStatus;
        return this;
    }

    public void setReservationStatus(ReservationStatus reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public Integer getAttendeesNo() {
        return attendeesNo;
    }

    public Reservation attendeesNo(Integer attendeesNo) {
        this.attendeesNo = attendeesNo;
        return this;
    }

    public void setAttendeesNo(Integer attendeesNo) {
        this.attendeesNo = attendeesNo;
    }

    public Float getTotal() {
        return total;
    }

    public Reservation total(Float total) {
        this.total = total;
        return this;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public String getNotes() {
        return notes;
    }

    public Reservation notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


    public Set<Invoice> getInvoices() {
        return invoices;
    }

    public Reservation invoices(Set<Invoice> invoices) {
        this.invoices = invoices;
        return this;
    }

    public Reservation addInvoice(Invoice invoice) {
        this.invoices.add(invoice);
        invoice.setReservation(this);
        return this;
    }

    public Reservation removeInvoice(Invoice invoice) {
        this.invoices.remove(invoice);
        invoice.setReservation(null);
        return this;
    }

    public void setInvoices(Set<Invoice> invoices) {
        this.invoices = invoices;
    }

    public User getUser() {
        return user;
    }

    public Reservation user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Extra> getExtras() {
        return extras;
    }

    public Reservation extras(Set<Extra> extras) {
        this.extras = extras;
        return this;
    }

    public Reservation addExtra(Extra extra) {
        this.extras.add(extra);
        extra.getExtras().add(this);
        return this;
    }

    public Reservation removeExtra(Extra extra) {
        this.extras.remove(extra);
        extra.getExtras().remove(this);
        return this;
    }

    public void setExtras(Set<Extra> extras) {
        this.extras = extras;
    }

    public Type getType() {
        return type;
    }

    public Reservation type(Type type) {
        this.type = type;
        return this;
    }

    public LocalDate getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(LocalDate reservationDate) {
        this.reservationDate = reservationDate;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Hall getHall() {
        return hall;
    }

    public Reservation hall(Hall hall) {
        this.hall = hall;
        return this;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Room getRoom() {
        return room;
    }

    public Reservation room(Room room) {
        this.room = room;
        return this;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Float getRoomTotal() {
        return roomTotal;
    }

    public void setRoomTotal(Float roomTotal) {
        this.roomTotal = roomTotal;
    }

    public Float getTypeTotal() {
        return typeTotal;
    }

    public void setTypeTotal(Float typeTotal) {
        this.typeTotal = typeTotal;
    }

    public Float getExtrasTotal() {
        return extrasTotal;
    }

    public void setExtrasTotal(Float extrasTotal) {
        this.extrasTotal = extrasTotal;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Reservation)) {
            return false;
        }
        return id != null && id.equals(((Reservation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Reservation{" +
            "id=" + getId() +
            ", attendeesType='" + getAttendeesType() + "'" +
            ", reservationStatus='" + getReservationStatus() + "'" +
            ", attendeesNo=" + getAttendeesNo() +
            ", total=" + getTotal() +
            ", notes='" + getNotes() + "'" +
            "}";
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }
}
