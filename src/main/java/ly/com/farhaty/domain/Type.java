package ly.com.farhaty.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Type.
 */
@Entity
@Table(name = "type")
public class Type implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "sat_price")
    private Float satPrice;

    @Column(name = "sun_price")
    private Float sunPrice;

    @Column(name = "mon_price")
    private Float monPrice;

    @Column(name = "tue_price")
    private Float tuePrice;

    @Column(name = "wed_price")
    private Float wedPrice;

    @Column(name = "thr_price")
    private Float thrPrice;

    @Column(name = "fri_price")
    private Float friPrice;

    @NotNull
    @Column(name = "active", nullable = false)
    private Boolean active;

    @ManyToOne(optional = false)
    @JsonIgnoreProperties("types")
    private Hall hall;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Type name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isActive() {
        return active;
    }

    public Type active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Hall getHall() {
        return hall;
    }

    public Type hall(Hall hall) {
        this.hall = hall;
        return this;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Float getSatPrice() {
        return satPrice;
    }

    public void setSatPrice(Float satPrice) {
        this.satPrice = satPrice;
    }

    public Float getSunPrice() {
        return sunPrice;
    }

    public void setSunPrice(Float sunPrice) {
        this.sunPrice = sunPrice;
    }

    public Float getMonPrice() {
        return monPrice;
    }

    public void setMonPrice(Float monPrice) {
        this.monPrice = monPrice;
    }

    public Float getTuePrice() {
        return tuePrice;
    }

    public void setTuePrice(Float tuePrice) {
        this.tuePrice = tuePrice;
    }

    public Float getWedPrice() {
        return wedPrice;
    }

    public void setWedPrice(Float wedPrice) {
        this.wedPrice = wedPrice;
    }

    public Float getThrPrice() {
        return thrPrice;
    }

    public void setThrPrice(Float thrPrice) {
        this.thrPrice = thrPrice;
    }

    public Float getFriPrice() {
        return friPrice;
    }

    public void setFriPrice(Float friPrice) {
        this.friPrice = friPrice;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Type)) {
            return false;
        }
        return id != null && id.equals(((Type) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Type{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", active='" + isActive() + "'" +
            "}";
    }
}
