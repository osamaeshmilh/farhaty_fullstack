package ly.com.farhaty.domain.enumeration;

/**
 * The AttendeesType enumeration.
 */
public enum AttendeesType {
    MEN, WOMEN, ALL
}
