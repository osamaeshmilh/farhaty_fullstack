package ly.com.farhaty.domain.enumeration;

/**
 * The Currency enumeration.
 */
public enum Currency {
    LYD, USD
}
