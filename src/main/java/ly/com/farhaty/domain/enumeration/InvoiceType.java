package ly.com.farhaty.domain.enumeration;

/**
 * The InvoiceType enumeration.
 */
public enum InvoiceType {
    FULL_PAYMENT, PARTIAL_PAYMENT, RETURN, GUARANTEE
}
