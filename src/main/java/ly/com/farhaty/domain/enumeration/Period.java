package ly.com.farhaty.domain.enumeration;

/**
 * The Period enumeration.
 */
public enum Period {
    MORNING, EVENING, FULL_DAY
}
