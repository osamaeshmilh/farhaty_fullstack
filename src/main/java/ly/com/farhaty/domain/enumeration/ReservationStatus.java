package ly.com.farhaty.domain.enumeration;

/**
 * The ReservationStatus enumeration.
 */
public enum ReservationStatus {
    PENDING, APPROVED, DONE, CANCELED_BY_USER, CANCELED_BY_HALL
}
