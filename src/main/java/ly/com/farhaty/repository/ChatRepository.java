package ly.com.farhaty.repository;

import ly.com.farhaty.domain.Chat;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Chat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChatRepository extends JpaRepository<Chat, Long>, JpaSpecificationExecutor<Chat> {

    @Query("select chat from Chat chat where chat.user.login = ?#{principal.username}")
    List<Chat> findByUserIsCurrentUser();

}
