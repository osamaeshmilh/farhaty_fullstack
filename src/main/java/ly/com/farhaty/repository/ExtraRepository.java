package ly.com.farhaty.repository;
import ly.com.farhaty.domain.Extra;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.stream.DoubleStream;


/**
 * Spring Data  repository for the Extra entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExtraRepository extends JpaRepository<Extra, Long>, JpaSpecificationExecutor<Extra> {

    Page<Extra> findAllByHallUserId(Long hall_user_id, Pageable pageable);
}
