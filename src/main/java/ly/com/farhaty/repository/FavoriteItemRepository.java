package ly.com.farhaty.repository;
import ly.com.farhaty.domain.FavoriteItem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the FavoriteItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FavoriteItemRepository extends JpaRepository<FavoriteItem, Long>, JpaSpecificationExecutor<FavoriteItem> {

    @Query("select favoriteItem from FavoriteItem favoriteItem where favoriteItem.user.login = ?#{principal.username}")
    List<FavoriteItem> findByUserIsCurrentUser();

}
