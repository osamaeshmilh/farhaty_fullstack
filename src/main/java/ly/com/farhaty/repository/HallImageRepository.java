package ly.com.farhaty.repository;
import ly.com.farhaty.domain.HallImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.stream.DoubleStream;


/**
 * Spring Data  repository for the HallImage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HallImageRepository extends JpaRepository<HallImage, Long>, JpaSpecificationExecutor<HallImage> {

    Page<HallImage> findAllByHallUserId(Long hall_user_id, Pageable pageable);

}
