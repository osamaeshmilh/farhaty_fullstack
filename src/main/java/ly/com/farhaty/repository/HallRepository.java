package ly.com.farhaty.repository;
import ly.com.farhaty.domain.Hall;
import ly.com.farhaty.domain.Store;
import ly.com.farhaty.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.stream.DoubleStream;


/**
 * Spring Data  repository for the Hall entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HallRepository extends JpaRepository<Hall, Long>, JpaSpecificationExecutor<Hall> {

    Hall findByUser(User user);

    Page<Hall> findAllByUserId(Long user_id, Pageable pageable);

    Page<Hall> findAllByAvailableFromGreaterThanEqualAndAvailableToLessThanEqual(LocalDate availableFrom, LocalDate availableTo, Pageable pageable);
}
