package ly.com.farhaty.repository;
import ly.com.farhaty.domain.Item;
import ly.com.farhaty.domain.StoreImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Item entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ItemRepository extends JpaRepository<Item, Long>, JpaSpecificationExecutor<Item> {

    Page<Item> findAllByStoreUserId(Long store_user_id, Pageable pageable);

}
