package ly.com.farhaty.repository;
import ly.com.farhaty.domain.Reservation;
import ly.com.farhaty.domain.User;
import ly.com.farhaty.domain.enumeration.ReservationStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.DoubleStream;

/**
 * Spring Data  repository for the Reservation entity.
 */
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long>, JpaSpecificationExecutor<Reservation> {

    @Query("select reservation from Reservation reservation where reservation.user.login = ?#{principal.username}")
    List<Reservation> findByUserIsCurrentUser();

    List<Reservation> findByUserIdAndReservationStatus(Long user_id, @NotNull ReservationStatus reservationStatus);

    @Query(value = "select distinct reservation from Reservation reservation left join fetch reservation.extras",
        countQuery = "select count(distinct reservation) from Reservation reservation")
    Page<Reservation> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct reservation from Reservation reservation left join fetch reservation.extras")
    List<Reservation> findAllWithEagerRelationships();

    @Query("select reservation from Reservation reservation left join fetch reservation.extras where reservation.id =:id")
    Optional<Reservation> findOneWithEagerRelationships(@Param("id") Long id);

    Page<Reservation> findByHallUserId(Long hall_user_id, Pageable pageable);

    Long countByHallIdAndReservationDateEquals(Long hall_id, LocalDate reservationDate);

    @Query(value = "select * from reservation where reservation_date = :reservationDate and hall_id = :hallId and (reservation_status = 'PENDING' or reservation_status = 'APPROVED')", nativeQuery=true)
    List<Reservation> findByHallIdAndReservationDate(@Param("hallId")Long hallId, @Param("reservationDate") String reservationDate);

    @Query(value = "select * from reservation where reservation_date = :reservationDate and room_id = :roomId and (reservation_status = 'PENDING' or reservation_status = 'APPROVED')", nativeQuery=true)
    List<Reservation> findByRoomIdAndReservationDate(@Param("roomId")Long roomId, @Param("reservationDate") String reservationDate);

    Page<Reservation> findByHallId(Long hallId, Pageable pageable);

    Page<Reservation> findByHallIdAndRoomId(Long hall_id, Long room_id, Pageable pageable);

    @Query(value = "select reservation_date AS 'reservationDate', COUNT(id) as 'count' from reservation where hall_id = :hallId and (reservation_status = 'PENDING' or reservation_status = 'APPROVED') group by reservation_date", nativeQuery=true)
    List<Object[]> datesByHallId(@Param("hallId")Long hallId);

    @Query(value = "select reservation_date AS 'reservationDate', COUNT(id) as 'count' from reservation where hall_id = :hallId and room_id = :roomId and (reservation_status = 'PENDING' or reservation_status = 'APPROVED') group by reservation_date", nativeQuery=true)
    List<Object[]> datesByHallIdAndRoomId(@Param("hallId")Long hallId, @Param("roomId")Long roomId);

    Long countByHallId(Long hallId);

    List<Reservation> findAllByHallId(Long hallId);

    List<Reservation> findAllByReservationStatusAndCreatedDateBefore(@NotNull ReservationStatus reservationStatus, Instant createdDate);

    List<Reservation> findAllByHallIdAndReservationDateBetween(Long reservationId, LocalDate from,  LocalDate to);


}
