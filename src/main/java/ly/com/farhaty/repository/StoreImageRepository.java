package ly.com.farhaty.repository;
import ly.com.farhaty.domain.StoreImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.stream.DoubleStream;


/**
 * Spring Data  repository for the StoreImage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StoreImageRepository extends JpaRepository<StoreImage, Long>, JpaSpecificationExecutor<StoreImage> {

    Page<StoreImage> findAllByStoreUserId(Long store_user_id, Pageable pageable);
}
