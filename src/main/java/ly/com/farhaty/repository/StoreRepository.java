package ly.com.farhaty.repository;
import ly.com.farhaty.domain.Store;
import ly.com.farhaty.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.stream.DoubleStream;


/**
 * Spring Data  repository for the Store entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StoreRepository extends JpaRepository<Store, Long>, JpaSpecificationExecutor<Store> {


    Store findByUser(User user);

    Page<Store> findAllByUserId(Long user_id, Pageable pageable);
}
