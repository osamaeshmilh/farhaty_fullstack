package ly.com.farhaty.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String MANAGER = "ROLE_MANAGER";

    public static final String USER = "ROLE_USER";

    public static final String STORE = "ROLE_STORE";

    public static final String HALL = "ROLE_HALL";

    public static final String CUSTOMER = "ROLE_CUSTOMER";
    public static final String ADD_HALL = "ROLE_ADD_HALL";
    public static final String ADD_STORE = "ROLE_ADD_STORE";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {
    }
}
