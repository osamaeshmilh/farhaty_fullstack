package ly.com.farhaty.service;

import ly.com.farhaty.domain.Extra;
import ly.com.farhaty.repository.ExtraRepository;
import ly.com.farhaty.service.dto.ExtraDTO;
import ly.com.farhaty.service.mapper.ExtraMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Extra}.
 */
@Service
@Transactional
public class ExtraService {

    private final Logger log = LoggerFactory.getLogger(ExtraService.class);

    private final ExtraRepository extraRepository;

    private final ExtraMapper extraMapper;

    private final HallService hallService;

    public ExtraService(ExtraRepository extraRepository, ExtraMapper extraMapper, HallService hallService) {
        this.extraRepository = extraRepository;
        this.extraMapper = extraMapper;
        this.hallService = hallService;
    }

    /**
     * Save a extra.
     *
     * @param extraDTO the entity to save.
     * @return the persisted entity.
     */
    public ExtraDTO save(ExtraDTO extraDTO) {
        log.debug("Request to save Extra : {}", extraDTO);
        Extra extra = extraMapper.toEntity(extraDTO);
        extra.setHall(hallService.findOneByUser());
        extra = extraRepository.save(extra);
        return extraMapper.toDto(extra);
    }

    /**
     * Get all the extras.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExtraDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Extras");
        return extraRepository.findAll(pageable)
            .map(extraMapper::toDto);
    }


    /**
     * Get one extra by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExtraDTO> findOne(Long id) {
        log.debug("Request to get Extra : {}", id);
        return extraRepository.findById(id)
            .map(extraMapper::toDto);
    }

    /**
     * Delete the extra by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Extra : {}", id);
        extraRepository.deleteById(id);
    }

    public Page<ExtraDTO> findByUser(Long userId, Pageable pageable) {
        return extraRepository.findAllByHallUserId(userId, pageable).map(extraMapper::toDto);

    }
}
