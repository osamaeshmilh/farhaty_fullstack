package ly.com.farhaty.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ly.com.farhaty.domain.FavoriteItem;
import ly.com.farhaty.domain.*; // for static metamodels
import ly.com.farhaty.repository.FavoriteItemRepository;
import ly.com.farhaty.service.dto.FavoriteItemCriteria;
import ly.com.farhaty.service.dto.FavoriteItemDTO;
import ly.com.farhaty.service.mapper.FavoriteItemMapper;

/**
 * Service for executing complex queries for {@link FavoriteItem} entities in the database.
 * The main input is a {@link FavoriteItemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FavoriteItemDTO} or a {@link Page} of {@link FavoriteItemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FavoriteItemQueryService extends QueryService<FavoriteItem> {

    private final Logger log = LoggerFactory.getLogger(FavoriteItemQueryService.class);

    private final FavoriteItemRepository favoriteItemRepository;

    private final FavoriteItemMapper favoriteItemMapper;

    public FavoriteItemQueryService(FavoriteItemRepository favoriteItemRepository, FavoriteItemMapper favoriteItemMapper) {
        this.favoriteItemRepository = favoriteItemRepository;
        this.favoriteItemMapper = favoriteItemMapper;
    }

    /**
     * Return a {@link List} of {@link FavoriteItemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FavoriteItemDTO> findByCriteria(FavoriteItemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FavoriteItem> specification = createSpecification(criteria);
        return favoriteItemMapper.toDto(favoriteItemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link FavoriteItemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FavoriteItemDTO> findByCriteria(FavoriteItemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FavoriteItem> specification = createSpecification(criteria);
        return favoriteItemRepository.findAll(specification, page)
            .map(favoriteItemMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FavoriteItemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FavoriteItem> specification = createSpecification(criteria);
        return favoriteItemRepository.count(specification);
    }

    /**
     * Function to convert {@link FavoriteItemCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<FavoriteItem> createSpecification(FavoriteItemCriteria criteria) {
        Specification<FavoriteItem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), FavoriteItem_.id));
            }
            if (criteria.getItemId() != null) {
                specification = specification.and(buildSpecification(criteria.getItemId(),
                    root -> root.join(FavoriteItem_.item, JoinType.LEFT).get(Item_.id)));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(FavoriteItem_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
