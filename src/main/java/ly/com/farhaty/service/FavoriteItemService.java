package ly.com.farhaty.service;

import ly.com.farhaty.domain.FavoriteItem;
import ly.com.farhaty.repository.FavoriteItemRepository;
import ly.com.farhaty.service.dto.FavoriteItemDTO;
import ly.com.farhaty.service.mapper.FavoriteItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link FavoriteItem}.
 */
@Service
@Transactional
public class FavoriteItemService {

    private final Logger log = LoggerFactory.getLogger(FavoriteItemService.class);

    private final FavoriteItemRepository favoriteItemRepository;

    private final FavoriteItemMapper favoriteItemMapper;

    public FavoriteItemService(FavoriteItemRepository favoriteItemRepository, FavoriteItemMapper favoriteItemMapper) {
        this.favoriteItemRepository = favoriteItemRepository;
        this.favoriteItemMapper = favoriteItemMapper;
    }

    /**
     * Save a favoriteItem.
     *
     * @param favoriteItemDTO the entity to save.
     * @return the persisted entity.
     */
    public FavoriteItemDTO save(FavoriteItemDTO favoriteItemDTO) {
        log.debug("Request to save FavoriteItem : {}", favoriteItemDTO);
        FavoriteItem favoriteItem = favoriteItemMapper.toEntity(favoriteItemDTO);
        favoriteItem = favoriteItemRepository.save(favoriteItem);
        return favoriteItemMapper.toDto(favoriteItem);
    }

    /**
     * Get all the favoriteItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FavoriteItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FavoriteItems");
        return favoriteItemRepository.findAll(pageable)
            .map(favoriteItemMapper::toDto);
    }


    /**
     * Get one favoriteItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FavoriteItemDTO> findOne(Long id) {
        log.debug("Request to get FavoriteItem : {}", id);
        return favoriteItemRepository.findById(id)
            .map(favoriteItemMapper::toDto);
    }

    /**
     * Delete the favoriteItem by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete FavoriteItem : {}", id);
        favoriteItemRepository.deleteById(id);
    }
}
