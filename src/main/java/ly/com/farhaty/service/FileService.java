package ly.com.farhaty.service;

import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Random;

@Service
public class FileService {
    private final String uploadsDir = "/Users/osamaeshmilh/uploads/";


    public String upload(byte[] imageBytes, String fileContentType, String name) {
        String generatedName = "";

        if (imageBytes != null) {
            try {
                if(fileContentType.equals(MediaType.IMAGE_PNG_VALUE))
                    generatedName = name +"_"+ System.currentTimeMillis() + new Random().nextInt(100) + ".png";
                else
                    generatedName = name +"_"+ System.currentTimeMillis() + new Random().nextInt(100) + ".jpg";
                String path = uploadsDir + generatedName;
                FileUtils.writeByteArrayToFile(new File(path), imageBytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return generatedName;
    }

    public byte[] download(String fileName) {
        try {
            File file = new File(uploadsDir + fileName);
            return Files.toByteArray(file);
        } catch (IOException e) {
            e.printStackTrace();
            return new byte[0];
        }
    }
}
