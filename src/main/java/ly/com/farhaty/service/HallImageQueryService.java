package ly.com.farhaty.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import ly.com.farhaty.service.dto.HallImageAllDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ly.com.farhaty.domain.HallImage;
import ly.com.farhaty.domain.*; // for static metamodels
import ly.com.farhaty.repository.HallImageRepository;
import ly.com.farhaty.service.dto.HallImageCriteria;
import ly.com.farhaty.service.dto.HallImageDTO;
import ly.com.farhaty.service.mapper.HallImageMapper;

/**
 * Service for executing complex queries for {@link HallImage} entities in the database.
 * The main input is a {@link HallImageCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link HallImageDTO} or a {@link Page} of {@link HallImageDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class HallImageQueryService extends QueryService<HallImage> {

    private final Logger log = LoggerFactory.getLogger(HallImageQueryService.class);

    private final HallImageRepository hallImageRepository;

    private final HallImageMapper hallImageMapper;

    public HallImageQueryService(HallImageRepository hallImageRepository, HallImageMapper hallImageMapper) {
        this.hallImageRepository = hallImageRepository;
        this.hallImageMapper = hallImageMapper;
    }

    /**
     * Return a {@link List} of {@link HallImageDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<HallImageDTO> findByCriteria(HallImageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<HallImage> specification = createSpecification(criteria);
        return hallImageMapper.toDto(hallImageRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link HallImageDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<HallImageAllDTO> findByCriteria(HallImageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<HallImage> specification = createSpecification(criteria);
        return hallImageRepository.findAll(specification, page)
            .map(hallImageMapper::toAllDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(HallImageCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<HallImage> specification = createSpecification(criteria);
        return hallImageRepository.count(specification);
    }

    /**
     * Function to convert {@link HallImageCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<HallImage> createSpecification(HallImageCriteria criteria) {
        Specification<HallImage> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), HallImage_.id));
            }
            if (criteria.getHallId() != null) {
                specification = specification.and(buildSpecification(criteria.getHallId(),
                    root -> root.join(HallImage_.hall, JoinType.LEFT).get(Hall_.id)));
            }
            if (criteria.getRoomId() != null) {
                specification = specification.and(buildSpecification(criteria.getRoomId(),
                    root -> root.join(HallImage_.room, JoinType.LEFT).get(Room_.id)));
            }
        }
        return specification;
    }
}
