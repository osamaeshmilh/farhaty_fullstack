package ly.com.farhaty.service;

import ly.com.farhaty.domain.HallImage;
import ly.com.farhaty.repository.HallImageRepository;
import ly.com.farhaty.service.dto.HallImageAllDTO;
import ly.com.farhaty.service.dto.HallImageDTO;
import ly.com.farhaty.service.mapper.HallImageMapper;
import ly.com.farhaty.service.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link HallImage}.
 */
@Service
@Transactional
public class HallImageService {

    private final Logger log = LoggerFactory.getLogger(HallImageService.class);

    private final HallImageRepository hallImageRepository;

    private final HallImageMapper hallImageMapper;

    private final HallService hallService;


    public HallImageService(HallImageRepository hallImageRepository, HallImageMapper hallImageMapper, HallService hallService) {
        this.hallImageRepository = hallImageRepository;
        this.hallImageMapper = hallImageMapper;
        this.hallService = hallService;
    }

    /**
     * Save a hallImage.
     *
     * @param hallImageDTO the entity to save.
     * @return the persisted entity.
     */
    public HallImageDTO save(HallImageDTO hallImageDTO) {
        log.debug("Request to save HallImage : {}", hallImageDTO);
        HallImage hallImage = hallImageMapper.toEntity(hallImageDTO);

        if(hallImageDTO.getImage() != null){
            String filePath = FileUtils.upload(hallImage.getImage(), hallImage.getImageContentType(), hallImage.getTitle());
            hallImage.setImage(null);
            hallImage.setImageContentType(hallImageDTO.getImageContentType());
            hallImage.setImageUrl(filePath);
        }

        hallImage.setHall(hallService.findOneByUser());
        hallImage = hallImageRepository.save(hallImage);
        return hallImageMapper.toDto(hallImage);
    }

    /**
     * Get all the hallImages.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<HallImageDTO> findAll() {
        log.debug("Request to get all HallImages");
        return hallImageRepository.findAll().stream()
            .map(hallImageMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one hallImage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<HallImage> findOne(Long id) {
        log.debug("Request to get HallImage : {}", id);
        return hallImageRepository.findById(id);
    }

    /**
     * Delete the hallImage by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete HallImage : {}", id);
        hallImageRepository.deleteById(id);
    }

    public Page<HallImageAllDTO> findByUser(Long userId, Pageable pageable) {
        return hallImageRepository.findAllByHallUserId(userId, pageable).map(hallImageMapper::toAllDto);
    }
}
