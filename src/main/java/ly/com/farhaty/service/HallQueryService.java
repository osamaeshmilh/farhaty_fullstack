package ly.com.farhaty.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import ly.com.farhaty.service.dto.HallAllDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ly.com.farhaty.domain.Hall;
import ly.com.farhaty.domain.*; // for static metamodels
import ly.com.farhaty.repository.HallRepository;
import ly.com.farhaty.service.dto.HallCriteria;
import ly.com.farhaty.service.dto.HallDTO;
import ly.com.farhaty.service.mapper.HallMapper;

/**
 * Service for executing complex queries for {@link Hall} entities in the database.
 * The main input is a {@link HallCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link HallDTO} or a {@link Page} of {@link HallDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class HallQueryService extends QueryService<Hall> {

    private final Logger log = LoggerFactory.getLogger(HallQueryService.class);

    private final HallRepository hallRepository;

    private final ReservationService reservationService;

    private final HallMapper hallMapper;

    public HallQueryService(HallRepository hallRepository, ReservationService reservationService, HallMapper hallMapper) {
        this.hallRepository = hallRepository;
        this.reservationService = reservationService;
        this.hallMapper = hallMapper;
    }

    /**
     * Return a {@link List} of {@link HallDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<HallDTO> findByCriteria(HallCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Hall> specification = createSpecification(criteria);
        return hallMapper.toDto(hallRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link HallDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<HallAllDTO> findByCriteria(HallCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Hall> specification = createSpecification(criteria);
        Page<HallAllDTO> hallsPage = hallRepository.findAll(specification, page).map(hallMapper::toAllDto);
        hallsPage.forEach(hall -> {
            hall.setReservationCount(reservationService.countByHall(hall.getId()));
            hall.setReservationsTotal(reservationService.totalByHall(hall.getId()));
        });
        return hallsPage;
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(HallCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Hall> specification = createSpecification(criteria);
        return hallRepository.count(specification);
    }

    /**
     * Function to convert {@link HallCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Hall> createSpecification(HallCriteria criteria) {
        Specification<Hall> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Hall_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Hall_.name));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), Hall_.address));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), Hall_.phone));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), Hall_.email));
            }
            if (criteria.getLat() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLat(), Hall_.lat));
            }
            if (criteria.getLng() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLng(), Hall_.lng));
            }
            if (criteria.getVip() != null) {
                specification = specification.and(buildSpecification(criteria.getVip(), Hall_.vip));
            }
            if (criteria.getActive() != null) {
                specification = specification.and(buildSpecification(criteria.getActive(), Hall_.active));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Hall_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getTypeId() != null) {
                specification = specification.and(buildSpecification(criteria.getTypeId(),
                    root -> root.join(Hall_.types, JoinType.LEFT).get(Type_.id)));
            }
            if (criteria.getRoomId() != null) {
                specification = specification.and(buildSpecification(criteria.getRoomId(),
                    root -> root.join(Hall_.rooms, JoinType.LEFT).get(Room_.id)));
            }
            if (criteria.getExtraId() != null) {
                specification = specification.and(buildSpecification(criteria.getExtraId(),
                    root -> root.join(Hall_.extras, JoinType.LEFT).get(Extra_.id)));
            }
        }
        return specification;
    }
}
