package ly.com.farhaty.service;

import ly.com.farhaty.domain.*;
import ly.com.farhaty.repository.*;
import ly.com.farhaty.security.AuthoritiesConstants;
import ly.com.farhaty.service.dto.HallAllDTO;
import ly.com.farhaty.service.dto.HallDTO;
import ly.com.farhaty.service.mapper.HallMapper;
import ly.com.farhaty.service.util.FileUtils;
import ly.com.farhaty.web.rest.vm.ManagedUserVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Hall}.
 */
@Service
@Transactional
public class HallService {

    private final Logger log = LoggerFactory.getLogger(HallService.class);

    private final HallRepository hallRepository;

    private final HallMapper hallMapper;

    private final UserService userService;

    private final ReservationService reservationService;

    private final MailService mailService;


    public HallService(HallRepository hallRepository, HallMapper hallMapper, UserService userService, ReservationService reservationService, MailService mailService) {
        this.hallRepository = hallRepository;
        this.hallMapper = hallMapper;
        this.userService = userService;
        this.reservationService = reservationService;
        this.mailService = mailService;
    }

    /**
     * Save a hall.
     *
     * @param hallDTO the entity to save.
     * @return the persisted entity.
     */
    public HallDTO save(HallDTO hallDTO) {
        log.debug("Request to save Hall : {}", hallDTO);
        Hall hall = hallMapper.toEntity(hallDTO);


        if(hall.getLogo() != null){
            String filePath = FileUtils.upload(hall.getLogo(), hall.getLogoContentType(), hall.getName());
            hall.setLogo(null);
            hall.setLogoContentType(hallDTO.getLogoContentType());
            hall.setLogoUrl(filePath);
        }


        hall = hallRepository.save(hall);
        return hallMapper.toDto(hall);
    }

    /**
     * Get all the halls.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<HallDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Halls");
        return hallRepository.findAll(pageable)
            .map(hallMapper::toDto);
    }


    /**
     * Get one hall by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<Hall> findOne(Long id) {
        log.debug("Request to get Hall : {}", id);
        return hallRepository.findById(id);
    }

    /**
     * Delete the hall by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Hall : {}", id);
        hallRepository.deleteById(id);
    }

    @Transactional()
    public HallDTO create(HallDTO hallDTO) {

        log.debug("Request to save Hall : {}", hallDTO);

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setFirstName(hallDTO.getName());
        managedUserVM.setEmail(hallDTO.getEmail());
        managedUserVM.setPhone(hallDTO.getPhone());
        managedUserVM.setLogin(hallDTO.getEmail());
        managedUserVM.setPassword("farhaty123");
        User user = userService.registerUser(managedUserVM, managedUserVM.getPassword(), AuthoritiesConstants.HALL);
        mailService.sendCreationEmail(user, managedUserVM.getPassword());

        Hall hall = hallMapper.toEntity(hallDTO);
        hall.setUser(user);
        hall.setViewCount(0);
        return save(hallMapper.toDto(hall));
    }

    public Hall findOneByUser() {
        return hallRepository.findByUser(userService.getLoggedInUser().get());
    }

    public Page<HallAllDTO> findByUser(Long userId, Pageable pageable) {
        return hallRepository.findAllByUserId(userId, pageable).map(hallMapper::toAllDto);
    }

    public Page<HallAllDTO> findByDateIsAvailable(LocalDate requestedDate, Pageable pageable) {
        Page<HallAllDTO> hallAllDTOS = hallRepository.findAll(pageable).map(hallMapper::toAllDto);
        hallAllDTOS.forEach(hallAllDTO -> {
            if(requestedDate.isAfter(hallAllDTO.getAvailableFrom()) && requestedDate.isBefore(hallAllDTO.getAvailableTo())){
                hallAllDTO.setCanReserve(!reservationService.isHallFull(hallAllDTO, requestedDate));
                hallAllDTO.setRequestedDate(requestedDate);
                hallAllDTO.getRooms().forEach(roomDTO -> {
                    roomDTO.setCanReserve(!reservationService.isRoomFull(roomDTO.getId(), requestedDate));
                    roomDTO.setRequestedDate(requestedDate);
                });

            } else{
                hallAllDTO.setCanReserve(false);
            }

        });
        return hallAllDTOS;
    }

    public List<Object[]> datesByHallId(Long hallId) {
       return reservationService.datesByHall(hallId);
    }

    public List<Object[]> datesByHallIdAndRoomId(Long hallId, Long roomId) {
        return reservationService.datesByHallAndRoom(hallId, roomId);
    }


}

