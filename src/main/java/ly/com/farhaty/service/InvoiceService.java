package ly.com.farhaty.service;

import ly.com.farhaty.domain.Invoice;
import ly.com.farhaty.domain.Reservation;
import ly.com.farhaty.domain.enumeration.ReservationStatus;
import ly.com.farhaty.repository.InvoiceRepository;
import ly.com.farhaty.service.dto.InvoiceDTO;
import ly.com.farhaty.service.dto.ReservationDTO;
import ly.com.farhaty.service.mapper.InvoiceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Invoice}.
 */
@Service
@Transactional
public class InvoiceService {

    private final Logger log = LoggerFactory.getLogger(InvoiceService.class);

    private final InvoiceRepository invoiceRepository;

    private final InvoiceMapper invoiceMapper;

    private final ReservationService reservationService;

    public InvoiceService(InvoiceRepository invoiceRepository, InvoiceMapper invoiceMapper, ReservationService reservationService) {
        this.invoiceRepository = invoiceRepository;
        this.invoiceMapper = invoiceMapper;
        this.reservationService = reservationService;
    }

    /**
     * Save a invoice.
     *
     * @param invoiceDTO the entity to save.
     * @return the persisted entity.
     */
    public InvoiceDTO save(InvoiceDTO invoiceDTO) {
        log.debug("Request to save Invoice : {}", invoiceDTO);
        Invoice invoice = invoiceMapper.toEntity(invoiceDTO);
        invoice = invoiceRepository.save(invoice);
        return invoiceMapper.toDto(invoice);
    }

    /**
     * Get all the invoices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<InvoiceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Invoices");
        return invoiceRepository.findAll(pageable)
            .map(invoiceMapper::toDto);
    }


    /**
     * Get one invoice by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<InvoiceDTO> findOne(Long id) {
        log.debug("Request to get Invoice : {}", id);
        return invoiceRepository.findById(id)
            .map(invoiceMapper::toDto);
    }

    /**
     * Delete the invoice by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Invoice : {}", id);
        invoiceRepository.deleteById(id);
    }

    public Page<InvoiceDTO> findByUser(Long userId, Pageable pageable) {
        return invoiceRepository.findAllByReservationHallUserId(userId, pageable).map(invoiceMapper::toDto);

    }

    public InvoiceDTO pay(InvoiceDTO invoiceDTO) {

        ReservationDTO reservationDTO = reservationService.findOne(invoiceDTO.getReservationId()).get();
        reservationDTO.setReservationStatus(ReservationStatus.APPROVED);
        reservationService.save(reservationDTO);
        return save(invoiceDTO);

    }
}
