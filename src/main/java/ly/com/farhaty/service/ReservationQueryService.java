package ly.com.farhaty.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ly.com.farhaty.domain.Reservation;
import ly.com.farhaty.domain.*; // for static metamodels
import ly.com.farhaty.repository.ReservationRepository;
import ly.com.farhaty.service.dto.ReservationCriteria;
import ly.com.farhaty.service.dto.ReservationDTO;
import ly.com.farhaty.service.mapper.ReservationMapper;

/**
 * Service for executing complex queries for {@link Reservation} entities in the database.
 * The main input is a {@link ReservationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ReservationDTO} or a {@link Page} of {@link ReservationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ReservationQueryService extends QueryService<Reservation> {

    private final Logger log = LoggerFactory.getLogger(ReservationQueryService.class);

    private final ReservationRepository reservationRepository;

    private final ReservationMapper reservationMapper;

    public ReservationQueryService(ReservationRepository reservationRepository, ReservationMapper reservationMapper) {
        this.reservationRepository = reservationRepository;
        this.reservationMapper = reservationMapper;
    }

    /**
     * Return a {@link List} of {@link ReservationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ReservationDTO> findByCriteria(ReservationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Reservation> specification = createSpecification(criteria);
        return reservationMapper.toDto(reservationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ReservationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ReservationDTO> findByCriteria(ReservationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Reservation> specification = createSpecification(criteria);
        return reservationRepository.findAll(specification, page)
            .map(reservationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ReservationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Reservation> specification = createSpecification(criteria);
        return reservationRepository.count(specification);
    }

    /**
     * Function to convert {@link ReservationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Reservation> createSpecification(ReservationCriteria criteria) {
        Specification<Reservation> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Reservation_.id));
            }
            if (criteria.getAttendeesType() != null) {
                specification = specification.and(buildSpecification(criteria.getAttendeesType(), Reservation_.attendeesType));
            }
            if (criteria.getReservationStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getReservationStatus(), Reservation_.reservationStatus));
            }
            if (criteria.getAttendeesNo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAttendeesNo(), Reservation_.attendeesNo));
            }
            if (criteria.getTotal() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotal(), Reservation_.total));
            }
            if (criteria.getNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNotes(), Reservation_.notes));
            }
            if (criteria.getReservationDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReservationDate(), Reservation_.reservationDate));
            }
            if (criteria.getInvoiceId() != null) {
                specification = specification.and(buildSpecification(criteria.getInvoiceId(),
                    root -> root.join(Reservation_.invoices, JoinType.LEFT).get(Invoice_.id)));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Reservation_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getExtraId() != null) {
                specification = specification.and(buildSpecification(criteria.getExtraId(),
                    root -> root.join(Reservation_.extras, JoinType.LEFT).get(Extra_.id)));
            }
            if (criteria.getTypeId() != null) {
                specification = specification.and(buildSpecification(criteria.getTypeId(),
                    root -> root.join(Reservation_.type, JoinType.LEFT).get(Type_.id)));
            }
            if (criteria.getHallId() != null) {
                specification = specification.and(buildSpecification(criteria.getHallId(),
                    root -> root.join(Reservation_.hall, JoinType.LEFT).get(Hall_.id)));
            }
            if (criteria.getRoomId() != null) {
                specification = specification.and(buildSpecification(criteria.getRoomId(),
                    root -> root.join(Reservation_.room, JoinType.LEFT).get(Room_.id)));
            }
        }
        return specification;
    }
}
