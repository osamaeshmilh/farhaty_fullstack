package ly.com.farhaty.service;

import ly.com.farhaty.domain.Reservation;
import ly.com.farhaty.domain.enumeration.ReservationStatus;
import ly.com.farhaty.repository.HallRepository;
import ly.com.farhaty.repository.ReservationRepository;
import ly.com.farhaty.service.dto.HallAllDTO;
import ly.com.farhaty.service.dto.ReservationDTO;
import ly.com.farhaty.service.mapper.ReservationMapper;
import ly.com.farhaty.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Service Implementation for managing {@link Reservation}.
 */
@Service
@Transactional
public class ReservationService {

    private final Logger log = LoggerFactory.getLogger(ReservationService.class);

    private final ReservationRepository reservationRepository;

    private final ReservationMapper reservationMapper;

    private final HallRepository hallRepository;

    private final RoomService roomService;

    private final TypeService typeService;

    private final ExtraService extraService;

    private final UserService userService;

    public ReservationService(ReservationRepository reservationRepository,
                              ReservationMapper reservationMapper,
                              HallRepository hallRepository,
                              @Lazy RoomService roomService,
                              @Lazy TypeService typeService,
                              @Lazy ExtraService extraService, UserService userService) {
        this.reservationRepository = reservationRepository;
        this.reservationMapper = reservationMapper;
        this.hallRepository = hallRepository;
        this.roomService = roomService;
        this.typeService = typeService;
        this.extraService = extraService;
        this.userService = userService;
    }

    /**
     * Save a reservation.
     *
     * @param reservationDTO the entity to save.
     * @return the persisted entity.
     */
    public ReservationDTO save(ReservationDTO reservationDTO) {
        log.debug("Request to save Reservation : {}", reservationDTO);
        reservationDTO.setReservationDate(findOne(reservationDTO.getId()).get().getReservationDate());
        Reservation reservation = reservationMapper.toEntity(reservationDTO);

        reservation.setRoomTotal(roomService.findOne(reservationDTO.getRoomId()).get().getPrice());
        final Float[] finalTotal = {0.0F};
        roomService.findOne(reservationDTO.getRoomId()).ifPresent(roomDTO -> {
            finalTotal[0] += roomDTO.getPrice();
        });
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(reservationDTO.getReservationDate().getYear(),
            reservationDTO.getReservationDate().getMonthValue()-1,
            reservationDTO.getReservationDate().getDayOfMonth());
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        if(day == Calendar.SATURDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getSatPrice();
            });
        }else if (day == Calendar.SUNDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getSunPrice();
            });
        }else if (day == Calendar.MONDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getMonPrice();
            });
        }else if (day == Calendar.TUESDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getTuePrice();
            });
        }else if (day == Calendar.WEDNESDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getWedPrice();
            });
        }else if (day == Calendar.THURSDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getThrPrice();
            });
        }else if (day == Calendar.FRIDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getFriPrice();
            });
        }else {
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getSatPrice();
            });
        }
        AtomicReference<Float> extraTotal = new AtomicReference<>(0.0F);
        reservationDTO.getExtras().forEach(extraDTO -> {
            extraTotal.updateAndGet(v -> v + extraService.findOne(extraDTO.getId()).get().getPrice());
            finalTotal[0] += extraService.findOne(extraDTO.getId()).get().getPrice();

        });
        reservation.setExtrasTotal(extraTotal.get());
        reservation.setTotal(finalTotal[0]);

        reservation = reservationRepository.save(reservation);
        return reservationMapper.toDto(reservation);
    }

    public ReservationDTO create(ReservationDTO reservationDTO) {
        log.debug("Request to save Reservation : {}", reservationDTO);
        Reservation reservation = reservationMapper.toEntity(reservationDTO);
        if(reservation.getHall() == null){
            reservation.setHall(hallRepository.findByUser(userService.getLoggedInUser().get()));
        }else {
            reservation.setUser(userService.getLoggedInUser().get());
            if(reservationRepository.findByUserIdAndReservationStatus(userService.getLoggedInUser().get().getId(), ReservationStatus.PENDING).size() >= 3){
                throw new BadRequestAlertException("لديك تلاتة حجوزات غير مؤكدة !", "", "NotaryMobileCodeError");

            }
        }

        //System.out.println(reservation.getReservationDate().toString());

        reservation.setRoomTotal(roomService.findOne(reservationDTO.getRoomId()).get().getPrice());
        final Float[] finalTotal = {0.0F};
        roomService.findOne(reservationDTO.getRoomId()).ifPresent(roomDTO -> {
             finalTotal[0] += roomDTO.getPrice();
        });
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(reservationDTO.getReservationDate().getYear(),
            reservationDTO.getReservationDate().getMonthValue()-1,
            reservationDTO.getReservationDate().getDayOfMonth());
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        if(day == Calendar.SATURDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getSatPrice();
            });
        }else if (day == Calendar.SUNDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getSunPrice();
            });
        }else if (day == Calendar.MONDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getMonPrice();
            });
        }else if (day == Calendar.TUESDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getTuePrice();
            });
        }else if (day == Calendar.WEDNESDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getWedPrice();
            });
        }else if (day == Calendar.THURSDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getThrPrice();
            });
        }else if (day == Calendar.FRIDAY){
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getFriPrice();
            });
        }else {
            typeService.findOne(reservationDTO.getTypeId()).ifPresent(typeDTO -> {
                finalTotal[0] += typeDTO.getSatPrice();
            });
        }
        AtomicReference<Float> extraTotal = new AtomicReference<>(0.0F);
        reservationDTO.getExtras().forEach(extraDTO -> {
            extraTotal.updateAndGet(v -> v + extraService.findOne(extraDTO.getId()).get().getPrice());
            finalTotal[0] += extraService.findOne(extraDTO.getId()).get().getPrice();

        });
        reservation.setExtrasTotal(extraTotal.get());
        reservation.setTotal(finalTotal[0]);

        reservation = reservationRepository.save(reservation);
        return reservationMapper.toDto(reservation);
    }

    /**
     * Get all the reservations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ReservationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Reservations");
        return reservationRepository.findAll(pageable)
            .map(reservationMapper::toDto);
    }

    /**
     * Get all the reservations with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<ReservationDTO> findAllWithEagerRelationships(Pageable pageable) {
        return reservationRepository.findAllWithEagerRelationships(pageable).map(reservationMapper::toDto);
    }


    /**
     * Get one reservation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ReservationDTO> findOne(Long id) {
        log.debug("Request to get Reservation : {}", id);
        return reservationRepository.findOneWithEagerRelationships(id)
            .map(reservationMapper::toDto);
    }

    /**
     * Delete the reservation by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Reservation : {}", id);
        reservationRepository.deleteById(id);
    }


    public Page<ReservationDTO> findByHallUser(Long userId, Pageable pageable) {
        return reservationRepository.findByHallUserId(userId, pageable).map(reservationMapper::toDto);
    }

    public Page<ReservationDTO> findByHall(Long hallId, Pageable pageable) {
        return reservationRepository.findByHallId(hallId, pageable).map(reservationMapper::toDto);
    }

    public Page<ReservationDTO> findByHallAndRoom(Long hallId, Long roomId, Pageable pageable) {
        return reservationRepository.findByHallIdAndRoomId(hallId, roomId, pageable).map(reservationMapper::toDto);
    }

    public List<Object[]> datesByHall(Long hallId) {
        return reservationRepository.datesByHallId(hallId);
    }

    public List<Object[]> datesByHallAndRoom(Long hallId, Long roomId) {
        return reservationRepository.datesByHallIdAndRoomId(hallId, roomId);
    }


    public Boolean isHallFull(HallAllDTO hall, LocalDate date) {
        return reservationRepository.findByHallIdAndReservationDate(hall.getId(), date.toString()).size() >= hall.getRooms().size();
    }

    public Boolean isRoomFull(Long roomId, LocalDate date) {
        return reservationRepository.findByRoomIdAndReservationDate(roomId, date.toString()).size() > 0;
    }

    public Long countByHall(Long hallId) {
        return reservationRepository.countByHallId(hallId);
    }

    public Float totalByHall(Long hallId) {
        AtomicReference<Float> total = new AtomicReference<>(0.0F);
        List<Reservation> reservations =  reservationRepository.findAllByHallId(hallId);
        reservations.forEach(reservation -> {
            total.updateAndGet(v -> v + reservation.getTotal());
        });
        return total.get();
    }

    @Scheduled(cron = "0 0 1 * * ?")
    public void removePendingReservations() {
        reservationRepository
            .findAllByReservationStatusAndCreatedDateBefore(ReservationStatus.PENDING, Instant.now().minus(3, ChronoUnit.DAYS))
            .forEach(reservation -> {
                log.debug("expire pending reservation {}", reservation.getId());
                reservation.setReservationStatus(ReservationStatus.CANCELED_BY_HALL);
                reservation.setNotes("تم الغاء الحجز بسبب مضي تلاتة أيام دون تأكيد الحجز!");
                save(reservationMapper.toDto(reservation));
            });
    }

    public List<Reservation> findListByHall(Long hallId) {
        return reservationRepository.findAllByHallId(hallId);
    }

    public List<Reservation> findListByHallAndDate(Long hallId, LocalDate from, LocalDate to) {
        return reservationRepository.findAllByHallIdAndReservationDateBetween(hallId, from, to);

    }
}
