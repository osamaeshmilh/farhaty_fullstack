package ly.com.farhaty.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ly.com.farhaty.domain.Revenue;
import ly.com.farhaty.domain.*; // for static metamodels
import ly.com.farhaty.repository.RevenueRepository;
import ly.com.farhaty.service.dto.RevenueCriteria;
import ly.com.farhaty.service.dto.RevenueDTO;
import ly.com.farhaty.service.mapper.RevenueMapper;

/**
 * Service for executing complex queries for {@link Revenue} entities in the database.
 * The main input is a {@link RevenueCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RevenueDTO} or a {@link Page} of {@link RevenueDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RevenueQueryService extends QueryService<Revenue> {

    private final Logger log = LoggerFactory.getLogger(RevenueQueryService.class);

    private final RevenueRepository revenueRepository;

    private final RevenueMapper revenueMapper;

    public RevenueQueryService(RevenueRepository revenueRepository, RevenueMapper revenueMapper) {
        this.revenueRepository = revenueRepository;
        this.revenueMapper = revenueMapper;
    }

    /**
     * Return a {@link List} of {@link RevenueDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RevenueDTO> findByCriteria(RevenueCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Revenue> specification = createSpecification(criteria);
        return revenueMapper.toDto(revenueRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link RevenueDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RevenueDTO> findByCriteria(RevenueCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Revenue> specification = createSpecification(criteria);
        return revenueRepository.findAll(specification, page)
            .map(revenueMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RevenueCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Revenue> specification = createSpecification(criteria);
        return revenueRepository.count(specification);
    }

    /**
     * Function to convert {@link RevenueCriteria} to a {@link Specification}
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Revenue> createSpecification(RevenueCriteria criteria) {
        Specification<Revenue> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Revenue_.id));
            }
            if (criteria.getDetails() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDetails(), Revenue_.details));
            }
            if (criteria.getTotal() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotal(), Revenue_.total));
            }
            if (criteria.getHallId() != null) {
                specification = specification.and(buildSpecification(criteria.getHallId(),
                    root -> root.join(Revenue_.hall, JoinType.LEFT).get(Hall_.id)));
            }
        }
        return specification;
    }
}
