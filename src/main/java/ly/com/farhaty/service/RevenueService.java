package ly.com.farhaty.service;

import ly.com.farhaty.service.dto.RevenueDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ly.com.farhaty.domain.Revenue}.
 */
public interface RevenueService {

    /**
     * Save a revenue.
     *
     * @param revenueDTO the entity to save.
     * @return the persisted entity.
     */
    RevenueDTO save(RevenueDTO revenueDTO);

    /**
     * Get all the revenues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RevenueDTO> findAll(Pageable pageable);


    /**
     * Get the "id" revenue.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RevenueDTO> findOne(Long id);

    /**
     * Delete the "id" revenue.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
