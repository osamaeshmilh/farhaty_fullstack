package ly.com.farhaty.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import ly.com.farhaty.service.dto.StoreImageAllDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ly.com.farhaty.domain.StoreImage;
import ly.com.farhaty.domain.*; // for static metamodels
import ly.com.farhaty.repository.StoreImageRepository;
import ly.com.farhaty.service.dto.StoreImageCriteria;
import ly.com.farhaty.service.dto.StoreImageDTO;
import ly.com.farhaty.service.mapper.StoreImageMapper;

/**
 * Service for executing complex queries for {@link StoreImage} entities in the database.
 * The main input is a {@link StoreImageCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link StoreImageDTO} or a {@link Page} of {@link StoreImageDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class StoreImageQueryService extends QueryService<StoreImage> {

    private final Logger log = LoggerFactory.getLogger(StoreImageQueryService.class);

    private final StoreImageRepository storeImageRepository;

    private final StoreImageMapper storeImageMapper;

    public StoreImageQueryService(StoreImageRepository storeImageRepository, StoreImageMapper storeImageMapper) {
        this.storeImageRepository = storeImageRepository;
        this.storeImageMapper = storeImageMapper;
    }

    /**
     * Return a {@link List} of {@link StoreImageDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<StoreImageDTO> findByCriteria(StoreImageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<StoreImage> specification = createSpecification(criteria);
        return storeImageMapper.toDto(storeImageRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link StoreImageDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<StoreImageAllDTO> findByCriteria(StoreImageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<StoreImage> specification = createSpecification(criteria);
        return storeImageRepository.findAll(specification, page)
            .map(storeImageMapper::toAllDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(StoreImageCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<StoreImage> specification = createSpecification(criteria);
        return storeImageRepository.count(specification);
    }

    /**
     * Function to convert {@link StoreImageCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<StoreImage> createSpecification(StoreImageCriteria criteria) {
        Specification<StoreImage> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), StoreImage_.id));
            }
            if (criteria.getStoreId() != null) {
                specification = specification.and(buildSpecification(criteria.getStoreId(),
                    root -> root.join(StoreImage_.store, JoinType.LEFT).get(Store_.id)));
            }
        }
        return specification;
    }
}
