package ly.com.farhaty.service;

import ly.com.farhaty.domain.StoreImage;
import ly.com.farhaty.repository.StoreImageRepository;
import ly.com.farhaty.service.dto.StoreImageAllDTO;
import ly.com.farhaty.service.dto.StoreImageDTO;
import ly.com.farhaty.service.mapper.StoreImageMapper;
import ly.com.farhaty.service.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link StoreImage}.
 */
@Service
@Transactional
public class StoreImageService {

    private final Logger log = LoggerFactory.getLogger(StoreImageService.class);

    private final StoreImageRepository storeImageRepository;

    private final StoreImageMapper storeImageMapper;

    private final StoreService storeService;

    public StoreImageService(StoreImageRepository storeImageRepository, StoreImageMapper storeImageMapper, StoreService storeService) {
        this.storeImageRepository = storeImageRepository;
        this.storeImageMapper = storeImageMapper;
        this.storeService = storeService;
    }

    /**
     * Save a storeImage.
     *
     * @param storeImageDTO the entity to save.
     * @return the persisted entity.
     */
    @Transactional
    public StoreImageDTO save(StoreImageDTO storeImageDTO) {
        log.debug("Request to save StoreImage : {}", storeImageDTO);
        StoreImage storeImage = storeImageMapper.toEntity(storeImageDTO);

        if(storeImageDTO.getImage() != null) {
            String filePath = FileUtils.upload(storeImage.getImage(), storeImage.getImageContentType(), storeImage.getTitle());
            storeImage.setImage(null);
            storeImage.setImageContentType(storeImageDTO.getImageContentType());
            storeImage.setImageUrl(filePath);
        }

        storeImage.setStore(storeService.findOneByUser());
        storeImage = storeImageRepository.save(storeImage);
        return storeImageMapper.toDto(storeImage);
    }

    /**
     * Get all the storeImages.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<StoreImageDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StoreImages");
        return storeImageRepository.findAll(pageable)
            .map(storeImageMapper::toDto);
    }


    /**
     * Get one storeImage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<StoreImage> findOne(Long id) {
        log.debug("Request to get StoreImage : {}", id);
        return storeImageRepository.findById(id);
    }

    /**
     * Delete the storeImage by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete StoreImage : {}", id);
        storeImageRepository.deleteById(id);
    }

    public Page<StoreImageAllDTO> findByUser(Long userId, Pageable pageable) {
        return storeImageRepository.findAllByStoreUserId(userId, pageable).map(storeImageMapper::toAllDto);
    }
}
