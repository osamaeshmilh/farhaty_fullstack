package ly.com.farhaty.service;

import ly.com.farhaty.domain.Store;
import ly.com.farhaty.domain.User;
import ly.com.farhaty.repository.StoreRepository;
import ly.com.farhaty.security.AuthoritiesConstants;
import ly.com.farhaty.service.dto.StoreAllDTO;
import ly.com.farhaty.service.dto.StoreDTO;
import ly.com.farhaty.service.mapper.StoreMapper;
import ly.com.farhaty.service.util.FileUtils;
import ly.com.farhaty.service.util.RandomUtil;
import ly.com.farhaty.web.rest.vm.ManagedUserVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Store}.
 */
@Service
@Transactional
public class StoreService {

    private final Logger log = LoggerFactory.getLogger(StoreService.class);

    private final StoreRepository storeRepository;

    private final StoreMapper storeMapper;

    private final UserService userService;

    private final MailService mailService;

    public StoreService(StoreRepository storeRepository, StoreMapper storeMapper, UserService userService, MailService mailService) {
        this.storeRepository = storeRepository;
        this.storeMapper = storeMapper;
        this.userService = userService;
        this.mailService = mailService;
    }

    /**
     * Save a store.
     *
     * @param storeDTO the entity to save.
     * @return the persisted entity.
     */
    public StoreDTO save(StoreDTO storeDTO) {
        log.debug("Request to save Store : {}", storeDTO);
        Store store = storeMapper.toEntity(storeDTO);

        if(store.getLogo() != null){
            String filePath = FileUtils.upload(store.getLogo(), store.getLogoContentType(), store.getName());
            store.setLogo(null);
            store.setLogoContentType(storeDTO.getLogoContentType());
            store.setLogoUrl(filePath);
        }


        store = storeRepository.save(store);
        return storeMapper.toDto(store);
    }

    @Transactional()
    public StoreDTO create(StoreDTO storeDTO) {
        log.debug("Request to save Store : {}", storeDTO);

        ManagedUserVM managedUserVM = new ManagedUserVM();
        managedUserVM.setFirstName(storeDTO.getName());
        managedUserVM.setEmail(storeDTO.getEmail());
        managedUserVM.setPhone(storeDTO.getPhone());
        managedUserVM.setLogin(storeDTO.getEmail());
        managedUserVM.setPassword("farhaty123");
        User user = userService.registerUser(managedUserVM, managedUserVM.getPassword(), AuthoritiesConstants.STORE);
        mailService.sendCreationEmail(user, managedUserVM.getPassword());

        Store store = storeMapper.toEntity(storeDTO);
        store.setUser(user);
        store.setViewCount(0);
        return save(storeMapper.toDto(store));
    }

    /**
     * Get all the stores.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<StoreDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Stores");
        return storeRepository.findAll(pageable)
            .map(storeMapper::toDto);
    }


    /**
     * Get one store by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<Store> findOne(Long id) {
        log.debug("Request to get Store : {}", id);

        return storeRepository.findById(id);
    }

    /**
     * Delete the store by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Store : {}", id);
        storeRepository.deleteById(id);
    }

    public Store findOneByUser() {
        return storeRepository.findByUser(userService.getLoggedInUser().get());
    }

    public Page<StoreAllDTO> findByUser(Long userId, Pageable pageable) {
        return storeRepository.findAllByUserId(userId, pageable).map(storeMapper::toAllDto);
    }
}
