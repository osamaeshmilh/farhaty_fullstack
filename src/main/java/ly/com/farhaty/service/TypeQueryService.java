package ly.com.farhaty.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ly.com.farhaty.domain.Type;
import ly.com.farhaty.domain.*; // for static metamodels
import ly.com.farhaty.repository.TypeRepository;
import ly.com.farhaty.service.dto.TypeCriteria;
import ly.com.farhaty.service.dto.TypeDTO;
import ly.com.farhaty.service.mapper.TypeMapper;

/**
 * Service for executing complex queries for {@link Type} entities in the database.
 * The main input is a {@link TypeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TypeDTO} or a {@link Page} of {@link TypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TypeQueryService extends QueryService<Type> {

    private final Logger log = LoggerFactory.getLogger(TypeQueryService.class);

    private final TypeRepository typeRepository;

    private final TypeMapper typeMapper;

    public TypeQueryService(TypeRepository typeRepository, TypeMapper typeMapper) {
        this.typeRepository = typeRepository;
        this.typeMapper = typeMapper;
    }

    /**
     * Return a {@link List} of {@link TypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TypeDTO> findByCriteria(TypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Type> specification = createSpecification(criteria);
        return typeMapper.toDto(typeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TypeDTO> findByCriteria(TypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Type> specification = createSpecification(criteria);
        return typeRepository.findAll(specification, page)
            .map(typeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TypeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Type> specification = createSpecification(criteria);
        return typeRepository.count(specification);
    }

    /**
     * Function to convert {@link TypeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Type> createSpecification(TypeCriteria criteria) {
        Specification<Type> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Type_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Type_.name));
            }
            if (criteria.getSatPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSatPrice(), Type_.satPrice));
            }
            if (criteria.getSunPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSunPrice(), Type_.sunPrice));
            }
            if (criteria.getMonPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMonPrice(), Type_.monPrice));
            }
            if (criteria.getTuePrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTuePrice(), Type_.tuePrice));
            }
            if (criteria.getWedPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getWedPrice(), Type_.wedPrice));
            }
            if (criteria.getThrPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThrPrice(), Type_.thrPrice));
            }
            if (criteria.getFriPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFriPrice(), Type_.friPrice));
            }
            if (criteria.getActive() != null) {
                specification = specification.and(buildSpecification(criteria.getActive(), Type_.active));
            }
            if (criteria.getHallId() != null) {
                specification = specification.and(buildSpecification(criteria.getHallId(),
                    root -> root.join(Type_.hall, JoinType.LEFT).get(Hall_.id)));
            }
        }
        return specification;
    }
}
