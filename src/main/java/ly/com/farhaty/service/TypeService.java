package ly.com.farhaty.service;

import ly.com.farhaty.domain.Type;
import ly.com.farhaty.repository.TypeRepository;
import ly.com.farhaty.service.dto.TypeDTO;
import ly.com.farhaty.service.mapper.TypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Type}.
 */
@Service
@Transactional
public class TypeService {

    private final Logger log = LoggerFactory.getLogger(TypeService.class);

    private final TypeRepository typeRepository;

    private final TypeMapper typeMapper;

    private final HallService hallService;

    public TypeService(TypeRepository typeRepository, TypeMapper typeMapper, HallService hallService) {
        this.typeRepository = typeRepository;
        this.typeMapper = typeMapper;
        this.hallService = hallService;
    }

    /**
     * Save a type.
     *
     * @param typeDTO the entity to save.
     * @return the persisted entity.
     */
    public TypeDTO save(TypeDTO typeDTO) {
        log.debug("Request to save Type : {}", typeDTO);
        Type type = typeMapper.toEntity(typeDTO);
        type.setHall(hallService.findOneByUser());
        type = typeRepository.save(type);
        return typeMapper.toDto(type);
    }

    /**
     * Get all the types.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Types");
        return typeRepository.findAll(pageable)
            .map(typeMapper::toDto);
    }


    /**
     * Get one type by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TypeDTO> findOne(Long id) {
        log.debug("Request to get Type : {}", id);
        return typeRepository.findById(id)
            .map(typeMapper::toDto);
    }

    /**
     * Delete the type by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Type : {}", id);
        typeRepository.deleteById(id);
    }

    public Page<TypeDTO> findByUser(Long userId, Pageable pageable) {
        return typeRepository.findAllByHallUserId(userId, pageable).map(typeMapper::toDto);

    }
}
