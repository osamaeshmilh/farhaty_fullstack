package ly.com.farhaty.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import ly.com.farhaty.domain.enumeration.Period;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link ly.com.farhaty.domain.Appointment} entity. This class is used
 * in {@link ly.com.farhaty.web.rest.AppointmentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /appointments?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AppointmentCriteria implements Serializable, Criteria {
    /**
     * Class for filtering Period
     */
    public static class PeriodFilter extends Filter<Period> {

        public PeriodFilter() {
        }

        public PeriodFilter(PeriodFilter filter) {
            super(filter);
        }

        @Override
        public PeriodFilter copy() {
            return new PeriodFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter appointmentDate;

    private PeriodFilter period;

    private FloatFilter price;

    private StringFilter notes;

    private LongFilter reservationId;

    private LongFilter hallId;

    private LongFilter roomId;

    public AppointmentCriteria(){
    }

    public AppointmentCriteria(AppointmentCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.appointmentDate = other.appointmentDate == null ? null : other.appointmentDate.copy();
        this.period = other.period == null ? null : other.period.copy();
        this.price = other.price == null ? null : other.price.copy();
        this.notes = other.notes == null ? null : other.notes.copy();
        this.reservationId = other.reservationId == null ? null : other.reservationId.copy();
        this.hallId = other.hallId == null ? null : other.hallId.copy();
        this.roomId = other.roomId == null ? null : other.roomId.copy();
    }

    @Override
    public AppointmentCriteria copy() {
        return new AppointmentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(LocalDateFilter appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public PeriodFilter getPeriod() {
        return period;
    }

    public void setPeriod(PeriodFilter period) {
        this.period = period;
    }

    public FloatFilter getPrice() {
        return price;
    }

    public void setPrice(FloatFilter price) {
        this.price = price;
    }

    public StringFilter getNotes() {
        return notes;
    }

    public void setNotes(StringFilter notes) {
        this.notes = notes;
    }

    public LongFilter getReservationId() {
        return reservationId;
    }

    public void setReservationId(LongFilter reservationId) {
        this.reservationId = reservationId;
    }

    public LongFilter getHallId() {
        return hallId;
    }

    public void setHallId(LongFilter hallId) {
        this.hallId = hallId;
    }

    public LongFilter getRoomId() {
        return roomId;
    }

    public void setRoomId(LongFilter roomId) {
        this.roomId = roomId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AppointmentCriteria that = (AppointmentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(appointmentDate, that.appointmentDate) &&
            Objects.equals(period, that.period) &&
            Objects.equals(price, that.price) &&
            Objects.equals(notes, that.notes) &&
            Objects.equals(reservationId, that.reservationId) &&
            Objects.equals(hallId, that.hallId) &&
            Objects.equals(roomId, that.roomId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        appointmentDate,
        period,
        price,
        notes,
        reservationId,
        hallId,
        roomId
        );
    }

    @Override
    public String toString() {
        return "AppointmentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (appointmentDate != null ? "appointmentDate=" + appointmentDate + ", " : "") +
                (period != null ? "period=" + period + ", " : "") +
                (price != null ? "price=" + price + ", " : "") +
                (notes != null ? "notes=" + notes + ", " : "") +
                (reservationId != null ? "reservationId=" + reservationId + ", " : "") +
                (hallId != null ? "hallId=" + hallId + ", " : "") +
                (roomId != null ? "roomId=" + roomId + ", " : "") +
            "}";
    }

}
