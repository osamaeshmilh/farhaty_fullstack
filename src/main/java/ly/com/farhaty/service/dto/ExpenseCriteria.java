package ly.com.farhaty.service.dto;

import java.io.Serializable;
import java.util.Objects;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ly.com.farhaty.domain.Expense} entity. This class is used
 * in {@link ly.com.farhaty.web.rest.ExpenseResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /expenses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ExpenseCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter details;

    private FloatFilter total;

    private LongFilter hallId;

    public ExpenseCriteria() {
    }

    public ExpenseCriteria(ExpenseCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.details = other.details == null ? null : other.details.copy();
        this.total = other.total == null ? null : other.total.copy();
        this.hallId = other.hallId == null ? null : other.hallId.copy();
    }

    @Override
    public ExpenseCriteria copy() {
        return new ExpenseCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDetails() {
        return details;
    }

    public void setDetails(StringFilter details) {
        this.details = details;
    }

    public FloatFilter getTotal() {
        return total;
    }

    public void setTotal(FloatFilter total) {
        this.total = total;
    }

    public LongFilter getHallId() {
        return hallId;
    }

    public void setHallId(LongFilter hallId) {
        this.hallId = hallId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ExpenseCriteria that = (ExpenseCriteria) o;
        return
            Objects.equals(id, that.id) &&
                Objects.equals(details, that.details) &&
                Objects.equals(total, that.total) &&
                Objects.equals(hallId, that.hallId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            details,
            total,
            hallId
        );
    }

    @Override
    public String toString() {
        return "ExpenseCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (details != null ? "details=" + details + ", " : "") +
            (total != null ? "total=" + total + ", " : "") +
            (hallId != null ? "hallId=" + hallId + ", " : "") +
            "}";
    }

}
