package ly.com.farhaty.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ly.com.farhaty.domain.FavoriteItem} entity. This class is used
 * in {@link ly.com.farhaty.web.rest.FavoriteItemResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /favorite-items?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FavoriteItemCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter itemId;

    private LongFilter userId;

    public FavoriteItemCriteria(){
    }

    public FavoriteItemCriteria(FavoriteItemCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.itemId = other.itemId == null ? null : other.itemId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public FavoriteItemCriteria copy() {
        return new FavoriteItemCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getItemId() {
        return itemId;
    }

    public void setItemId(LongFilter itemId) {
        this.itemId = itemId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FavoriteItemCriteria that = (FavoriteItemCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(itemId, that.itemId) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        itemId,
        userId
        );
    }

    @Override
    public String toString() {
        return "FavoriteItemCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (itemId != null ? "itemId=" + itemId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
