package ly.com.farhaty.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ly.com.farhaty.domain.FavoriteItem} entity.
 */
public class FavoriteItemDTO implements Serializable {

    private Long id;


    private Long itemId;

    private String itemName;

    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FavoriteItemDTO favoriteItemDTO = (FavoriteItemDTO) o;
        if (favoriteItemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), favoriteItemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FavoriteItemDTO{" +
            "id=" + getId() +
            ", item=" + getItemId() +
            ", item='" + getItemName() + "'" +
            ", user=" + getUserId() +
            "}";
    }
}
