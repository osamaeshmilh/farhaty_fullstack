package ly.com.farhaty.service.dto;

import ly.com.farhaty.domain.Extra;
import ly.com.farhaty.domain.Reservation;
import ly.com.farhaty.domain.Room;
import ly.com.farhaty.domain.Type;

import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link ly.com.farhaty.domain.Hall} entity.
 */
public class HallAllDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String description;

    @NotNull
    private String address;

    @NotNull
    private String phone;

    private String email;

    private String facebook;

    private String whatsup;

    private String viper;

    private String twitter;

    private String instagram;

    private String snapchat;

    private Float lat;

    private Float lng;

    @Lob
    private String rules;

    private Boolean vip;

    private String logoUrl;

    private String logoContentType;

    private LocalDate availableFrom;

    private LocalDate availableTo;

    private Integer pendingAppointmentPeriod;

    private Integer viewCount;

    private Boolean twoReservations;

    private Long userId;

    private Float reservationsTotal;

    private Long reservationCount;

    private Set<TypeDTO> types = new HashSet<>();

    private Set<RoomDTO> rooms = new HashSet<>();

    private Set<ExtraDTO> extras = new HashSet<>();



//    private Set<ReservationDTO> reservation = new HashSet<>();

    private Boolean canReserve;

    private LocalDate requestedDate;

    private List<Object[]> reservedDates;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public Boolean isVip() {
        return vip;
    }

    public void setVip(Boolean vip) {
        this.vip = vip;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLogoContentType() {
        return logoContentType;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }

    public LocalDate getAvailableFrom() {
        return availableFrom;
    }

    public void setAvailableFrom(LocalDate availableFrom) {
        this.availableFrom = availableFrom;
    }

    public LocalDate getAvailableTo() {
        return availableTo;
    }

    public void setAvailableTo(LocalDate availableTo) {
        this.availableTo = availableTo;
    }

    public Integer getPendingAppointmentPeriod() {
        return pendingAppointmentPeriod;
    }

    public void setPendingAppointmentPeriod(Integer pendingAppointmentPeriod) {
        this.pendingAppointmentPeriod = pendingAppointmentPeriod;
    }

    public Set<TypeDTO> getTypes() {
        return types;
    }

    public void setTypes(Set<TypeDTO> types) {
        this.types = types;
    }

    public Set<RoomDTO> getRooms() {
        return rooms;
    }

    public void setRooms(Set<RoomDTO> rooms) {
        this.rooms = rooms;
    }

    public Set<ExtraDTO> getExtras() {
        return extras;
    }

    public void setExtras(Set<ExtraDTO> extras) {
        this.extras = extras;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getWhatsup() {
        return whatsup;
    }

    public void setWhatsup(String whatsup) {
        this.whatsup = whatsup;
    }

    public String getViper() {
        return viper;
    }

    public void setViper(String viper) {
        this.viper = viper;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getSnapchat() {
        return snapchat;
    }

    public void setSnapchat(String snapchat) {
        this.snapchat = snapchat;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Boolean getCanReserve() {
        return canReserve;
    }

    public void setCanReserve(Boolean canReserve) {
        this.canReserve = canReserve;
    }

    public LocalDate getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(LocalDate requestedDate) {
        this.requestedDate = requestedDate;
    }

    public List<Object[]> getReservedDates() {
        return reservedDates;
    }

    public void setReservedDates(List<Object[]> reservedDates) {
        this.reservedDates = reservedDates;
    }

    public Float getReservationsTotal() {
        return reservationsTotal;
    }

    public void setReservationsTotal(Float reservationsTotal) {
        this.reservationsTotal = reservationsTotal;
    }

    public Long getReservationCount() {
        return reservationCount;
    }

    public void setReservationCount(Long reservationCount) {
        this.reservationCount = reservationCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HallAllDTO hallDTO = (HallAllDTO) o;
        if (hallDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), hallDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HallDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", lat=" + getLat() +
            ", lng=" + getLng() +
            ", rules='" + getRules() + "'" +
            ", vip='" + isVip() + "'" +
            ", user=" + getUserId() +
            "}";
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getTwoReservations() {
        return twoReservations;
    }

    public void setTwoReservations(Boolean twoReservations) {
        this.twoReservations = twoReservations;
    }
}
