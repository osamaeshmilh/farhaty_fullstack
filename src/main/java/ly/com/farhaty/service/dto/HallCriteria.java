package ly.com.farhaty.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.*;

/**
 * Criteria class for the {@link ly.com.farhaty.domain.Hall} entity. This class is used
 * in {@link ly.com.farhaty.web.rest.HallResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /halls?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class HallCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter address;

    private StringFilter phone;

    private StringFilter email;

    private FloatFilter lat;

    private FloatFilter lng;

    private BooleanFilter vip;

    private LongFilter userId;

    private LongFilter appointmentId;

    private LongFilter typeId;

    private LongFilter roomId;

    private LongFilter extraId;

    private BooleanFilter active;

    private LocalDateFilter requestedDate;

    public HallCriteria(){
    }

    public HallCriteria(HallCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.address = other.address == null ? null : other.address.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.lat = other.lat == null ? null : other.lat.copy();
        this.lng = other.lng == null ? null : other.lng.copy();
        this.vip = other.vip == null ? null : other.vip.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.appointmentId = other.appointmentId == null ? null : other.appointmentId.copy();
        this.typeId = other.typeId == null ? null : other.typeId.copy();
        this.roomId = other.roomId == null ? null : other.roomId.copy();
        this.extraId = other.extraId == null ? null : other.extraId.copy();
        this.active = other.active == null ? null : other.active.copy();
        this.requestedDate = other.requestedDate == null ? null : other.requestedDate.copy();
    }

    @Override
    public HallCriteria copy() {
        return new HallCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getAddress() {
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public FloatFilter getLat() {
        return lat;
    }

    public void setLat(FloatFilter lat) {
        this.lat = lat;
    }

    public FloatFilter getLng() {
        return lng;
    }

    public void setLng(FloatFilter lng) {
        this.lng = lng;
    }

    public BooleanFilter getVip() {
        return vip;
    }

    public void setVip(BooleanFilter vip) {
        this.vip = vip;
    }

    public BooleanFilter getActive() {
        return active;
    }

    public void setActive(BooleanFilter active) {
        this.active = active;
    }


    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(LongFilter appointmentId) {
        this.appointmentId = appointmentId;
    }

    public LongFilter getTypeId() {
        return typeId;
    }

    public void setTypeId(LongFilter typeId) {
        this.typeId = typeId;
    }

    public LongFilter getRoomId() {
        return roomId;
    }

    public void setRoomId(LongFilter roomId) {
        this.roomId = roomId;
    }

    public LongFilter getExtraId() {
        return extraId;
    }

    public void setExtraId(LongFilter extraId) {
        this.extraId = extraId;
    }

    public LocalDateFilter getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(LocalDateFilter requestedDate) {
        this.requestedDate = requestedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final HallCriteria that = (HallCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(address, that.address) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(email, that.email) &&
            Objects.equals(lat, that.lat) &&
            Objects.equals(lng, that.lng) &&
            Objects.equals(vip, that.vip) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(appointmentId, that.appointmentId) &&
            Objects.equals(typeId, that.typeId) &&
            Objects.equals(roomId, that.roomId) &&
                Objects.equals(extraId, that.extraId) &&
                Objects.equals(active, that.active) &&
                Objects.equals(requestedDate, that.requestedDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        address,
        phone,
        email,
        lat,
        lng,
        vip,
        userId,
        appointmentId,
        typeId,
        roomId,
        extraId,
            active,
             requestedDate
            );
    }

    @Override
    public String toString() {
        return "HallCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (address != null ? "address=" + address + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (lat != null ? "lat=" + lat + ", " : "") +
                (lng != null ? "lng=" + lng + ", " : "") +
                (vip != null ? "vip=" + vip + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (appointmentId != null ? "appointmentId=" + appointmentId + ", " : "") +
                (typeId != null ? "typeId=" + typeId + ", " : "") +
                (roomId != null ? "roomId=" + roomId + ", " : "") +
            (active != null ? "active=" + active + ", " : "") +
            (extraId != null ? "extraId=" + extraId + ", " : "") +
            (requestedDate != null ? "requestedDate=" + requestedDate + ", " : "") +
            "}";
    }

}
