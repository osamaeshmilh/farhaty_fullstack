package ly.com.farhaty.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ly.com.farhaty.domain.HallImage} entity. This class is used
 * in {@link ly.com.farhaty.web.rest.HallImageResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /hall-images?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class HallImageCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter hallId;

    private LongFilter roomId;

    public HallImageCriteria(){
    }

    public HallImageCriteria(HallImageCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.hallId = other.hallId == null ? null : other.hallId.copy();
        this.roomId = other.roomId == null ? null : other.roomId.copy();
    }

    @Override
    public HallImageCriteria copy() {
        return new HallImageCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getHallId() {
        return hallId;
    }

    public void setHallId(LongFilter hallId) {
        this.hallId = hallId;
    }

    public LongFilter getRoomId() {
        return roomId;
    }

    public void setRoomId(LongFilter roomId) {
        this.roomId = roomId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final HallImageCriteria that = (HallImageCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(hallId, that.hallId) &&
            Objects.equals(roomId, that.roomId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        hallId,
        roomId
        );
    }

    @Override
    public String toString() {
        return "HallImageCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (hallId != null ? "hallId=" + hallId + ", " : "") +
                (roomId != null ? "roomId=" + roomId + ", " : "") +
            "}";
    }

}
