package ly.com.farhaty.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import ly.com.farhaty.domain.enumeration.InvoiceType;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link ly.com.farhaty.domain.Invoice} entity. This class is used
 * in {@link ly.com.farhaty.web.rest.InvoiceResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /invoices?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class InvoiceCriteria implements Serializable, Criteria {
    /**
     * Class for filtering InvoiceType
     */
    public static class InvoiceTypeFilter extends Filter<InvoiceType> {

        public InvoiceTypeFilter() {
        }

        public InvoiceTypeFilter(InvoiceTypeFilter filter) {
            super(filter);
        }

        @Override
        public InvoiceTypeFilter copy() {
            return new InvoiceTypeFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter invoiceDate;

    private FloatFilter amount;

    private StringFilter details;

    private InvoiceTypeFilter invoiceType;

    private LongFilter reservationId;

    public InvoiceCriteria(){
    }

    public InvoiceCriteria(InvoiceCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.invoiceDate = other.invoiceDate == null ? null : other.invoiceDate.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.details = other.details == null ? null : other.details.copy();
        this.invoiceType = other.invoiceType == null ? null : other.invoiceType.copy();
        this.reservationId = other.reservationId == null ? null : other.reservationId.copy();
    }

    @Override
    public InvoiceCriteria copy() {
        return new InvoiceCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(LocalDateFilter invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public FloatFilter getAmount() {
        return amount;
    }

    public void setAmount(FloatFilter amount) {
        this.amount = amount;
    }

    public StringFilter getDetails() {
        return details;
    }

    public void setDetails(StringFilter details) {
        this.details = details;
    }

    public InvoiceTypeFilter getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(InvoiceTypeFilter invoiceType) {
        this.invoiceType = invoiceType;
    }

    public LongFilter getReservationId() {
        return reservationId;
    }

    public void setReservationId(LongFilter reservationId) {
        this.reservationId = reservationId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final InvoiceCriteria that = (InvoiceCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(invoiceDate, that.invoiceDate) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(details, that.details) &&
            Objects.equals(invoiceType, that.invoiceType) &&
            Objects.equals(reservationId, that.reservationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        invoiceDate,
        amount,
        details,
        invoiceType,
        reservationId
        );
    }

    @Override
    public String toString() {
        return "InvoiceCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (invoiceDate != null ? "invoiceDate=" + invoiceDate + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (details != null ? "details=" + details + ", " : "") +
                (invoiceType != null ? "invoiceType=" + invoiceType + ", " : "") +
                (reservationId != null ? "reservationId=" + reservationId + ", " : "") +
            "}";
    }

}
