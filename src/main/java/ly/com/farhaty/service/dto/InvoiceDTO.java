package ly.com.farhaty.service.dto;
import java.time.Instant;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import ly.com.farhaty.domain.Reservation;
import ly.com.farhaty.domain.enumeration.InvoiceType;
import org.mapstruct.Mapping;

/**
 * A DTO for the {@link ly.com.farhaty.domain.Invoice} entity.
 */
public class InvoiceDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate invoiceDate;

    @NotNull
    private Float amount;

    private String details;

    @NotNull
    private InvoiceType invoiceType;

    private Long hallId;

    private String hallName;

    private Long reservationId;

    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

    private Float reservationTotal;

    private Float extrasTotal;

    private String reservationCustomerName;

    private String reservationCustomerPhone;

    private LocalDate reservationReservationDate;

    private String reservationUserFirstName;

    private String reservationUserPhone;

    public InvoiceDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public InvoiceType getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(InvoiceType invoiceType) {
        this.invoiceType = invoiceType;
    }

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InvoiceDTO invoiceDTO = (InvoiceDTO) o;
        if (invoiceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), invoiceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InvoiceDTO{" +
            "id=" + getId() +
            ", invoiceDate='" + getInvoiceDate() + "'" +
            ", amount=" + getAmount() +
            ", details='" + getDetails() + "'" +
            ", invoiceType='" + getInvoiceType() + "'" +
            ", reservation=" + getReservationId() +
            "}";
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Float getReservationTotal() {
        return reservationTotal;
    }

    public void setReservationTotal(Float reservationTotal) {
        this.reservationTotal = reservationTotal;
    }

    public String getReservationCustomerName() {
        return reservationCustomerName;
    }

    public void setReservationCustomerName(String reservationCustomerName) {
        this.reservationCustomerName = reservationCustomerName;
    }

    public String getReservationCustomerPhone() {
        return reservationCustomerPhone;
    }

    public void setReservationCustomerPhone(String reservationCustomerPhone) {
        this.reservationCustomerPhone = reservationCustomerPhone;
    }

    public LocalDate getReservationReservationDate() {
        return reservationReservationDate;
    }

    public void setReservationReservationDate(LocalDate reservationReservationDate) {
        this.reservationReservationDate = reservationReservationDate;
    }

    public String getReservationUserFirstName() {
        return reservationUserFirstName;
    }

    public void setReservationUserFirstName(String reservationUserFirstName) {
        this.reservationUserFirstName = reservationUserFirstName;
    }

    public String getReservationUserPhone() {
        return reservationUserPhone;
    }

    public void setReservationUserPhone(String reservationUserPhone) {
        this.reservationUserPhone = reservationUserPhone;
    }

    public Long getHallId() {
        return hallId;
    }

    public void setHallId(Long hallId) {
        this.hallId = hallId;
    }

    public Float getExtrasTotal() {
        return extrasTotal;
    }

    public void setExtrasTotal(Float extrasTotal) {
        this.extrasTotal = extrasTotal;
    }
}
