package ly.com.farhaty.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.*;
import ly.com.farhaty.domain.enumeration.AttendeesType;
import ly.com.farhaty.domain.enumeration.ReservationStatus;

/**
 * Criteria class for the {@link ly.com.farhaty.domain.Reservation} entity. This class is used
 * in {@link ly.com.farhaty.web.rest.ReservationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /reservations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ReservationCriteria implements Serializable, Criteria {
    /**
     * Class for filtering AttendeesType
     */
    public static class AttendeesTypeFilter extends Filter<AttendeesType> {

        public AttendeesTypeFilter() {
        }

        public AttendeesTypeFilter(AttendeesTypeFilter filter) {
            super(filter);
        }

        @Override
        public AttendeesTypeFilter copy() {
            return new AttendeesTypeFilter(this);
        }

    }
    /**
     * Class for filtering ReservationStatus
     */
    public static class ReservationStatusFilter extends Filter<ReservationStatus> {

        public ReservationStatusFilter() {
        }

        public ReservationStatusFilter(ReservationStatusFilter filter) {
            super(filter);
        }

        @Override
        public ReservationStatusFilter copy() {
            return new ReservationStatusFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private AttendeesTypeFilter attendeesType;

    private ReservationStatusFilter reservationStatus;

    private IntegerFilter attendeesNo;

    private FloatFilter total;

    private StringFilter notes;

    private LongFilter appointmentId;

    private LongFilter invoiceId;

    private LongFilter userId;

    private LongFilter extraId;

    private LongFilter typeId;

    private LongFilter hallId;

    private LongFilter roomId;

    private LocalDateFilter reservationDate;


    public ReservationCriteria(){
    }

    public ReservationCriteria(ReservationCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.attendeesType = other.attendeesType == null ? null : other.attendeesType.copy();
        this.reservationStatus = other.reservationStatus == null ? null : other.reservationStatus.copy();
        this.attendeesNo = other.attendeesNo == null ? null : other.attendeesNo.copy();
        this.total = other.total == null ? null : other.total.copy();
        this.notes = other.notes == null ? null : other.notes.copy();
        this.appointmentId = other.appointmentId == null ? null : other.appointmentId.copy();
        this.invoiceId = other.invoiceId == null ? null : other.invoiceId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.extraId = other.extraId == null ? null : other.extraId.copy();
        this.typeId = other.typeId == null ? null : other.typeId.copy();
        this.hallId = other.hallId == null ? null : other.hallId.copy();
        this.roomId = other.roomId == null ? null : other.roomId.copy();
        this.reservationDate = other.reservationDate == null ? null : other.reservationDate.copy();
    }

    @Override
    public ReservationCriteria copy() {
        return new ReservationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public AttendeesTypeFilter getAttendeesType() {
        return attendeesType;
    }

    public void setAttendeesType(AttendeesTypeFilter attendeesType) {
        this.attendeesType = attendeesType;
    }

    public ReservationStatusFilter getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatusFilter reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public IntegerFilter getAttendeesNo() {
        return attendeesNo;
    }

    public void setAttendeesNo(IntegerFilter attendeesNo) {
        this.attendeesNo = attendeesNo;
    }

    public FloatFilter getTotal() {
        return total;
    }

    public void setTotal(FloatFilter total) {
        this.total = total;
    }

    public StringFilter getNotes() {
        return notes;
    }

    public void setNotes(StringFilter notes) {
        this.notes = notes;
    }

    public LongFilter getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(LongFilter appointmentId) {
        this.appointmentId = appointmentId;
    }

    public LongFilter getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(LongFilter invoiceId) {
        this.invoiceId = invoiceId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getExtraId() {
        return extraId;
    }

    public void setExtraId(LongFilter extraId) {
        this.extraId = extraId;
    }

    public LongFilter getTypeId() {
        return typeId;
    }

    public void setTypeId(LongFilter typeId) {
        this.typeId = typeId;
    }

    public LongFilter getHallId() {
        return hallId;
    }

    public void setHallId(LongFilter hallId) {
        this.hallId = hallId;
    }

    public LongFilter getRoomId() {
        return roomId;
    }

    public void setRoomId(LongFilter roomId) {
        this.roomId = roomId;
    }

    public LocalDateFilter getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(LocalDateFilter reservationDate) {
        this.reservationDate = reservationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ReservationCriteria that = (ReservationCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(attendeesType, that.attendeesType) &&
            Objects.equals(reservationStatus, that.reservationStatus) &&
            Objects.equals(attendeesNo, that.attendeesNo) &&
            Objects.equals(total, that.total) &&
            Objects.equals(notes, that.notes) &&
            Objects.equals(appointmentId, that.appointmentId) &&
            Objects.equals(invoiceId, that.invoiceId) &&
            Objects.equals(userId, that.userId) &&
                Objects.equals(extraId, that.extraId) &&
                Objects.equals(hallId, that.hallId) &&
                Objects.equals(roomId, that.roomId) &&
                Objects.equals(reservationDate, that.reservationDate) &&
            Objects.equals(typeId, that.typeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        attendeesType,
        reservationStatus,
        attendeesNo,
        total,
        notes,
        appointmentId,
        invoiceId,
        userId,
        extraId,
        typeId,
            hallId,
            roomId,
            reservationDate
        );
    }

    @Override
    public String toString() {
        return "ReservationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (attendeesType != null ? "attendeesType=" + attendeesType + ", " : "") +
                (reservationStatus != null ? "reservationStatus=" + reservationStatus + ", " : "") +
                (attendeesNo != null ? "attendeesNo=" + attendeesNo + ", " : "") +
                (total != null ? "total=" + total + ", " : "") +
                (notes != null ? "notes=" + notes + ", " : "") +
                (appointmentId != null ? "appointmentId=" + appointmentId + ", " : "") +
                (invoiceId != null ? "invoiceId=" + invoiceId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (extraId != null ? "extraId=" + extraId + ", " : "") +
            (typeId != null ? "typeId=" + typeId + ", " : "") +
            (hallId != null ? "hallId=" + hallId + ", " : "") +
            "}";
    }

}
