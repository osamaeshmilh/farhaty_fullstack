package ly.com.farhaty.service.dto;
import javax.persistence.Column;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.Objects;
import ly.com.farhaty.domain.enumeration.AttendeesType;
import ly.com.farhaty.domain.enumeration.Period;
import ly.com.farhaty.domain.enumeration.ReservationStatus;
import java.time.LocalDate;

/**
 * A DTO for the {@link ly.com.farhaty.domain.Reservation} entity.
 */
public class ReservationDTO implements Serializable {

    private Long id;

    private AttendeesType attendeesType;

    @NotNull
    private ReservationStatus reservationStatus;

    private Integer attendeesNo;

    private Float total;

    private Float roomTotal;

    private Float typeTotal;

    private Float extrasTotal;

    private String notes;

    private Period period;

    private Long userId;

    private String userLogin;

    private Set<ExtraDTO> extras = new HashSet<>();

    private Set<InvoiceDTO> invoices = new HashSet<>();

    private Long typeId;

    private String typeName;

    private LocalDate reservationDate;

    private String customerName;

    private String customerPhone;

    private Long hallId;

    private String hallName;

    private Long roomId;

    private String roomName;

    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

    private String userFirstName;

    private String userPhone;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AttendeesType getAttendeesType() {
        return attendeesType;
    }

    public void setAttendeesType(AttendeesType attendeesType) {
        this.attendeesType = attendeesType;
    }

    public ReservationStatus getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatus reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public Integer getAttendeesNo() {
        return attendeesNo;
    }

    public void setAttendeesNo(Integer attendeesNo) {
        this.attendeesNo = attendeesNo;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Set<ExtraDTO> getExtras() {
        return extras;
    }

    public void setExtras(Set<ExtraDTO> extras) {
        this.extras = extras;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public LocalDate getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(LocalDate reservationDate) {
        this.reservationDate = reservationDate;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Long getHallId() {
        return hallId;
    }

    public void setHallId(Long hallId) {
        this.hallId = hallId;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Set<InvoiceDTO> getInvoices() {
        return invoices;
    }

    public void setInvoices(Set<InvoiceDTO> invoices) {
        this.invoices = invoices;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public Float getRoomTotal() {
        return roomTotal;
    }

    public void setRoomTotal(Float roomTotal) {
        this.roomTotal = roomTotal;
    }

    public Float getTypeTotal() {
        return typeTotal;
    }

    public void setTypeTotal(Float typeTotal) {
        this.typeTotal = typeTotal;
    }

    public Float getExtrasTotal() {
        return extrasTotal;
    }

    public void setExtrasTotal(Float extrasTotal) {
        this.extrasTotal = extrasTotal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReservationDTO reservationDTO = (ReservationDTO) o;
        if (reservationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reservationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }
}
