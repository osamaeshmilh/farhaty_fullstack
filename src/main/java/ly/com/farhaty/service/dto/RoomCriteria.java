package ly.com.farhaty.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ly.com.farhaty.domain.Room} entity. This class is used
 * in {@link ly.com.farhaty.web.rest.RoomResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /rooms?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RoomCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private IntegerFilter maxAttendees;

    private FloatFilter price;

    private BooleanFilter active;

    private LongFilter appointmentId;

    private LongFilter hallId;

    public RoomCriteria(){
    }

    public RoomCriteria(RoomCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.maxAttendees = other.maxAttendees == null ? null : other.maxAttendees.copy();
        this.price = other.price == null ? null : other.price.copy();
        this.active = other.active == null ? null : other.active.copy();
        this.appointmentId = other.appointmentId == null ? null : other.appointmentId.copy();
        this.hallId = other.hallId == null ? null : other.hallId.copy();
    }

    @Override
    public RoomCriteria copy() {
        return new RoomCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public IntegerFilter getMaxAttendees() {
        return maxAttendees;
    }

    public void setMaxAttendees(IntegerFilter maxAttendees) {
        this.maxAttendees = maxAttendees;
    }

    public FloatFilter getPrice() {
        return price;
    }

    public void setPrice(FloatFilter price) {
        this.price = price;
    }

    public BooleanFilter getActive() {
        return active;
    }

    public void setActive(BooleanFilter active) {
        this.active = active;
    }

    public LongFilter getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(LongFilter appointmentId) {
        this.appointmentId = appointmentId;
    }

    public LongFilter getHallId() {
        return hallId;
    }

    public void setHallId(LongFilter hallId) {
        this.hallId = hallId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RoomCriteria that = (RoomCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(maxAttendees, that.maxAttendees) &&
            Objects.equals(price, that.price) &&
            Objects.equals(active, that.active) &&
            Objects.equals(appointmentId, that.appointmentId) &&
            Objects.equals(hallId, that.hallId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        maxAttendees,
        price,
        active,
        appointmentId,
        hallId
        );
    }

    @Override
    public String toString() {
        return "RoomCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (maxAttendees != null ? "maxAttendees=" + maxAttendees + ", " : "") +
                (price != null ? "price=" + price + ", " : "") +
                (active != null ? "active=" + active + ", " : "") +
                (appointmentId != null ? "appointmentId=" + appointmentId + ", " : "") +
                (hallId != null ? "hallId=" + hallId + ", " : "") +
            "}";
    }

}
