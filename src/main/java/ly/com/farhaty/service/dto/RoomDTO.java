package ly.com.farhaty.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link ly.com.farhaty.domain.Room} entity.
 */
public class RoomDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private Integer maxAttendees;

    private Float price;

    @NotNull
    private Boolean active;

    private Boolean canReserve;

    private LocalDate requestedDate;

    private List<Object[]> reservedDates;

    private Long hallId;

    private String hallName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxAttendees() {
        return maxAttendees;
    }

    public void setMaxAttendees(Integer maxAttendees) {
        this.maxAttendees = maxAttendees;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getHallId() {
        return hallId;
    }

    public void setHallId(Long hallId) {
        this.hallId = hallId;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public Boolean getCanReserve() {
        return canReserve;
    }

    public void setCanReserve(Boolean canReserve) {
        this.canReserve = canReserve;
    }

    public LocalDate getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(LocalDate requestedDate) {
        this.requestedDate = requestedDate;
    }

    public List<Object[]> getReservedDates() {
        return reservedDates;
    }

    public void setReservedDates(List<Object[]> reservedDates) {
        this.reservedDates = reservedDates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RoomDTO roomDTO = (RoomDTO) o;
        if (roomDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), roomDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RoomDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", maxAttendees=" + getMaxAttendees() +
            ", price=" + getPrice() +
            ", active='" + isActive() + "'" +
            ", hall=" + getHallId() +
            ", hall='" + getHallName() + "'" +
            "}";
    }
}
