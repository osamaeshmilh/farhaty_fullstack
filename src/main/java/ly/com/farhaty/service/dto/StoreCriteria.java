package ly.com.farhaty.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ly.com.farhaty.domain.Store} entity. This class is used
 * in {@link ly.com.farhaty.web.rest.StoreResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /stores?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class StoreCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter description;

    private StringFilter address;

    private FloatFilter lat;

    private FloatFilter lng;

    private StringFilter phone;

    private StringFilter facebook;

    private StringFilter whatsup;

    private StringFilter viper;

    private StringFilter twitter;

    private StringFilter instagram;

    private StringFilter snapchat;

    private BooleanFilter vip;

    private LongFilter userId;

    private LongFilter storeImageId;

    private LongFilter categoryId;

    private BooleanFilter active;

    public StoreCriteria(){
    }

    public StoreCriteria(StoreCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.address = other.address == null ? null : other.address.copy();
        this.lat = other.lat == null ? null : other.lat.copy();
        this.lng = other.lng == null ? null : other.lng.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.facebook = other.facebook == null ? null : other.facebook.copy();
        this.whatsup = other.whatsup == null ? null : other.whatsup.copy();
        this.viper = other.viper == null ? null : other.viper.copy();
        this.twitter = other.twitter == null ? null : other.twitter.copy();
        this.instagram = other.instagram == null ? null : other.instagram.copy();
        this.snapchat = other.snapchat == null ? null : other.snapchat.copy();
        this.vip = other.vip == null ? null : other.vip.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.storeImageId = other.storeImageId == null ? null : other.storeImageId.copy();
        this.categoryId = other.categoryId == null ? null : other.categoryId.copy();
        this.active = other.active == null ? null : other.active.copy();

    }

    @Override
    public StoreCriteria copy() {
        return new StoreCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getAddress() {
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public FloatFilter getLat() {
        return lat;
    }

    public void setLat(FloatFilter lat) {
        this.lat = lat;
    }

    public FloatFilter getLng() {
        return lng;
    }

    public void setLng(FloatFilter lng) {
        this.lng = lng;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getFacebook() {
        return facebook;
    }

    public void setFacebook(StringFilter facebook) {
        this.facebook = facebook;
    }

    public StringFilter getWhatsup() {
        return whatsup;
    }

    public void setWhatsup(StringFilter whatsup) {
        this.whatsup = whatsup;
    }

    public StringFilter getViper() {
        return viper;
    }

    public void setViper(StringFilter viper) {
        this.viper = viper;
    }

    public StringFilter getTwitter() {
        return twitter;
    }

    public void setTwitter(StringFilter twitter) {
        this.twitter = twitter;
    }

    public StringFilter getInstagram() {
        return instagram;
    }

    public void setInstagram(StringFilter instagram) {
        this.instagram = instagram;
    }

    public StringFilter getSnapchat() {
        return snapchat;
    }

    public void setSnapchat(StringFilter snapchat) {
        this.snapchat = snapchat;
    }

    public BooleanFilter getVip() {
        return vip;
    }

    public void setVip(BooleanFilter vip) {
        this.vip = vip;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getStoreImageId() {
        return storeImageId;
    }

    public void setStoreImageId(LongFilter storeImageId) {
        this.storeImageId = storeImageId;
    }

    public LongFilter getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(LongFilter categoryId) {
        this.categoryId = categoryId;
    }

    public BooleanFilter getActive() {
        return active;
    }

    public void setActive(BooleanFilter active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final StoreCriteria that = (StoreCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(description, that.description) &&
            Objects.equals(address, that.address) &&
            Objects.equals(lat, that.lat) &&
            Objects.equals(lng, that.lng) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(facebook, that.facebook) &&
            Objects.equals(whatsup, that.whatsup) &&
            Objects.equals(viper, that.viper) &&
            Objects.equals(twitter, that.twitter) &&
            Objects.equals(instagram, that.instagram) &&
            Objects.equals(snapchat, that.snapchat) &&
            Objects.equals(vip, that.vip) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(storeImageId, that.storeImageId) &&
                Objects.equals(active, that.active) &&
                Objects.equals(categoryId, that.categoryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        description,
        address,
        lat,
        lng,
        phone,
        facebook,
        whatsup,
        viper,
        twitter,
        instagram,
        snapchat,
        vip,
        userId,
        storeImageId,
        categoryId,
            active
        );
    }

    @Override
    public String toString() {
        return "StoreCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (address != null ? "address=" + address + ", " : "") +
                (lat != null ? "lat=" + lat + ", " : "") +
                (lng != null ? "lng=" + lng + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (facebook != null ? "facebook=" + facebook + ", " : "") +
                (whatsup != null ? "whatsup=" + whatsup + ", " : "") +
                (viper != null ? "viper=" + viper + ", " : "") +
                (twitter != null ? "twitter=" + twitter + ", " : "") +
                (instagram != null ? "instagram=" + instagram + ", " : "") +
                (snapchat != null ? "snapchat=" + snapchat + ", " : "") +
                (vip != null ? "vip=" + vip + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (storeImageId != null ? "storeImageId=" + storeImageId + ", " : "") +
            (active != null ? "active=" + active + ", " : "") +
            (categoryId != null ? "categoryId=" + categoryId + ", " : "") +
            "}";
    }

}
