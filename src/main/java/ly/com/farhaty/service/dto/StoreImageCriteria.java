package ly.com.farhaty.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ly.com.farhaty.domain.StoreImage} entity. This class is used
 * in {@link ly.com.farhaty.web.rest.StoreImageResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /store-images?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class StoreImageCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter storeId;

    public StoreImageCriteria(){
    }

    public StoreImageCriteria(StoreImageCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.storeId = other.storeId == null ? null : other.storeId.copy();
    }

    @Override
    public StoreImageCriteria copy() {
        return new StoreImageCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getStoreId() {
        return storeId;
    }

    public void setStoreId(LongFilter storeId) {
        this.storeId = storeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final StoreImageCriteria that = (StoreImageCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(storeId, that.storeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        storeId
        );
    }

    @Override
    public String toString() {
        return "StoreImageCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (storeId != null ? "storeId=" + storeId + ", " : "") +
            "}";
    }

}
