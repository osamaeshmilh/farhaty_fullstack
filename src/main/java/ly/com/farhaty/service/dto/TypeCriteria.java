package ly.com.farhaty.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ly.com.farhaty.domain.Type} entity. This class is used
 * in {@link ly.com.farhaty.web.rest.TypeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /types?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TypeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private FloatFilter satPrice;

    private FloatFilter sunPrice;

    private FloatFilter monPrice;

    private FloatFilter tuePrice;

    private FloatFilter wedPrice;

    private FloatFilter thrPrice;

    private FloatFilter friPrice;

    private BooleanFilter active;

    private LongFilter reservationId;

    private LongFilter hallId;

    public TypeCriteria(){
    }

    public TypeCriteria(TypeCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.satPrice = other.satPrice == null ? null : other.satPrice.copy();
        this.sunPrice = other.sunPrice == null ? null : other.sunPrice.copy();
        this.monPrice = other.monPrice == null ? null : other.monPrice.copy();
        this.tuePrice = other.tuePrice == null ? null : other.tuePrice.copy();
        this.wedPrice = other.wedPrice == null ? null : other.wedPrice.copy();
        this.thrPrice = other.thrPrice == null ? null : other.thrPrice.copy();
        this.friPrice = other.friPrice == null ? null : other.friPrice.copy();
        this.active = other.active == null ? null : other.active.copy();
        this.reservationId = other.reservationId == null ? null : other.reservationId.copy();
        this.hallId = other.hallId == null ? null : other.hallId.copy();
    }

    @Override
    public TypeCriteria copy() {
        return new TypeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public BooleanFilter getActive() {
        return active;
    }

    public void setActive(BooleanFilter active) {
        this.active = active;
    }

    public LongFilter getReservationId() {
        return reservationId;
    }

    public void setReservationId(LongFilter reservationId) {
        this.reservationId = reservationId;
    }

    public LongFilter getHallId() {
        return hallId;
    }

    public void setHallId(LongFilter hallId) {
        this.hallId = hallId;
    }

    public FloatFilter getSatPrice() {
        return satPrice;
    }

    public void setSatPrice(FloatFilter satPrice) {
        this.satPrice = satPrice;
    }

    public FloatFilter getSunPrice() {
        return sunPrice;
    }

    public void setSunPrice(FloatFilter sunPrice) {
        this.sunPrice = sunPrice;
    }

    public FloatFilter getMonPrice() {
        return monPrice;
    }

    public void setMonPrice(FloatFilter monPrice) {
        this.monPrice = monPrice;
    }

    public FloatFilter getTuePrice() {
        return tuePrice;
    }

    public void setTuePrice(FloatFilter tuePrice) {
        this.tuePrice = tuePrice;
    }

    public FloatFilter getWedPrice() {
        return wedPrice;
    }

    public void setWedPrice(FloatFilter wedPrice) {
        this.wedPrice = wedPrice;
    }

    public FloatFilter getThrPrice() {
        return thrPrice;
    }

    public void setThrPrice(FloatFilter thrPrice) {
        this.thrPrice = thrPrice;
    }

    public FloatFilter getFriPrice() {
        return friPrice;
    }

    public void setFriPrice(FloatFilter friPrice) {
        this.friPrice = friPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TypeCriteria that = (TypeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
                Objects.equals(satPrice, that.satPrice) &&
                Objects.equals(sunPrice, that.sunPrice) &&
                Objects.equals(monPrice, that.monPrice) &&
                Objects.equals(tuePrice, that.tuePrice) &&
                Objects.equals(wedPrice, that.wedPrice) &&
                Objects.equals(thrPrice, that.thrPrice) &&
                Objects.equals(friPrice, that.friPrice) &&
            Objects.equals(active, that.active) &&
            Objects.equals(reservationId, that.reservationId) &&
            Objects.equals(hallId, that.hallId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        active,
        satPrice,
        sunPrice,
        monPrice,
        tuePrice,
        wedPrice,
        thrPrice,
        friPrice,
        reservationId,
        hallId
        );
    }



}
