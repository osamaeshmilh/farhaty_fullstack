package ly.com.farhaty.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ly.com.farhaty.domain.Type} entity.
 */
public class TypeDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private Float satPrice;

    private Float sunPrice;

    private Float monPrice;

    private Float tuePrice;

    private Float wedPrice;

    private Float thrPrice;

    private Float friPrice;

    @NotNull
    private Boolean active;


    private Long hallId;

    private String hallName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getHallId() {
        return hallId;
    }

    public void setHallId(Long hallId) {
        this.hallId = hallId;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public Float getSatPrice() {
        return satPrice;
    }

    public void setSatPrice(Float satPrice) {
        this.satPrice = satPrice;
    }

    public Float getSunPrice() {
        return sunPrice;
    }

    public void setSunPrice(Float sunPrice) {
        this.sunPrice = sunPrice;
    }

    public Float getMonPrice() {
        return monPrice;
    }

    public void setMonPrice(Float monPrice) {
        this.monPrice = monPrice;
    }

    public Float getTuePrice() {
        return tuePrice;
    }

    public void setTuePrice(Float tuePrice) {
        this.tuePrice = tuePrice;
    }

    public Float getWedPrice() {
        return wedPrice;
    }

    public void setWedPrice(Float wedPrice) {
        this.wedPrice = wedPrice;
    }

    public Float getThrPrice() {
        return thrPrice;
    }

    public void setThrPrice(Float thrPrice) {
        this.thrPrice = thrPrice;
    }

    public Float getFriPrice() {
        return friPrice;
    }

    public void setFriPrice(Float friPrice) {
        this.friPrice = friPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TypeDTO typeDTO = (TypeDTO) o;
        if (typeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), typeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TypeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", active='" + isActive() + "'" +
            ", hall=" + getHallId() +
            ", hall='" + getHallName() + "'" +
            "}";
    }
}
