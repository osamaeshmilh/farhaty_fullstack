package ly.com.farhaty.service.impl;

import ly.com.farhaty.service.RevenueService;
import ly.com.farhaty.domain.Revenue;
import ly.com.farhaty.repository.RevenueRepository;
import ly.com.farhaty.service.dto.RevenueDTO;
import ly.com.farhaty.service.mapper.RevenueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Revenue}.
 */
@Service
@Transactional
public class RevenueServiceImpl implements RevenueService {

    private final Logger log = LoggerFactory.getLogger(RevenueServiceImpl.class);

    private final RevenueRepository revenueRepository;

    private final RevenueMapper revenueMapper;

    public RevenueServiceImpl(RevenueRepository revenueRepository, RevenueMapper revenueMapper) {
        this.revenueRepository = revenueRepository;
        this.revenueMapper = revenueMapper;
    }

    /**
     * Save a revenue.
     *
     * @param revenueDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RevenueDTO save(RevenueDTO revenueDTO) {
        log.debug("Request to save Revenue : {}", revenueDTO);
        Revenue revenue = revenueMapper.toEntity(revenueDTO);
        revenue = revenueRepository.save(revenue);
        return revenueMapper.toDto(revenue);
    }

    /**
     * Get all the revenues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RevenueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Revenues");
        return revenueRepository.findAll(pageable)
            .map(revenueMapper::toDto);
    }


    /**
     * Get one revenue by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RevenueDTO> findOne(Long id) {
        log.debug("Request to get Revenue : {}", id);
        return revenueRepository.findById(id)
            .map(revenueMapper::toDto);
    }

    /**
     * Delete the revenue by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Revenue : {}", id);
        revenueRepository.deleteById(id);
    }
}
