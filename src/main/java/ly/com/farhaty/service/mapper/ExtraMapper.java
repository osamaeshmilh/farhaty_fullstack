package ly.com.farhaty.service.mapper;

import ly.com.farhaty.domain.*;
import ly.com.farhaty.service.dto.ExtraDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Extra} and its DTO {@link ExtraDTO}.
 */
@Mapper(componentModel = "spring", uses = {HallMapper.class})
public interface ExtraMapper extends EntityMapper<ExtraDTO, Extra> {

    @Mapping(source = "hall.id", target = "hallId")
    @Mapping(source = "hall.name", target = "hallName")
    ExtraDTO toDto(Extra extra);

    @Mapping(source = "hallId", target = "hall")
    @Mapping(target = "extras", ignore = true)
    @Mapping(target = "removeExtras", ignore = true)
    Extra toEntity(ExtraDTO extraDTO);

    default Extra fromId(Long id) {
        if (id == null) {
            return null;
        }
        Extra extra = new Extra();
        extra.setId(id);
        return extra;
    }
}
