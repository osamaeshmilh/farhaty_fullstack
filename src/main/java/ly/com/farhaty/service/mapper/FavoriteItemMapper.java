package ly.com.farhaty.service.mapper;

import ly.com.farhaty.domain.*;
import ly.com.farhaty.service.dto.FavoriteItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link FavoriteItem} and its DTO {@link FavoriteItemDTO}.
 */
@Mapper(componentModel = "spring", uses = {ItemMapper.class, UserMapper.class})
public interface FavoriteItemMapper extends EntityMapper<FavoriteItemDTO, FavoriteItem> {

    @Mapping(source = "item.id", target = "itemId")
    @Mapping(source = "item.name", target = "itemName")
    @Mapping(source = "user.id", target = "userId")
    FavoriteItemDTO toDto(FavoriteItem favoriteItem);

    @Mapping(source = "itemId", target = "item")
    @Mapping(source = "userId", target = "user")
    FavoriteItem toEntity(FavoriteItemDTO favoriteItemDTO);

    default FavoriteItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        FavoriteItem favoriteItem = new FavoriteItem();
        favoriteItem.setId(id);
        return favoriteItem;
    }
}
