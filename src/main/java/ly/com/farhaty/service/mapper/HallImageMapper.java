package ly.com.farhaty.service.mapper;

import ly.com.farhaty.domain.*;
import ly.com.farhaty.service.dto.HallImageAllDTO;
import ly.com.farhaty.service.dto.HallImageDTO;

import org.mapstruct.*;

import java.util.List;

/**
 * Mapper for the entity {@link HallImage} and its DTO {@link HallImageDTO}.
 */
@Mapper(componentModel = "spring", uses = {HallMapper.class, RoomMapper.class})
public interface HallImageMapper extends EntityMapper<HallImageDTO, HallImage> {

    @Mapping(source = "hall.id", target = "hallId")
    @Mapping(source = "room.id", target = "roomId")
    @Mapping(source = "room.name", target = "roomName")
    HallImageDTO toDto(HallImage hallImage);

    @Mapping(source = "hallId", target = "hall")
    @Mapping(source = "roomId", target = "room")
    HallImage toEntity(HallImageDTO hallImageDTO);

    default HallImage fromId(Long id) {
        if (id == null) {
            return null;
        }
        HallImage hallImage = new HallImage();
        hallImage.setId(id);
        return hallImage;
    }

    @Mapping(source = "hall.id", target = "hallId")
    @Mapping(source = "room.id", target = "roomId")
    HallImageAllDTO toAllDto(HallImage hallImage);
}
