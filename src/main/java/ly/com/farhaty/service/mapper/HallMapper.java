package ly.com.farhaty.service.mapper;

import ly.com.farhaty.domain.*;
import ly.com.farhaty.service.dto.HallAllDTO;
import ly.com.farhaty.service.dto.HallDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Hall} and its DTO {@link HallDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface HallMapper extends EntityMapper<HallDTO, Hall> {

    @Mapping(source = "user.id", target = "userId")
    HallDTO toDto(Hall hall);

    @Mapping(source = "userId", target = "user")
    @Mapping(target = "appointments", ignore = true)
    @Mapping(target = "removeAppointment", ignore = true)
    @Mapping(target = "types", ignore = true)
    @Mapping(target = "removeType", ignore = true)
    @Mapping(target = "rooms", ignore = true)
    @Mapping(target = "removeRoom", ignore = true)
    @Mapping(target = "extras", ignore = true)
    @Mapping(target = "removeExtra", ignore = true)
    Hall toEntity(HallDTO hallDTO);

    default Hall fromId(Long id) {
        if (id == null) {
            return null;
        }
        Hall hall = new Hall();
        hall.setId(id);
        return hall;
    }

    @Mapping(source = "user.id", target = "userId")
    HallAllDTO toAllDto(Hall hall);
}
