package ly.com.farhaty.service.mapper;

import ly.com.farhaty.domain.*;
import ly.com.farhaty.service.dto.InvoiceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Invoice} and its DTO {@link InvoiceDTO}.
 */
@Mapper(componentModel = "spring", uses = {ReservationMapper.class})
public interface InvoiceMapper extends EntityMapper<InvoiceDTO, Invoice> {

    @Mapping(source = "reservation.id", target = "reservationId")
    @Mapping(source = "reservation.total", target = "reservationTotal")
    @Mapping(source = "reservation.extrasTotal", target = "extrasTotal")
    @Mapping(source = "reservation.customerName", target = "reservationCustomerName")
    @Mapping(source = "reservation.customerPhone", target = "reservationCustomerPhone")
    @Mapping(source = "reservation.reservationDate", target = "reservationReservationDate")
    @Mapping(source = "reservation.user.firstName", target = "reservationUserFirstName")
    @Mapping(source = "reservation.user.phone", target = "reservationUserPhone")
    @Mapping(source = "reservation.hall.name", target = "hallName")
    @Mapping(source = "reservation.hall.id", target = "hallId")
    InvoiceDTO toDto(Invoice invoice);

    @Mapping(source = "reservationId", target = "reservation")
    Invoice toEntity(InvoiceDTO invoiceDTO);

    default Invoice fromId(Long id) {
        if (id == null) {
            return null;
        }
        Invoice invoice = new Invoice();
        invoice.setId(id);
        return invoice;
    }
}
