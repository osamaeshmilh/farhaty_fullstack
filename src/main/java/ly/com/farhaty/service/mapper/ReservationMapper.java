package ly.com.farhaty.service.mapper;

import ly.com.farhaty.domain.*;
import ly.com.farhaty.service.dto.ReservationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Reservation} and its DTO {@link ReservationDTO}.
 */
@Mapper(componentModel = "spring", uses = {HallMapper.class, RoomMapper.class, UserMapper.class, ExtraMapper.class, TypeMapper.class})
public interface ReservationMapper extends EntityMapper<ReservationDTO, Reservation> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "user.firstName", target = "userFirstName")
    @Mapping(source = "user.phone", target = "userPhone")
    @Mapping(source = "type.id", target = "typeId")
    @Mapping(source = "type.name", target = "typeName")
    @Mapping(source = "hall.id", target = "hallId")
    @Mapping(source = "hall.name", target = "hallName")
    @Mapping(source = "room.id", target = "roomId")
    @Mapping(source = "room.name", target = "roomName")
    ReservationDTO toDto(Reservation reservation);

    @Mapping(target = "invoices", ignore = true)
    @Mapping(target = "removeInvoice", ignore = true)
    @Mapping(source = "userId", target = "user")
    @Mapping(target = "removeExtra", ignore = true)
    @Mapping(source = "typeId", target = "type")
    @Mapping(source = "hallId", target = "hall")
    @Mapping(source = "roomId", target = "room")
    Reservation toEntity(ReservationDTO reservationDTO);

    default Reservation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Reservation reservation = new Reservation();
        reservation.setId(id);
        return reservation;
    }
}
