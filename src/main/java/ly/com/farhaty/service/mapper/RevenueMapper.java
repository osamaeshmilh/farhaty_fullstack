package ly.com.farhaty.service.mapper;

import ly.com.farhaty.domain.*;
import ly.com.farhaty.service.dto.RevenueDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Revenue} and its DTO {@link RevenueDTO}.
 */
@Mapper(componentModel = "spring", uses = {HallMapper.class})
public interface RevenueMapper extends EntityMapper<RevenueDTO, Revenue> {

    @Mapping(source = "hall.id", target = "hallId")
    @Mapping(source = "hall.name", target = "hallName")
    RevenueDTO toDto(Revenue revenue);

    @Mapping(source = "hallId", target = "hall")
    Revenue toEntity(RevenueDTO revenueDTO);

    default Revenue fromId(Long id) {
        if (id == null) {
            return null;
        }
        Revenue revenue = new Revenue();
        revenue.setId(id);
        return revenue;
    }
}
