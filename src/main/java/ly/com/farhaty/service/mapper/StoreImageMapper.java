package ly.com.farhaty.service.mapper;

import ly.com.farhaty.domain.*;
import ly.com.farhaty.service.dto.StoreImageAllDTO;
import ly.com.farhaty.service.dto.StoreImageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link StoreImage} and its DTO {@link StoreImageDTO}.
 */
@Mapper(componentModel = "spring", uses = {StoreMapper.class})
public interface StoreImageMapper extends EntityMapper<StoreImageDTO, StoreImage> {

    @Mapping(source = "store.id", target = "storeId")
    @Mapping(source = "store.name", target = "storeName")
    StoreImageDTO toDto(StoreImage storeImage);

    @Mapping(source = "storeId", target = "store")
    StoreImage toEntity(StoreImageDTO storeImageDTO);

    default StoreImage fromId(Long id) {
        if (id == null) {
            return null;
        }
        StoreImage storeImage = new StoreImage();
        storeImage.setId(id);
        return storeImage;
    }

    @Mapping(source = "store.id", target = "storeId")
    @Mapping(source = "store.name", target = "storeName")
    StoreImageAllDTO toAllDto(StoreImage storeImage);
}
