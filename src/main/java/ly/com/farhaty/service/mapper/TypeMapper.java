package ly.com.farhaty.service.mapper;

import ly.com.farhaty.domain.*;
import ly.com.farhaty.service.dto.TypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Type} and its DTO {@link TypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {HallMapper.class})
public interface TypeMapper extends EntityMapper<TypeDTO, Type> {

    @Mapping(source = "hall.id", target = "hallId")
    @Mapping(source = "hall.name", target = "hallName")
    TypeDTO toDto(Type type);

    @Mapping(source = "hallId", target = "hall")
    Type toEntity(TypeDTO typeDTO);

    default Type fromId(Long id) {
        if (id == null) {
            return null;
        }
        Type type = new Type();
        type.setId(id);
        return type;
    }
}
