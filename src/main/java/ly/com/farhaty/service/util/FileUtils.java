package ly.com.farhaty.service.util;

import com.google.common.io.Files;
import org.springframework.http.MediaType;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class FileUtils {

    private static final String uploadsDir = "/Users/osamaeshmilh/uploads/";

    public static String upload(byte[] imageBytes, String fileContentType, String name) {
        String generatedName = "";

        if (imageBytes != null) {
            try {
                if(fileContentType.equals(MediaType.IMAGE_PNG_VALUE))
                    generatedName = name +"_"+ System.currentTimeMillis() + new Random().nextInt(100) + ".png";
                else
                    generatedName = name +"_"+ System.currentTimeMillis() + new Random().nextInt(100) + ".jpg";
                String path = uploadsDir + generatedName;
                org.apache.commons.io.FileUtils.writeByteArrayToFile(new File(path), imageBytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return generatedName;
    }

    public static byte[] download(String fileName) {
        try {
            File file = new File(uploadsDir + fileName);
            return Files.toByteArray(file);
        } catch (IOException e) {
            return new byte[0];
        }
    }
}
