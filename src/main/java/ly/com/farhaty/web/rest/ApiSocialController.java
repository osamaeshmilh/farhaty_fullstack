package ly.com.farhaty.web.rest;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.json.jackson2.JacksonFactory;

import io.micrometer.core.annotation.Timed;
import ly.com.farhaty.domain.Authority;
import ly.com.farhaty.domain.User;
import ly.com.farhaty.security.AuthoritiesConstants;
import ly.com.farhaty.security.jwt.JWTFilter;
import ly.com.farhaty.security.jwt.TokenProvider;
import ly.com.farhaty.service.UserService;
import ly.com.farhaty.service.dto.UserDTO;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
public class ApiSocialController {

    private final Logger log = LoggerFactory.getLogger(ApiSocialController.class);

    private final UserService userService;
    private final TokenProvider tokenProvider;

    public ApiSocialController(UserService userService, TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
        this.userService = userService;
    }

    @PostMapping("/authenticate/appFacebook")
    @Timed
    public ResponseEntity<UserJWTController.JWTToken> authorizeClientFromFacebook(@RequestBody String token, HttpServletResponse response)
            throws IOException {
        token = token.replace("\"", "");

        String graph = "";
        try {

            String g = "https://graph.facebook.com/v6.0/me?access_token=" + token
                + "&fields=id,name,email,last_name,name_format,first_name";
            URL u = new URL(g);
            URLConnection c = u.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));
            String inputLine;
            StringBuffer b = new StringBuffer();
            while ((inputLine = in.readLine()) != null)
                b.append(inputLine + "\n");
            in.close();
            graph = b.toString();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("ERROR in getting FB graph data. " + e);
        }
        FBData data = getGraphData(graph);
        UserDTO userDto = new UserDTO();

        if(data.email == null) {
            data.email = data.firstName.toLowerCase() + "@facebook.com";
        }
        Optional<User> findUser = userService.getUserWithAuthoritiesByEmail(data.email);

        userDto.setFirstName(data.firstName + " " + data.lastName);
        userDto.setLastName(data.lastName);
        userDto.setLogin(data.email);
        userDto.setEmail(data.email);

        Set<String> authorities = new HashSet<>();
        authorities.add(AuthoritiesConstants.USER);
        userDto.setAuthorities(authorities);


        User user = findUser.orElseGet(() -> userService.createUser(userDto));

        return authenticateSocialUser(user, response);
    }

    static class FBData {
        public String firstName;
        public String lastName;
        public String email;

        boolean isValid() {
            return firstName != null && lastName != null && email != null;
        }
    }

    private FBData getGraphData(String fbGraph) {
        FBData ret = new FBData();
        try {
            JSONObject json = new JSONObject(fbGraph);

            if (json.has("email"))
                ret.email = json.getString("email");
            if (json.has("last_name"))
                ret.lastName = json.getString("last_name");
            if (json.has("last_name"))
                ret.firstName = json.getString("last_name");
        } catch (JSONException e) {
            e.printStackTrace();
            throw new RuntimeException("ERROR in parsing FB graph data. " + e);
        }
        return ret;
    }

    @PostMapping("/authenticate/appGoogle")
    @Timed
    public ResponseEntity<UserJWTController.JWTToken> authorizeClientFromGoogle(@RequestBody String token, HttpServletResponse response)
            throws IOException {


        GoogleIdToken idToken = GoogleIdToken.parse(JacksonFactory.getDefaultInstance(), token);
        GoogleIdToken.Payload payload = idToken.getPayload();
        String email = payload.getEmail();
        String name = (String) payload.get("name");
        String familyName = (String) payload.get("family_name");

        Optional<User> findUser = userService.getUserWithAuthoritiesByEmail(email);

        UserDTO userDto = new UserDTO();

        userDto.setFirstName(name);
        userDto.setLastName(familyName);
        userDto.setLogin(email);
        userDto.setEmail(email);

        Set<String> authorities = new HashSet<>();
        authorities.add(AuthoritiesConstants.USER);
        userDto.setAuthorities(authorities);

        User user = findUser.orElseGet(() -> userService.createUser(userDto));

        return authenticateSocialUser(user, response);
    }

    @PostMapping("/authenticate/appApple")
    @Timed
    public ResponseEntity authorizeClientFromApple(@RequestBody String authCode, HttpServletResponse response)
        throws Exception {

        //IdTokenPayload idTokenPayload = AppleUserUtil.appleAuth(authCode);

        Optional<User> findUser = userService.getUserWithAuthoritiesByEmail("idTokenPayload.getEmail()");

        UserDTO userDto = new UserDTO();

//        userDto.setFirstName(idTokenPayload.getEmail());
//        userDto.setLogin(idTokenPayload.getEmail());
//        userDto.setEmail(idTokenPayload.getEmail());

        try {
            User user = findUser.orElseGet(() -> userService.createUser(userDto));
            return authenticateSocialUser(user, response);

        } catch (Exception e) {
            return ResponseEntity.ok("idTokenPayload");
        }
    }

    private ResponseEntity<UserJWTController.JWTToken> authenticateSocialUser(User user, HttpServletResponse response) {
        Authentication authentication = new Authentication() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                ArrayList<SimpleGrantedAuthority> ret = new ArrayList<SimpleGrantedAuthority>();
                ret.add(new SimpleGrantedAuthority("ROLE_USER"));
                return ret;
            }

            @Override
            public Object getCredentials() {
                return null;
            }

            @Override
            public Object getDetails() {
                return null;
            }

            @Override
            public Object getPrincipal() {
                return user;
            }

            @Override
            public boolean isAuthenticated() {
                return true;
            }

            @Override
            public void setAuthenticated(boolean b) throws IllegalArgumentException {

            }

            @Override
            public String getName() {
                return user.getEmail();
            }
        };

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.createToken(authentication, true);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new UserJWTController.JWTToken(jwt), httpHeaders, HttpStatus.OK);

    }
}
