package ly.com.farhaty.web.rest;

import ly.com.farhaty.security.SecurityUtils;
import ly.com.farhaty.service.ExtraService;
import ly.com.farhaty.service.UserService;
import ly.com.farhaty.web.rest.errors.BadRequestAlertException;
import ly.com.farhaty.service.dto.ExtraDTO;
import ly.com.farhaty.service.dto.ExtraCriteria;
import ly.com.farhaty.service.ExtraQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ly.com.farhaty.domain.Extra}.
 */
@RestController
@RequestMapping("/api")
public class ExtraResource {

    private final Logger log = LoggerFactory.getLogger(ExtraResource.class);

    private static final String ENTITY_NAME = "extra";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExtraService extraService;

    private final ExtraQueryService extraQueryService;

    private final UserService userService;

    public ExtraResource(ExtraService extraService, ExtraQueryService extraQueryService, UserService userService) {
        this.extraService = extraService;
        this.extraQueryService = extraQueryService;
        this.userService = userService;
    }

    /**
     * {@code POST  /extras} : Create a new extra.
     *
     * @param extraDTO the extraDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new extraDTO, or with status {@code 400 (Bad Request)} if the extra has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/extras")
    public ResponseEntity<ExtraDTO> createExtra(@Valid @RequestBody ExtraDTO extraDTO) throws URISyntaxException {
        log.debug("REST request to save Extra : {}", extraDTO);
        if (extraDTO.getId() != null) {
            throw new BadRequestAlertException("A new extra cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExtraDTO result = extraService.save(extraDTO);
        return ResponseEntity.created(new URI("/api/extras/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /extras} : Updates an existing extra.
     *
     * @param extraDTO the extraDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated extraDTO,
     * or with status {@code 400 (Bad Request)} if the extraDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the extraDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/extras")
    public ResponseEntity<ExtraDTO> updateExtra(@Valid @RequestBody ExtraDTO extraDTO) throws URISyntaxException {
        log.debug("REST request to update Extra : {}", extraDTO);
        if (extraDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ExtraDTO result = extraService.save(extraDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, extraDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /extras} : get all the extras.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of extras in body.
     */
    @GetMapping("/extras")
    public ResponseEntity<List<ExtraDTO>> getAllExtras(ExtraCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Extras by criteria: {}", criteria);

        Page<ExtraDTO> page;
        if(SecurityUtils.isCurrentUserInRole("ROLE_HALL")){
            page = extraService.findByUser(userService.getLoggedInUser().get().getId(), pageable);
        }else {
            page = extraQueryService.findByCriteria(criteria, pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /extras/count} : count all the extras.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/extras/count")
    public ResponseEntity<Long> countExtras(ExtraCriteria criteria) {
        log.debug("REST request to count Extras by criteria: {}", criteria);
        return ResponseEntity.ok().body(extraQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /extras/:id} : get the "id" extra.
     *
     * @param id the id of the extraDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the extraDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/extras/{id}")
    public ResponseEntity<ExtraDTO> getExtra(@PathVariable Long id) {
        log.debug("REST request to get Extra : {}", id);
        Optional<ExtraDTO> extraDTO = extraService.findOne(id);
        return ResponseUtil.wrapOrNotFound(extraDTO);
    }

    /**
     * {@code DELETE  /extras/:id} : delete the "id" extra.
     *
     * @param id the id of the extraDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/extras/{id}")
    public ResponseEntity<Void> deleteExtra(@PathVariable Long id) {
        log.debug("REST request to delete Extra : {}", id);
        extraService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
