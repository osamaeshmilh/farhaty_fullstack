package ly.com.farhaty.web.rest;

import ly.com.farhaty.service.FavoriteItemService;
import ly.com.farhaty.web.rest.errors.BadRequestAlertException;
import ly.com.farhaty.service.dto.FavoriteItemDTO;
import ly.com.farhaty.service.dto.FavoriteItemCriteria;
import ly.com.farhaty.service.FavoriteItemQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ly.com.farhaty.domain.FavoriteItem}.
 */
@RestController
@RequestMapping("/api")
public class FavoriteItemResource {

    private final Logger log = LoggerFactory.getLogger(FavoriteItemResource.class);

    private static final String ENTITY_NAME = "favoriteItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FavoriteItemService favoriteItemService;

    private final FavoriteItemQueryService favoriteItemQueryService;

    public FavoriteItemResource(FavoriteItemService favoriteItemService, FavoriteItemQueryService favoriteItemQueryService) {
        this.favoriteItemService = favoriteItemService;
        this.favoriteItemQueryService = favoriteItemQueryService;
    }

    /**
     * {@code POST  /favorite-items} : Create a new favoriteItem.
     *
     * @param favoriteItemDTO the favoriteItemDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new favoriteItemDTO, or with status {@code 400 (Bad Request)} if the favoriteItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/favorite-items")
    public ResponseEntity<FavoriteItemDTO> createFavoriteItem(@RequestBody FavoriteItemDTO favoriteItemDTO) throws URISyntaxException {
        log.debug("REST request to save FavoriteItem : {}", favoriteItemDTO);
        if (favoriteItemDTO.getId() != null) {
            throw new BadRequestAlertException("A new favoriteItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FavoriteItemDTO result = favoriteItemService.save(favoriteItemDTO);
        return ResponseEntity.created(new URI("/api/favorite-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /favorite-items} : Updates an existing favoriteItem.
     *
     * @param favoriteItemDTO the favoriteItemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated favoriteItemDTO,
     * or with status {@code 400 (Bad Request)} if the favoriteItemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the favoriteItemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/favorite-items")
    public ResponseEntity<FavoriteItemDTO> updateFavoriteItem(@RequestBody FavoriteItemDTO favoriteItemDTO) throws URISyntaxException {
        log.debug("REST request to update FavoriteItem : {}", favoriteItemDTO);
        if (favoriteItemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FavoriteItemDTO result = favoriteItemService.save(favoriteItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, favoriteItemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /favorite-items} : get all the favoriteItems.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of favoriteItems in body.
     */
    @GetMapping("/favorite-items")
    public ResponseEntity<List<FavoriteItemDTO>> getAllFavoriteItems(FavoriteItemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get FavoriteItems by criteria: {}", criteria);
        Page<FavoriteItemDTO> page = favoriteItemQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /favorite-items/count} : count all the favoriteItems.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/favorite-items/count")
    public ResponseEntity<Long> countFavoriteItems(FavoriteItemCriteria criteria) {
        log.debug("REST request to count FavoriteItems by criteria: {}", criteria);
        return ResponseEntity.ok().body(favoriteItemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /favorite-items/:id} : get the "id" favoriteItem.
     *
     * @param id the id of the favoriteItemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the favoriteItemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/favorite-items/{id}")
    public ResponseEntity<FavoriteItemDTO> getFavoriteItem(@PathVariable Long id) {
        log.debug("REST request to get FavoriteItem : {}", id);
        Optional<FavoriteItemDTO> favoriteItemDTO = favoriteItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(favoriteItemDTO);
    }

    /**
     * {@code DELETE  /favorite-items/:id} : delete the "id" favoriteItem.
     *
     * @param id the id of the favoriteItemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/favorite-items/{id}")
    public ResponseEntity<Void> deleteFavoriteItem(@PathVariable Long id) {
        log.debug("REST request to delete FavoriteItem : {}", id);
        favoriteItemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
