package ly.com.farhaty.web.rest;

import ly.com.farhaty.service.FileService;
import ly.com.farhaty.service.util.FileUtils;
import ly.com.farhaty.service.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class FileResource {
    private final Logger log = LoggerFactory.getLogger(FileResource.class);

    private final FileService fileService;

    public FileResource(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/public/file/download/{fileName}")
    public ResponseEntity<byte[]> downloadImage(@PathVariable String fileName) {
        if(fileName.contains(".png")){
            return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(FileUtils.download(fileName));
        }else {
            return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(FileUtils.download(fileName));
        }
    }
}
