package ly.com.farhaty.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import ly.com.farhaty.security.SecurityUtils;
import ly.com.farhaty.service.HallImageService;
import ly.com.farhaty.service.UserService;
import ly.com.farhaty.service.dto.HallImageAllDTO;
import ly.com.farhaty.service.mapper.HallImageMapper;
import ly.com.farhaty.service.util.FileUtils;
import ly.com.farhaty.web.rest.errors.BadRequestAlertException;
import ly.com.farhaty.service.dto.HallImageDTO;
import ly.com.farhaty.service.dto.HallImageCriteria;
import ly.com.farhaty.service.HallImageQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ly.com.farhaty.domain.HallImage}.
 */
@RestController
@RequestMapping("/api")
public class HallImageResource {

    private final Logger log = LoggerFactory.getLogger(HallImageResource.class);

    private static final String ENTITY_NAME = "hallImage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HallImageService hallImageService;

    private final HallImageQueryService hallImageQueryService;

    private final HallImageMapper hallImageMapper;

    private final UserService userService;

    public HallImageResource(HallImageService hallImageService, HallImageQueryService hallImageQueryService, HallImageMapper hallImageMapper, UserService userService) {
        this.hallImageService = hallImageService;
        this.hallImageQueryService = hallImageQueryService;
        this.hallImageMapper = hallImageMapper;
        this.userService = userService;
    }

    /**
     * {@code POST  /hall-images} : Create a new hallImage.
     *
     * @param hallImageDTO the hallImageDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hallImageDTO, or with status {@code 400 (Bad Request)} if the hallImage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hall-images")
    public ResponseEntity<HallImageDTO> createHallImage(@RequestBody HallImageDTO hallImageDTO) throws URISyntaxException {
        log.debug("REST request to save HallImage : {}", hallImageDTO);
        if (hallImageDTO.getId() != null) {
            throw new BadRequestAlertException("A new hallImage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HallImageDTO result = hallImageService.save(hallImageDTO);
        return ResponseEntity.created(new URI("/api/hall-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /hall-images} : Updates an existing hallImage.
     *
     * @param hallImageDTO the hallImageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hallImageDTO,
     * or with status {@code 400 (Bad Request)} if the hallImageDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hallImageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/hall-images")
    public ResponseEntity<HallImageDTO> updateHallImage(@RequestBody HallImageDTO hallImageDTO) throws URISyntaxException {
        log.debug("REST request to update HallImage : {}", hallImageDTO);
        if (hallImageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HallImageDTO result = hallImageService.save(hallImageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, hallImageDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /hall-images} : get all the hallImages.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hallImages in body.
     */
    @GetMapping("/hall-images")
    public ResponseEntity<List<HallImageAllDTO>> getAllHallImages(HallImageCriteria criteria, Pageable pageable) {
        log.debug("REST request to get HallImages by criteria: {}", criteria);

        Page<HallImageAllDTO> page;

        if(SecurityUtils.isCurrentUserInRole("ROLE_HALL")){
            page = hallImageService.findByUser(userService.getLoggedInUser().get().getId(), pageable);
        }else {
            page = hallImageQueryService.findByCriteria(criteria, pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /hall-images/count} : count all the hallImages.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/hall-images/count")
    public ResponseEntity<Long> countHallImages(HallImageCriteria criteria) {
        log.debug("REST request to count HallImages by criteria: {}", criteria);
        return ResponseEntity.ok().body(hallImageQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /hall-images/:id} : get the "id" hallImage.
     *
     * @param id the id of the hallImageDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hallImageDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hall-images/{id}")
    public ResponseEntity<HallImageDTO> getHallImage(@PathVariable Long id) {
        log.debug("REST request to get HallImage : {}", id);
        Optional<HallImageDTO> hallImageDTO = hallImageService.findOne(id)
            .map(hallImageMapper::toDto);
        return ResponseUtil.wrapOrNotFound(hallImageDTO);
    }

    /**
     * {@code DELETE  /hall-images/:id} : delete the "id" hallImage.
     *
     * @param id the id of the hallImageDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/hall-images/{id}")
    public ResponseEntity<Void> deleteHallImage(@PathVariable Long id) {
        log.debug("REST request to delete HallImage : {}", id);
        hallImageService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }


    @GetMapping("/public/hall-images/image/{id}")
    public ResponseEntity<byte[]> downloadImage(@PathVariable Long id) {
        if (hallImageService.findOne(id).isPresent()) {
            return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(FileUtils.download(hallImageService.findOne(id).get().getImageUrl()));
        }else {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }

    @GetMapping("/public/hall-images")
    public ResponseEntity<List<HallImageAllDTO>> getAllHallImagesPublic(HallImageCriteria criteria, Pageable pageable) {
        log.debug("REST request to get HallImages by criteria: {}", criteria);
        Page<HallImageAllDTO> page = hallImageQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
