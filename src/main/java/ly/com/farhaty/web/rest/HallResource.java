package ly.com.farhaty.web.rest;

import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.LongFilter;
import ly.com.farhaty.security.SecurityUtils;
import ly.com.farhaty.service.HallService;
import ly.com.farhaty.service.UserService;
import ly.com.farhaty.service.dto.HallAllDTO;
import ly.com.farhaty.service.mapper.HallMapper;
import ly.com.farhaty.service.util.FileUtils;
import ly.com.farhaty.web.rest.errors.BadRequestAlertException;
import ly.com.farhaty.service.dto.HallDTO;
import ly.com.farhaty.service.dto.HallCriteria;
import ly.com.farhaty.service.HallQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ly.com.farhaty.domain.Hall}.
 */
@RestController
@RequestMapping("/api")
public class HallResource {

    private final Logger log = LoggerFactory.getLogger(HallResource.class);

    private static final String ENTITY_NAME = "hall";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HallService hallService;

    private final HallQueryService hallQueryService;

    private final HallMapper hallMapper;

    private final UserService userService;

    public HallResource(HallService hallService, HallQueryService hallQueryService, HallMapper hallMapper, UserService userService) {
        this.hallService = hallService;
        this.hallQueryService = hallQueryService;
        this.hallMapper = hallMapper;
        this.userService = userService;
    }

    /**
     * {@code POST  /halls} : Create a new hall.
     *
     * @param hallDTO the hallDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hallDTO, or with status {@code 400 (Bad Request)} if the hall has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/halls")
    public ResponseEntity<HallDTO> createHall(@Valid @RequestBody HallDTO hallDTO) throws URISyntaxException {
        log.debug("REST request to save Hall : {}", hallDTO);
        if (hallDTO.getId() != null) {
            throw new BadRequestAlertException("A new hall cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HallDTO result = hallService.create(hallDTO);
        return ResponseEntity.created(new URI("/api/halls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /halls} : Updates an existing hall.
     *
     * @param hallDTO the hallDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hallDTO,
     * or with status {@code 400 (Bad Request)} if the hallDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hallDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/halls")
    public ResponseEntity<HallDTO> updateHall(@Valid @RequestBody HallDTO hallDTO) throws URISyntaxException {
        log.debug("REST request to update Hall : {}", hallDTO);
        if (hallDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HallDTO result = hallService.save(hallDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, hallDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /halls} : get all the halls.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of halls in body.
     */
    @GetMapping("/halls")
    public ResponseEntity<List<HallAllDTO>> getAllHalls(HallCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Halls by criteria: {}", criteria);

        Page<HallAllDTO> page;
        if(SecurityUtils.isCurrentUserInRole("ROLE_HALL")){
            LongFilter longFilter = new LongFilter();
            longFilter.setEquals(hallService.findOneByUser().getId());
            criteria.setId(longFilter);
            page = hallQueryService.findByCriteria(criteria, pageable);
        }else {
            page = hallQueryService.findByCriteria(criteria, pageable);
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /halls/count} : count all the halls.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/halls/count")
    public ResponseEntity<Long> countHalls(HallCriteria criteria) {
        log.debug("REST request to count Halls by criteria: {}", criteria);
        return ResponseEntity.ok().body(hallQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /halls/:id} : get the "id" hall.
     *
     * @param id the id of the hallDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hallDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/halls/{id}")
    public ResponseEntity<HallDTO> getHall(@PathVariable Long id) {
        log.debug("REST request to get Hall : {}", id);
        Optional<HallDTO> hallDTO = hallService.findOne(id).map(hallMapper::toDto);
        return ResponseUtil.wrapOrNotFound(hallDTO);
    }

    /**
     * {@code DELETE  /halls/:id} : delete the "id" hall.
     *
     * @param id the id of the hallDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/halls/{id}")
    public ResponseEntity<Void> deleteHall(@PathVariable Long id) {
        log.debug("REST request to delete Hall : {}", id);
        hallService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/public/halls/logo/{id}")
    public ResponseEntity<byte[]> downloadImage(@PathVariable Long id) {
        if (hallService.findOne(id).isPresent()) {
            return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(FileUtils.download(hallService.findOne(id).get().getLogoUrl()));
        }else {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }

    @GetMapping("/public/halls")
    public ResponseEntity<List<HallAllDTO>> getAllHallsPublic(HallCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Halls by criteria: {}", criteria);
        if(criteria.getRequestedDate() != null){
            LocalDateFilter dateFilter = criteria.getRequestedDate();
            Page<HallAllDTO> page = hallService.findByDateIsAvailable(dateFilter.getEquals() , pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
        Page<HallAllDTO> page = hallQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/public/halls/{id}")
    public ResponseEntity<HallAllDTO> getHallPublic(@PathVariable Long id) {
        log.debug("REST request to get Hall : {}", id);
        Optional<HallAllDTO> hallDTO = hallService.findOne(id).map(hallMapper::toAllDto);

        hallDTO.get().setReservedDates(hallService.datesByHallId(hallDTO.get().getId()));
        hallDTO.get().getRooms().forEach(roomDTO -> {
            roomDTO.setReservedDates(hallService.datesByHallIdAndRoomId(hallDTO.get().getId(), roomDTO.getId()));
        });

//        Optional<HallDTO> hallDTO2 = hallService.findOne(id).map(hallMapper::toDto);
//        hallDTO2.ifPresent(hallDTO1 -> {
//            if(hallDTO1.getViewCount() == null)
//                hallDTO1.setViewCount(0);
//            hallDTO1.setViewCount(hallDTO1.getViewCount() + 1);
//            hallService.save(hallDTO1);
//        });

        return ResponseUtil.wrapOrNotFound(hallDTO);
    }


}
