package ly.com.farhaty.web.rest;

import io.github.jhipster.service.filter.LongFilter;
import ly.com.farhaty.domain.Reservation;
import ly.com.farhaty.domain.enumeration.ReservationStatus;
import ly.com.farhaty.security.SecurityUtils;
import ly.com.farhaty.service.HallService;
import ly.com.farhaty.service.ReservationService;
import ly.com.farhaty.service.UserService;
import ly.com.farhaty.service.util.JasperReportsUtil;
import ly.com.farhaty.web.rest.errors.BadRequestAlertException;
import ly.com.farhaty.service.dto.ReservationDTO;
import ly.com.farhaty.service.dto.ReservationCriteria;
import ly.com.farhaty.service.ReservationQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.*;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * REST controller for managing {@link ly.com.farhaty.domain.Reservation}.
 */
@RestController
@RequestMapping("/api")
public class ReservationResource {

    private final Logger log = LoggerFactory.getLogger(ReservationResource.class);

    private static final String ENTITY_NAME = "reservation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReservationService reservationService;

    private final ReservationQueryService reservationQueryService;

    private final UserService userService;

    private final HallService hallService;

    @Autowired
    JasperReportsUtil jasperReportsUtil;

    public ReservationResource(ReservationService reservationService, ReservationQueryService reservationQueryService, UserService userService, HallService hallService) {
        this.reservationService = reservationService;
        this.reservationQueryService = reservationQueryService;
        this.userService = userService;
        this.hallService = hallService;
    }

    /**
     * {@code POST  /reservations} : Create a new reservation.
     *
     * @param reservationDTO the reservationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new reservationDTO, or with status {@code 400 (Bad Request)} if the reservation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reservations")
    public ResponseEntity<ReservationDTO> createReservation(@Valid @RequestBody ReservationDTO reservationDTO) throws URISyntaxException {
        log.debug("REST request to save Reservation : {}", reservationDTO);
        System.out.println(reservationDTO.getReservationDate().toString());
        if (reservationDTO.getId() != null) {
            throw new BadRequestAlertException("A new reservation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ReservationDTO result = reservationService.create(reservationDTO);
        return ResponseEntity.created(new URI("/api/reservations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reservations} : Updates an existing reservation.
     *
     * @param reservationDTO the reservationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated reservationDTO,
     * or with status {@code 400 (Bad Request)} if the reservationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the reservationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reservations")
    public ResponseEntity<ReservationDTO> updateReservation(@Valid @RequestBody ReservationDTO reservationDTO) throws URISyntaxException {
        log.debug("REST request to update Reservation : {}", reservationDTO);
        if (reservationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ReservationDTO result = reservationService.save(reservationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, reservationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reservations} : get all the reservations.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of reservations in body.
     */
    @GetMapping("/reservations")
    public ResponseEntity<List<ReservationDTO>> getAllReservations(ReservationCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Reservations by criteria: {}", criteria);
        Page<ReservationDTO> page ;

        if(SecurityUtils.isCurrentUserInRole("ROLE_HALL")){
            LongFilter longFilter = new LongFilter();
            longFilter.setEquals(hallService.findOneByUser().getId());
            criteria.setHallId(longFilter);
            page = reservationQueryService.findByCriteria(criteria, pageable);
        }else if(SecurityUtils.isCurrentUserInRole("ROLE_ADMIN")) {
            page = reservationQueryService.findByCriteria(criteria, pageable);
        }else {
            LongFilter longFilter = new LongFilter();
            longFilter.setEquals(userService.getLoggedInUser().get().getId());
            criteria.setUserId(longFilter);
            page = reservationQueryService.findByCriteria(criteria, pageable);

        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /reservations/count} : count all the reservations.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/reservations/count")
    public ResponseEntity<Long> countReservations(ReservationCriteria criteria) {
        log.debug("REST request to count Reservations by criteria: {}", criteria);
        return ResponseEntity.ok().body(reservationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reservations/:id} : get the "id" reservation.
     *
     * @param id the id of the reservationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the reservationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reservations/{id}")
    public ResponseEntity<ReservationDTO> getReservation(@PathVariable Long id) {
        log.debug("REST request to get Reservation : {}", id);
        Optional<ReservationDTO> reservationDTO = reservationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(reservationDTO);
    }

    /**
     * {@code DELETE  /reservations/:id} : delete the "id" reservation.
     *
     * @param id the id of the reservationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reservations/{id}")
    public ResponseEntity<Void> deleteReservation(@PathVariable Long id) {
        log.debug("REST request to delete Reservation : {}", id);
        reservationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }


    @GetMapping("/public/reservations/is-room-available")
    public ResponseEntity<String> isRoomAvailable(ReservationCriteria criteria) {
        log.debug("REST request to count Orders by criteria: {}", criteria);
        String available = "0";
        LocalDate date = criteria.getReservationDate().getEquals();
        Long roomId = criteria.getRoomId().getEquals();
        boolean isFull = reservationService.isRoomFull(roomId, date);
        available = isFull ?  "0" : "1";
        return ResponseEntity.ok().body(available);
    }

    @GetMapping(value = "/public/reservations/xlsx/{hallId}/{from}/{to}/{all}" , produces = "application/vnd.ms-excel")
    public ResponseEntity<byte[]> getReservationsByHallAsXSLX(@PathVariable Long hallId, @PathVariable LocalDate from, @PathVariable LocalDate to, @PathVariable Integer all) {
        log.debug("REST request to get xslx");
        List<Reservation> reservationList;
        String[] columns = { "رقم الحجز","تاريخ الحجز", "حالة الحجز" , "تاريخ المناسبة" ,"اسم الزبون", "رقم هاتف الزبون", "الصالة", "نوع المناسبة", "الاجمالي"};
        if (all==1) {
            reservationList = reservationService.findListByHall(hallId);
        }else {
            reservationList = reservationService.findListByHallAndDate(hallId, from, to);
        }

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Reservations");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Other rows and cells with contacts data
        int rowNum = 1;

        for (Reservation reservation : reservationList) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(reservation.getId());
            row.createCell(1).setCellValue(reservation.getCreatedDate().toString());
            row.createCell(2).setCellValue(getStatusInArabic(reservation.getReservationStatus()));
            row.createCell(3).setCellValue(reservation.getReservationDate().toString());
            row.createCell(4).setCellValue(reservation.getCustomerName());
            row.createCell(5).setCellValue(reservation.getCustomerPhone());
            row.createCell(6).setCellValue(reservation.getHall().getName());
            row.createCell(7).setCellValue(reservation.getType().getName());
            row.createCell(8).setCellValue(reservation.getTotal());
        }


        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        byte[] bytes = new byte[0];

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            bos.close();
            bytes = bos.toByteArray();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf("application/vnd.ms-excel"));
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" +hallId+"_"+ String.valueOf(new Date()) + ".xlsx");
        header.setContentLength(bytes.length);

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bytes), header);

    }

    private String getStatusInArabic(ReservationStatus reservationStatus) {
        if(reservationStatus == ReservationStatus.PENDING){
            return "حجز مبدئي";
        }else if (reservationStatus == ReservationStatus.APPROVED){
            return "حجز مؤكد";
        }else if (reservationStatus == ReservationStatus.DONE){
            return "حجز منتهي";
        }else if (reservationStatus == ReservationStatus.CANCELED_BY_HALL){
            return "حجز ملغي من الصالة";
        }else if (reservationStatus == ReservationStatus.CANCELED_BY_USER){
            return "حجز ملغي من الزبون";
        }else {
            return reservationStatus.toString();
        }
    }

    @GetMapping(value = "/public/reservations/pdf/{hallId}/{from}/{to}/{all}", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> printReservationsByHallAsPDF(@PathVariable Long hallId, @PathVariable LocalDate from, @PathVariable LocalDate to, @PathVariable Integer all) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("hall_id", hallId.toString());
        byte[] fileBytes = jasperReportsUtil.getReportAsPDF(parameters, "hall_report");
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_PDF);
        header.set(HttpHeaders.CONTENT_DISPOSITION,
            "attachment; filename=Name_" + System.currentTimeMillis() + ".pdf");
        header.setContentLength(fileBytes.length);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(fileBytes), header);

    }


}
