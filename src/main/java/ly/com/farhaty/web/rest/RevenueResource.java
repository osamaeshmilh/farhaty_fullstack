package ly.com.farhaty.web.rest;

import ly.com.farhaty.service.RevenueService;
import ly.com.farhaty.web.rest.errors.BadRequestAlertException;
import ly.com.farhaty.service.dto.RevenueDTO;
import ly.com.farhaty.service.dto.RevenueCriteria;
import ly.com.farhaty.service.RevenueQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ly.com.farhaty.domain.Revenue}.
 */
@RestController
@RequestMapping("/api")
public class RevenueResource {

    private final Logger log = LoggerFactory.getLogger(RevenueResource.class);

    private static final String ENTITY_NAME = "revenue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RevenueService revenueService;

    private final RevenueQueryService revenueQueryService;

    public RevenueResource(RevenueService revenueService, RevenueQueryService revenueQueryService) {
        this.revenueService = revenueService;
        this.revenueQueryService = revenueQueryService;
    }

    /**
     * {@code POST  /revenues} : Create a new revenue.
     *
     * @param revenueDTO the revenueDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new revenueDTO, or with status {@code 400 (Bad Request)} if the revenue has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/revenues")
    public ResponseEntity<RevenueDTO> createRevenue(@Valid @RequestBody RevenueDTO revenueDTO) throws URISyntaxException {
        log.debug("REST request to save Revenue : {}", revenueDTO);
        if (revenueDTO.getId() != null) {
            throw new BadRequestAlertException("A new revenue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RevenueDTO result = revenueService.save(revenueDTO);
        return ResponseEntity.created(new URI("/api/revenues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /revenues} : Updates an existing revenue.
     *
     * @param revenueDTO the revenueDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated revenueDTO,
     * or with status {@code 400 (Bad Request)} if the revenueDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the revenueDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/revenues")
    public ResponseEntity<RevenueDTO> updateRevenue(@Valid @RequestBody RevenueDTO revenueDTO) throws URISyntaxException {
        log.debug("REST request to update Revenue : {}", revenueDTO);
        if (revenueDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RevenueDTO result = revenueService.save(revenueDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, revenueDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /revenues} : get all the revenues.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of revenues in body.
     */
    @GetMapping("/revenues")
    public ResponseEntity<List<RevenueDTO>> getAllRevenues(RevenueCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Revenues by criteria: {}", criteria);
        Page<RevenueDTO> page = revenueQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /revenues/count} : count all the revenues.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/revenues/count")
    public ResponseEntity<Long> countRevenues(RevenueCriteria criteria) {
        log.debug("REST request to count Revenues by criteria: {}", criteria);
        return ResponseEntity.ok().body(revenueQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /revenues/:id} : get the "id" revenue.
     *
     * @param id the id of the revenueDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the revenueDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/revenues/{id}")
    public ResponseEntity<RevenueDTO> getRevenue(@PathVariable Long id) {
        log.debug("REST request to get Revenue : {}", id);
        Optional<RevenueDTO> revenueDTO = revenueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(revenueDTO);
    }

    /**
     * {@code DELETE  /revenues/:id} : delete the "id" revenue.
     *
     * @param id the id of the revenueDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/revenues/{id}")
    public ResponseEntity<Void> deleteRevenue(@PathVariable Long id) {
        log.debug("REST request to delete Revenue : {}", id);
        revenueService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
