package ly.com.farhaty.web.rest;

import ly.com.farhaty.security.SecurityUtils;
import ly.com.farhaty.service.StoreImageService;
import ly.com.farhaty.service.UserService;
import ly.com.farhaty.service.dto.StoreImageAllDTO;
import ly.com.farhaty.service.mapper.StoreImageMapper;
import ly.com.farhaty.service.util.FileUtils;
import ly.com.farhaty.web.rest.errors.BadRequestAlertException;
import ly.com.farhaty.service.dto.StoreImageDTO;
import ly.com.farhaty.service.dto.StoreImageCriteria;
import ly.com.farhaty.service.StoreImageQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ly.com.farhaty.domain.StoreImage}.
 */
@RestController
@RequestMapping("/api")
public class StoreImageResource {

    private final Logger log = LoggerFactory.getLogger(StoreImageResource.class);

    private static final String ENTITY_NAME = "storeImage";

    private final StoreImageMapper storeImageMapper;


    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StoreImageService storeImageService;

    private final StoreImageQueryService storeImageQueryService;

    private final UserService userService;


    public StoreImageResource(StoreImageMapper storeImageMapper, StoreImageService storeImageService, StoreImageQueryService storeImageQueryService, UserService userService) {
        this.storeImageMapper = storeImageMapper;
        this.storeImageService = storeImageService;
        this.storeImageQueryService = storeImageQueryService;
        this.userService = userService;
    }

    /**
     * {@code POST  /store-images} : Create a new storeImage.
     *
     * @param storeImageDTO the storeImageDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new storeImageDTO, or with status {@code 400 (Bad Request)} if the storeImage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/store-images")
    public ResponseEntity<StoreImageDTO> createStoreImage(@Valid @RequestBody StoreImageDTO storeImageDTO) throws URISyntaxException {
        log.debug("REST request to save StoreImage : {}", storeImageDTO);
        if (storeImageDTO.getId() != null) {
            throw new BadRequestAlertException("A new storeImage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StoreImageDTO result = storeImageService.save(storeImageDTO);
        return ResponseEntity.created(new URI("/api/store-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /store-images} : Updates an existing storeImage.
     *
     * @param storeImageDTO the storeImageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated storeImageDTO,
     * or with status {@code 400 (Bad Request)} if the storeImageDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the storeImageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/store-images")
    public ResponseEntity<StoreImageDTO> updateStoreImage(@Valid @RequestBody StoreImageDTO storeImageDTO) throws URISyntaxException {
        log.debug("REST request to update StoreImage : {}", storeImageDTO);
        if (storeImageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        StoreImageDTO result = storeImageService.save(storeImageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, storeImageDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /store-images} : get all the storeImages.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of storeImages in body.
     */
    @GetMapping("/store-images")
    public ResponseEntity<List<StoreImageAllDTO>> getAllStoreImages(StoreImageCriteria criteria, Pageable pageable) {
        log.debug("REST request to get StoreImages by criteria: {}", criteria);

        Page<StoreImageAllDTO> page;

        if(SecurityUtils.isCurrentUserInRole("ROLE_STORE")){
            page = storeImageService.findByUser(userService.getLoggedInUser().get().getId(), pageable);
        }else {
            page = storeImageQueryService.findByCriteria(criteria, pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /store-images/count} : count all the storeImages.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/store-images/count")
    public ResponseEntity<Long> countStoreImages(StoreImageCriteria criteria) {
        log.debug("REST request to count StoreImages by criteria: {}", criteria);
        return ResponseEntity.ok().body(storeImageQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /store-images/:id} : get the "id" storeImage.
     *
     * @param id the id of the storeImageDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the storeImageDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/store-images/{id}")
    public ResponseEntity<StoreImageDTO> getStoreImage(@PathVariable Long id) {
        log.debug("REST request to get StoreImage : {}", id);
        Optional<StoreImageDTO> storeImageDTO = storeImageService.findOne(id).map(storeImageMapper::toDto);
        return ResponseUtil.wrapOrNotFound(storeImageDTO);
    }

    /**
     * {@code DELETE  /store-images/:id} : delete the "id" storeImage.
     *
     * @param id the id of the storeImageDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/store-images/{id}")
    public ResponseEntity<Void> deleteStoreImage(@PathVariable Long id) {
        log.debug("REST request to delete StoreImage : {}", id);
        storeImageService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/public/store-images/image/{id}")
    public ResponseEntity<byte[]> downloadImage(@PathVariable Long id) {
        if (storeImageService.findOne(id).isPresent()) {
            return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(FileUtils.download(storeImageService.findOne(id).get().getImageUrl()));
        }else {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }

    @GetMapping("/public/store-images")
    public ResponseEntity<List<StoreImageAllDTO>> getAllStoreImagesPublic(StoreImageCriteria criteria, Pageable pageable) {
        log.debug("REST request to get StoreImages by criteria: {}", criteria);
        Page<StoreImageAllDTO> page = storeImageQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
