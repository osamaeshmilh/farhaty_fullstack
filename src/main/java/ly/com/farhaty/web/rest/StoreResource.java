package ly.com.farhaty.web.rest;

import ly.com.farhaty.security.SecurityUtils;
import ly.com.farhaty.service.StoreService;
import ly.com.farhaty.service.UserService;
import ly.com.farhaty.service.dto.StoreAllDTO;
import ly.com.farhaty.service.mapper.StoreMapper;
import ly.com.farhaty.service.util.FileUtils;
import ly.com.farhaty.web.rest.errors.BadRequestAlertException;
import ly.com.farhaty.service.dto.StoreDTO;
import ly.com.farhaty.service.dto.StoreCriteria;
import ly.com.farhaty.service.StoreQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ly.com.farhaty.domain.Store}.
 */
@RestController
@RequestMapping("/api")
public class StoreResource {

    private final Logger log = LoggerFactory.getLogger(StoreResource.class);

    private static final String ENTITY_NAME = "store";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StoreService storeService;

    private final StoreQueryService storeQueryService;

    private final StoreMapper storeMapper;

    private final UserService userService;

    public StoreResource(StoreService storeService, StoreQueryService storeQueryService, StoreMapper storeMapper, UserService userService) {
        this.storeService = storeService;
        this.storeQueryService = storeQueryService;
        this.storeMapper = storeMapper;
        this.userService = userService;
    }

    /**
     * {@code POST  /stores} : Create a new store.
     *
     * @param storeDTO the storeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new storeDTO, or with status {@code 400 (Bad Request)} if the store has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/stores")
    public ResponseEntity<StoreDTO> createStore(@Valid @RequestBody StoreDTO storeDTO) throws URISyntaxException {
        log.debug("REST request to save Store : {}", storeDTO);
        if (storeDTO.getId() != null) {
            throw new BadRequestAlertException("A new store cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StoreDTO result = storeService.create(storeDTO);
        return ResponseEntity.created(new URI("/api/stores/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /stores} : Updates an existing store.
     *
     * @param storeDTO the storeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated storeDTO,
     * or with status {@code 400 (Bad Request)} if the storeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the storeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/stores")
    public ResponseEntity<StoreDTO> updateStore(@Valid @RequestBody StoreDTO storeDTO) throws URISyntaxException {
        log.debug("REST request to update Store : {}", storeDTO);
        if (storeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        StoreDTO result = storeService.save(storeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, storeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /stores} : get all the stores.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of stores in body.
     */
    @GetMapping("/stores")
    public ResponseEntity<List<StoreAllDTO>> getAllStores(StoreCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Stores by criteria: {}", criteria);
        Page<StoreAllDTO> page;

        if(SecurityUtils.isCurrentUserInRole("ROLE_STORE")){
            page = storeService.findByUser(userService.getLoggedInUser().get().getId(), pageable);
        }else {
            page = storeQueryService.findByCriteria(criteria, pageable);
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /stores/count} : count all the stores.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/stores/count")
    public ResponseEntity<Long> countStores(StoreCriteria criteria) {
        log.debug("REST request to count Stores by criteria: {}", criteria);
        return ResponseEntity.ok().body(storeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /stores/:id} : get the "id" store.
     *
     * @param id the id of the storeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the storeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/stores/{id}")
    public ResponseEntity<StoreDTO> getStore(@PathVariable Long id) {
        log.debug("REST request to get Store : {}", id);
        Optional<StoreDTO> storeDTO = storeService.findOne(id)
            .map(storeMapper::toDto);
        return ResponseUtil.wrapOrNotFound(storeDTO);
    }

    /**
     * {@code DELETE  /stores/:id} : delete the "id" store.
     *
     * @param id the id of the storeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/stores/{id}")
    public ResponseEntity<Void> deleteStore(@PathVariable Long id) {
        log.debug("REST request to delete Store : {}", id);
        storeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/public/stores/logo/{id}")
    public ResponseEntity<byte[]> downloadImage(@PathVariable Long id) {
        if (storeService.findOne(id).isPresent()) {
            return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(FileUtils.download(storeService.findOne(id).get().getLogoUrl()));
        }else {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }

    @GetMapping("/public/stores")
    public ResponseEntity<List<StoreAllDTO>> getAllStoresPublic(StoreCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Stores by criteria: {}", criteria);
        Page<StoreAllDTO> page = storeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/public/stores/{id}")
    public ResponseEntity<StoreAllDTO> getStorePublic(@PathVariable Long id) {
        log.debug("REST request to get Store : {}", id);
        Optional<StoreAllDTO> storeDTO = storeService.findOne(id).map(storeMapper::toAllDto);

        Optional<StoreDTO> storeDTO2 = storeService.findOne(id).map(storeMapper::toDto);
        storeDTO2.ifPresent(storeDTO1 -> {
            if(storeDTO1.getViewCount() == null)
                storeDTO1.setViewCount(0);
            storeDTO1.setViewCount(storeDTO1.getViewCount() + 1);
            storeService.save(storeDTO1);
        });
        return ResponseUtil.wrapOrNotFound(storeDTO);
    }
}
