import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import {
  RegisterComponent,
  ActivateComponent,
  PasswordComponent,
  PasswordResetInitComponent,
  PasswordResetFinishComponent,
  SettingsComponent,
  accountState
} from './';
import { FarhatySharedModule } from '../shared';
import { SocialAuthComponent } from './social/social-auth.component';
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';
import { SocialRegisterComponent } from './social/social-register.component';
import { PasswordStrengthBarComponent } from 'app/account/password/password-strength-bar.component';
@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(accountState), MatPasswordStrengthModule],
  declarations: [
    ActivateComponent,
    RegisterComponent,
    PasswordComponent,
    PasswordResetInitComponent,
    PasswordResetFinishComponent,
    SettingsComponent,
    SocialAuthComponent,
    SocialRegisterComponent,
    PasswordStrengthBarComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FarhatyAccountModule {}
