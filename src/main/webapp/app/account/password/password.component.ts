import { Component, OnInit } from '@angular/core';

import { PasswordService } from './password.service';
import { ErrorStateMatcher } from '@angular/material';
import { FormControl } from '@angular/forms';
import { AccountService } from '../../core';

@Component({
  selector: 'jhi-password',
  templateUrl: './password.component.html'
})
export class PasswordComponent implements OnInit {
  doNotMatch: string;
  error: string;
  success: string;
  account: any;
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
  isSaving = false;
  validatePasswordMatch: ErrorStateMatcher = {
    isErrorState: (control: FormControl) => {
      if (control.touched) {
        return !this.passwordConfirmed() || control.hasError('required');
      } else {
        return false;
      }
    }
  };

  constructor(private passwordService: PasswordService, private accountService: AccountService) {}

  ngOnInit() {
    this.accountService.identity().then(account => {
      this.account = account;
    });
  }

  passwordConfirmed(): boolean {
    return this.confirmPassword === this.newPassword;
  }

  changePassword() {
    this.isSaving = true;
    this.passwordService.save(this.newPassword, this.currentPassword).subscribe(
      () => {
        this.error = null;
        this.success = 'OK';
      },
      () => {
        this.success = null;
        this.error = 'ERROR';
      }
    );
  }

  previousState() {
    window.history.back();
  }
}
