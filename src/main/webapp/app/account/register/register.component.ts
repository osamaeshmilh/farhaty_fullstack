import { Component, OnInit, AfterViewInit, Renderer, ElementRef, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { Register } from './register.service';
import { LoginModalService } from '../../core';
import { EMAIL_ALREADY_USED_TYPE } from '../../shared';
import { ErrorStateMatcher } from '@angular/material/core/';
import { FormControl } from '@angular/forms';
import swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit, AfterViewInit {
  confirmPassword: string;
  doNotMatch: string;
  error: string;
  errorEmailExists: string;
  errorUserExists: string;
  registerAccount: any;
  success: boolean;
  isLoading = false;

  validatePasswordMatch: ErrorStateMatcher = {
    isErrorState: (control: FormControl) => {
      if (control.touched) {
        return !this.passwordConfirmed() || control.hasError('required');
      } else {
        return false;
      }
    }
  };

  constructor(
    private loginModalService: LoginModalService,
    private registerService: Register,
    private elementRef: ElementRef,
    private renderer: Renderer,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.success = false;
    this.registerAccount = {};
  }

  ngAfterViewInit() {
    this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#login'), 'focus', []);
  }

  passwordConfirmed(): boolean {
    return this.confirmPassword === this.registerAccount.password;
  }

  register() {
    this.isLoading = true;
    this.registerAccount.email = this.registerAccount.login;
    this.error = null;
    this.errorUserExists = null;
    this.errorEmailExists = null;
    this.registerAccount.langKey = 'en';
    this.registerService.save(this.registerAccount).subscribe(
      () => {
        this.success = true;
        this.isLoading = false;
        this.toastr.success('تمت عملية التسجيل بنجاح');
        this.router.navigate(['/settings']);
      },
      response => this.processError(response)
    );
  }

  private processError(response: HttpErrorResponse) {
    this.isLoading = false;

    this.success = null;
    if (response.status === 400 && response.error.errorKey === EMAIL_ALREADY_USED_TYPE) {
      // swal({
      //     type: 'error',
      //     title: 'حدث خطأ ما!',
      //     text: 'البريد الإلتكتروني مسجل مسبقاً',
      //     confirmButtonText: 'حسناً'
      // });

      this.toastr.error('البريد الإلتكتروني مسجل مسبقاً', null, {
        positionClass: 'toast-top-center'
      });
    } else {
      this.error = 'ERROR';
    }
  }
}
