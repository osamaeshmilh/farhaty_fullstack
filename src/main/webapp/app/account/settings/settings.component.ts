import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../core';
import { ErrorStateMatcher } from '@angular/material';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'jhi-settings',
  templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
  error: string;
  success: string;
  user: any;
  languages: any[];
  isSaving = false;
  userCategories: any[] = [];
  isLoading = false;

  userCategoryErrorMatcher: ErrorStateMatcher = {
    isErrorState: (control: FormControl) => {
      if (!control.touched) {
        return false;
      }
      console.log('Validating ' + this.validateCategories());
      return !this.validateCategories();
    }
  };

  constructor(private accountService: AccountService, private toastr: ToastrService) {}

  ngOnInit() {
    this.accountService.identity().then(account => {
      this.user = this.copyAccount(account);
      console.log(this.user);
    });
  }

  validateCategories(): boolean {
    return this.user.categories.length >= 3;
  }

  save() {
    this.isSaving = true;
    this.accountService.save(this.user).subscribe(
      () => {
        this.isSaving = false;
        this.error = null;
        this.success = 'OK';
        this.accountService.identity(true).then(account => {
          this.user = this.copyAccount(account);
        });
        this.toastr.success('تمت عملية الحفظ بنحاح');

        this.previousState();
      },
      () => {
        this.isSaving = false;
        this.success = null;
        this.error = 'ERROR';
        this.toastr.error('حدث خطأ ما');
      }
    );
  }

  copyAccount(account) {
    return {
      id: account.id,
      activated: account.activated,
      email: account.email,
      firstName: account.firstName,
      langKey: account.langKey,
      lastName: account.lastName,
      login: account.login,
      imageUrl: account.imageUrl,
      gender: account.gender || 1,
      education: account.education,
      major: account.major,
      occupation: account.occupation,
      birthdate: account.birthdate,
      howUknow: account.howUknow,
      categories: account.categories
    };
  }

  previousState() {
    window.history.back();
  }
}
