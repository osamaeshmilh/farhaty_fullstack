import { Route } from '@angular/router';

import { SocialRegisterComponent } from './social-register.component';
import { SocialAuthComponent } from './social-auth.component';
import { UserRouteAccessService } from '../../core/auth/user-route-access-service';

export const socialRegisterRoute: Route = {
  path: 'social-register/:provider?{success:boolean}',
  component: SocialRegisterComponent,
  data: {
    authorities: [],
    pageTitle: 'Register with {{ label }}'
  },
  canActivate: [UserRouteAccessService]
};

export const socialAuthRoute: Route = {
  path: 'social-auth',
  component: SocialAuthComponent,
  data: {
    authorities: [],
    pageTitle: 'Register with {{ label }}'
  },
  canActivate: [UserRouteAccessService]
};
