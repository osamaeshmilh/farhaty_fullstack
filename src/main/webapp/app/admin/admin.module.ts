import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
/* jhipster-needle-add-admin-module-import - JHipster will add admin modules imports here */

import { adminState, AuditsComponent, UserMgmtComponent, UserMgmtDetailComponent, UserMgmtUpdateComponent, JhiDocsComponent } from './';
import { FarhatySharedModule } from '../shared';

@NgModule({
  imports: [
    FarhatySharedModule,
    RouterModule.forChild(adminState)
    /* jhipster-needle-add-admin-module - JHipster will add admin modules here */
  ],
  declarations: [AuditsComponent, UserMgmtComponent, UserMgmtDetailComponent, UserMgmtUpdateComponent, JhiDocsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FarhatyAdminModule {}
