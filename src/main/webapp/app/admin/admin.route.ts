import { Routes } from '@angular/router';

import { auditsRoute, docsRoute, userMgmtRoute } from './';
import { UserRouteAccessService } from '../core';

const ADMIN_ROUTES = [auditsRoute, docsRoute, ...userMgmtRoute];

export const adminState: Routes = [
  {
    path: '',
    data: {
      authorities: ['ROLE_ADMIN']
    },
    canActivate: [UserRouteAccessService],
    children: ADMIN_ROUTES
  }
];
