import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { ActivatedRoute, Router } from '@angular/router';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster-material';

import { Sort, PageEvent } from '@angular/material';
import { UserService, AccountService, User, IUser } from '../../core';
import { AlertDialog } from 'app/shared/alert-dialog/alert-dialog.component';
import { ITEMS_PER_PAGE } from 'app/shared';
import { filter, map } from 'rxjs/operators';
import { IHall } from 'app/shared/model/hall.model';

@Component({
  selector: 'jhi-user-mgmt',
  templateUrl: './user-management.component.html'
})
export class UserMgmtComponent implements OnInit, OnDestroy {
  currentAccount: any;
  users: User[];
  error: any;
  success: any;
  routeData: any;
  links: any;
  totalItems: any;
  queryCount: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  isloading = true;
  direction = 'asc';
  previousPage: any;
  reverse: any;

  selectedRole: any;
  currentSearch: string;

  constructor(
    private userService: UserService,
    private alertService: JhiAlertService,
    private accountService: AccountService,
    private parseLinks: JhiParseLinks,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private eventManager: JhiEventManager,
    private alertDialog: AlertDialog
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  ngOnInit() {
    this.accountService.identity().then(account => {
      this.currentAccount = account;
      this.loadAll();
      this.registerChangeInUsers();
    });
  }

  ngOnDestroy() {
    this.routeData.unsubscribe();
  }

  registerChangeInUsers() {
    this.eventManager.subscribe('userListModification', response => this.loadAll());
  }

  setActive(user, isActivated) {
    user.activated = isActivated;

    this.userService.update(user).subscribe(response => {
      if (response.status === 200) {
        this.error = null;
        this.success = 'OK';
        this.loadAll();
      } else {
        this.success = null;
        this.error = 'ERROR';
      }
    });
  }

  loadAll() {
    this.isloading = true;
    if (this.currentSearch) {
      //search
      this.userService
        .query({
          name: this.currentSearch,
          role: this.selectedRole,
          page: this.page,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe(
          (res: HttpResponse<User[]>) => this.onSuccess(res.body, res.headers),
          (res: HttpResponse<any>) => this.onError(res.body)
        );
      return;
    }

    this.userService
      .query({
        role: this.selectedRole,
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<User[]>) => this.onSuccess(res.body, res.headers), (res: HttpResponse<any>) => this.onError(res.body));
  }

  trackIdentity(index, item: User) {
    return item.id;
  }

  sort() {
    return `${this.predicate},${this.direction}`;
  }

  sortData(sort: Sort) {
    this.predicate = sort.active;
    this.direction = sort.direction;
    this.transition();
  }

  loadPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.itemsPerPage = event.pageSize;
    this.previousPage = this.page;
    this.transition();
  }

  transition() {
    this.router.navigate(['/admin/user-management'], {
      queryParams: {
        role: this.selectedRole,
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  delete(login: string) {
    this.alertDialog.open({ message: 'هل تريد حذف هذا المستخدم؟' }).subscribe(confirmed => {
      if (confirmed) {
        this.userService.delete(login).subscribe(res => this.loadAll());
      }
    });
  }

  private onSuccess(data, headers) {
    this.isloading = false;
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = headers.get('X-Total-Count');
    this.queryCount = this.totalItems;
    this.users = data;
  }

  private onError(error) {
    this.alertService.error(error.error, error.message, null);
  }

  getUsers(value: any) {
    this.selectedRole = value;
    this.userService
      .query({
        role: this.selectedRole,
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<User[]>) => this.onSuccess(res.body, res.headers), (res: HttpResponse<any>) => this.onError(res.body));
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.loadAll();
  }

  clear() {
    this.currentSearch = '';
    this.page = 0;
    this.router.navigate([
      '/admin/user-management',
      {
        role: this.selectedRole,
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }
}
