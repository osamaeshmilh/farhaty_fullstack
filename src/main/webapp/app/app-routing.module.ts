import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute } from './layouts';
import { DEBUG_INFO_ENABLED } from './app.constants';
import { JhiLoginModalComponent } from 'app/shared';

const LAYOUT_ROUTES = [...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        { path: 'login', component: JhiLoginModalComponent },
        {
          path: 'admin',
          loadChildren: () => import('./admin/admin.module').then(m => m.FarhatyAdminModule)
        },
        ...LAYOUT_ROUTES
      ],
      { useHash: true, enableTracing: DEBUG_INFO_ENABLED }
    )
  ],
  exports: [RouterModule]
})
export class FarhatyAppRoutingModule {}
