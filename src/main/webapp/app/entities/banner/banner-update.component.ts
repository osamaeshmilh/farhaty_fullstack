import { Component, OnInit, ElementRef } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster-material';
import { IBanner, Banner } from 'app/shared/model/banner.model';
import { BannerService } from './banner.service';
import { IStore } from 'app/shared/model/store.model';
import { StoreService } from 'app/entities/store/store.service';

@Component({
  selector: 'jhi-banner-update',
  templateUrl: './banner-update.component.html'
})
export class BannerUpdateComponent implements OnInit {
  isSaving: boolean;

  stores: IStore[];

  editForm = this.fb.group({
    id: [],
    title: [null, [Validators.required]],
    image: [],
    imageUrl: [],
    imageContentType: [],
    active: [null, [Validators.required]],
    storeId: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected bannerService: BannerService,
    protected storeService: StoreService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ banner }) => {
      this.updateForm(banner);
    });
    this.storeService
      .query({
        'bannerId.specified': 'false',
        size: 200
      })
      .pipe(
        filter((mayBeOk: HttpResponse<IStore[]>) => mayBeOk.ok),
        map((response: HttpResponse<IStore[]>) => response.body)
      )
      .subscribe(
        (res: IStore[]) => {
          if (!this.editForm.get('storeId').value) {
            this.stores = res;
          } else {
            this.storeService
              .find(this.editForm.get('storeId').value)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IStore>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IStore>) => subResponse.body)
              )
              .subscribe(
                (subRes: IStore) => (this.stores = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(banner: IBanner) {
    this.editForm.patchValue({
      id: banner.id,
      title: banner.title,
      image: banner.image,
      imageUrl: banner.imageUrl,
      imageContentType: banner.imageContentType,
      active: banner.active,
      storeId: banner.storeId
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const banner = this.createFromForm();
    if (banner.id !== undefined) {
      this.subscribeToSaveResponse(this.bannerService.update(banner));
    } else {
      this.subscribeToSaveResponse(this.bannerService.create(banner));
    }
  }

  private createFromForm(): IBanner {
    return {
      ...new Banner(),
      id: this.editForm.get(['id']).value,
      title: this.editForm.get(['title']).value,
      imageContentType: this.editForm.get(['imageContentType']).value,
      image: this.editForm.get(['image']).value,
      imageUrl: this.editForm.get(['imageUrl']).value,
      active: this.editForm.get(['active']).value,
      storeId: this.editForm.get(['storeId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBanner>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackStoreById(index: number, item: IStore) {
    return item.id;
  }
}
