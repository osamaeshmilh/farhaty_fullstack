import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { ReservationService } from 'app/entities/reservation/reservation.service';
import { forkJoin } from 'rxjs';
import { HallService } from 'app/entities/hall/hall.service';
import { StoreService } from 'app/entities/store/store.service';
@Component({
  selector: 'jhi-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  public doughnutChartLabels: string[] = ['حجز مبدئي', 'حجز مؤكد', 'حجز ملغي من الصالة', 'حجز ملغي من الزبون'];
  public doughnutChartData: Array<any>;
  public reservationStateData: Array<any> = [];
  public doughnutChartType = 'doughnut';

  public lineChartData: Array<any> = [
    {
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: 'عدد الحجوزات'
    }
    // { data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C' }
  ];
  public lineChartLabels: Array<any> = ['2019 Jun', '2019 Feb', '2019 Mar', '2019 Apr', '2019 May', '2019 Jun'];
  public showLegends = false;
  public lineChartOptions: any = {
    responsive: true,
    legend: {
      display: false
    },

    scales: {
      xAxes: [
        {
          display: true,
          gridLines: {
            display: false
          }
        }
      ],
      yAxes: [
        {
          display: true,
          gridLines: {
            display: false
          }
        }
      ]
    }
  };
  public lineChartColors: Array<any> = [
    {
      // grey
      backgroundColor: 'rgba(186,104,200,0.88)',
      borderColor: '#ba68c8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
    // {
    //     // dark grey
    //     backgroundColor: 'rgba(77,83,96,0.2)',
    //     borderColor: 'rgba(77,83,96,1)',
    //     pointBackgroundColor: 'rgba(77,83,96,1)',
    //     pointBorderColor: '#fff',
    //     pointHoverBackgroundColor: '#fff',
    //     pointHoverBorderColor: 'rgba(77,83,96,1)'
    // },
    // {
    //     // grey
    //     backgroundColor: 'rgba(148,159,177,0.2)',
    //     borderColor: 'rgba(148,159,177,1)',
    //     pointBackgroundColor: 'rgba(148,159,177,1)',
    //     pointBorderColor: '#fff',
    //     pointHoverBackgroundColor: '#fff',
    //     pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    // }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';

  reservationCountstoresCount: any;
  storesCount: any;
  reservationCount: any;
  hallsCount: any;

  constructor(
    private dashboardService: DashboardService,
    protected reservationService: ReservationService,
    protected hallService: HallService,
    protected storeService: StoreService
  ) {}

  ngOnInit(): void {
    forkJoin([
      this.reservationService.count({ 'reservationStatus.equals': 'PENDING' }),
      this.reservationService.count({ 'reservationStatus.equals': 'APPROVED' }),
      this.reservationService.count({ 'reservationStatus.equals': 'CANCELED_BY_HALL' }),
      this.reservationService.count({ 'reservationStatus.equals': 'CANCELED_BY_USER' })
    ]).subscribe(data => {
      this.reservationStateData.push(data[0].body);
      this.reservationStateData.push(data[1].body);
      this.reservationStateData.push(data[2].body);
      this.reservationStateData.push(data[3].body);
      this.doughnutChartData = this.reservationStateData;
    });

    this.reservationService.count().subscribe(res => (this.reservationCount = res.body));
    this.hallService.count().subscribe(res => (this.hallsCount = res.body));
    this.storeService.count().subscribe(res => (this.storesCount = res.body));
  }

  chartHovered($event: { event: MouseEvent; active: {}[] }) {}

  chartClicked($event: { event?: MouseEvent; active?: {}[] }) {}
}
