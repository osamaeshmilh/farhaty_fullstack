import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { dashboardRoute } from './';
import { DashboardComponent } from './dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { NgxLoadingModule } from 'ngx-loading';
import { FarhatySharedModule } from 'app/shared';

const ENTITY_STATES = [...dashboardRoute];

@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(ENTITY_STATES), NgxLoadingModule.forRoot({}), ChartsModule],
  declarations: [DashboardComponent],
  entryComponents: [DashboardComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule {}
