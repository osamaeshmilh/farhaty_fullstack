import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IStats } from 'app/entities/dashboard/stats.model';

type EntityResponseType = HttpResponse<IStats>;

@Injectable({ providedIn: 'root' })
export class DashboardService {
  private resourceUrl = SERVER_API_URL + 'api/dashboard';
  private resourceUrlStats = SERVER_API_URL + 'api/dashboard/statistics';

  constructor(private http: HttpClient) {}

  getStats(): Observable<EntityResponseType> {
    return this.http.get<IStats>(this.resourceUrlStats, { observe: 'response' });
  }
}
