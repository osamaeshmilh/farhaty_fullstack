export interface IStats {
  activeSubscribers?: number;
  allSubscribers?: number;
  associationCount?: number;
  familyMembersCount?: number;
  todaySubscribers?: number;
  transferRequests?: number;
  chart?: Array<any>;
}

export class Stats implements IStats {
  constructor(
    public activeSubscribers?: number,
    public allSubscribers?: number,
    public associationCount?: number,
    public familyMembersCount?: number,
    public todaySubscribers?: number,
    public transferRequests?: number,
    public chart?: Array<any>
  ) {}
}
