import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'banner',
        loadChildren: () => import('./banner/banner.module').then(m => m.FarhatyBannerModule)
      },
      {
        path: 'category',
        loadChildren: () => import('./category/category.module').then(m => m.FarhatyCategoryModule)
      },
      {
        path: 'store',
        loadChildren: () => import('./store/store.module').then(m => m.FarhatyStoreModule)
      },
      {
        path: 'store-image',
        loadChildren: () => import('./store-image/store-image.module').then(m => m.FarhatyStoreImageModule)
      },
      {
        path: 'notification',
        loadChildren: () => import('./notification/notification.module').then(m => m.FarhatyNotificationModule)
      },
      {
        path: 'hall',
        loadChildren: () => import('./hall/hall.module').then(m => m.FarhatyHallModule)
      },
      {
        path: 'type',
        loadChildren: () => import('./type/type.module').then(m => m.FarhatyTypeModule)
      },
      {
        path: 'room',
        loadChildren: () => import('./room/room.module').then(m => m.FarhatyRoomModule)
      },
      {
        path: 'extra',
        loadChildren: () => import('./extra/extra.module').then(m => m.FarhatyExtraModule)
      },
      {
        path: 'reservation',
        loadChildren: () => import('./reservation/reservation.module').then(m => m.FarhatyReservationModule)
      },
      {
        path: 'invoice',
        loadChildren: () => import('./invoice/invoice.module').then(m => m.FarhatyInvoiceModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'reservation-calender',
        loadChildren: () => import('./reservation-calender/reservation-calender.module').then(m => m.ReservationCalenderModule)
      },
      {
        path: 'hall-image',
        loadChildren: () => import('./hall-image/hall-image.module').then(m => m.FarhatyHallImageModule)
      },
      {
        path: 'item',
        loadChildren: () => import('./item/item.module').then(m => m.FarhatyItemModule)
      },
      {
        path: 'report',
        loadChildren: () => import('./report/report.module').then(m => m.FarhatyReportModule)
      },
      {
        path: 'favorite-item',
        loadChildren: () => import('./favorite-item/favorite-item.module').then(m => m.FarhatyFavoriteItemModule)
      },
      {
        path: 'expense',
        loadChildren: () => import('./expense/expense.module').then(m => m.FarhatyExpenseModule)
      },
      {
        path: 'revenue',
        loadChildren: () => import('./revenue/revenue.module').then(m => m.FarhatyRevenueModule)
      },
      {
        path: 'chat',
        loadChildren: () => import('./chat/chat.module').then(m => m.FarhatyChatModule)
      },
      {
        path: 'setting',
        loadChildren: () => import('./setting/setting.module').then(m => m.FarhatySettingModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class FarhatyEntityModule {}
