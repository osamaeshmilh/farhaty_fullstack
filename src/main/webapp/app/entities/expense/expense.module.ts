import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared/shared.module';
import { ExpenseComponent } from './expense.component';
import { ExpenseDetailComponent } from './expense-detail.component';
import { ExpenseUpdateComponent } from './expense-update.component';
import { ExpenseDeletePopupComponent, ExpenseDeleteDialogComponent } from './expense-delete-dialog.component';
import { expenseRoute, expensePopupRoute } from './expense.route';

const ENTITY_STATES = [...expenseRoute, ...expensePopupRoute];

@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ExpenseComponent,
    ExpenseDetailComponent,
    ExpenseUpdateComponent,
    ExpenseDeleteDialogComponent,
    ExpenseDeletePopupComponent
  ],
  entryComponents: [ExpenseDeleteDialogComponent]
})
export class FarhatyExpenseModule {}
