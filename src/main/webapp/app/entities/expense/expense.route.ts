import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Expense } from 'app/shared/model/expense.model';
import { ExpenseService } from './expense.service';
import { ExpenseComponent } from './expense.component';
import { ExpenseDetailComponent } from './expense-detail.component';
import { ExpenseUpdateComponent } from './expense-update.component';
import { ExpenseDeletePopupComponent } from './expense-delete-dialog.component';
import { IExpense } from 'app/shared/model/expense.model';

@Injectable({ providedIn: 'root' })
export class ExpenseResolve implements Resolve<IExpense> {
  constructor(private service: ExpenseService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IExpense> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Expense>) => response.ok),
        map((expense: HttpResponse<Expense>) => expense.body)
      );
    }
    return of(new Expense());
  }
}

export const expenseRoute: Routes = [
  {
    path: '',
    component: ExpenseComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Expenses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ExpenseDetailComponent,
    resolve: {
      expense: ExpenseResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Expenses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ExpenseUpdateComponent,
    resolve: {
      expense: ExpenseResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Expenses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ExpenseUpdateComponent,
    resolve: {
      expense: ExpenseResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Expenses'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const expensePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ExpenseDeletePopupComponent,
    resolve: {
      expense: ExpenseResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Expenses'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
