import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster-material';
import { IExtra, Extra } from 'app/shared/model/extra.model';
import { ExtraService } from './extra.service';
import { IHall } from 'app/shared/model/hall.model';
import { HallService } from 'app/entities/hall/hall.service';
import { IReservation } from 'app/shared/model/reservation.model';
import { ReservationService } from 'app/entities/reservation/reservation.service';

@Component({
  selector: 'jhi-extra-update',
  templateUrl: './extra-update.component.html'
})
export class ExtraUpdateComponent implements OnInit {
  isSaving: boolean;

  halls: IHall[];

  reservations: IReservation[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    price: [],
    active: [null, [Validators.required]],
    hallId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected extraService: ExtraService,
    protected hallService: HallService,
    protected reservationService: ReservationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ extra }) => {
      this.updateForm(extra);
    });
    this.hallService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IHall[]>) => mayBeOk.ok),
        map((response: HttpResponse<IHall[]>) => response.body)
      )
      .subscribe((res: IHall[]) => (this.halls = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.reservationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IReservation[]>) => mayBeOk.ok),
        map((response: HttpResponse<IReservation[]>) => response.body)
      )
      .subscribe((res: IReservation[]) => (this.reservations = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(extra: IExtra) {
    this.editForm.patchValue({
      id: extra.id,
      name: extra.name,
      price: extra.price,
      active: extra.active,
      hallId: extra.hallId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const extra = this.createFromForm();
    if (extra.id !== undefined) {
      this.subscribeToSaveResponse(this.extraService.update(extra));
    } else {
      this.subscribeToSaveResponse(this.extraService.create(extra));
    }
  }

  private createFromForm(): IExtra {
    return {
      ...new Extra(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      price: this.editForm.get(['price']).value,
      active: this.editForm.get(['active']).value,
      hallId: this.editForm.get(['hallId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExtra>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackHallById(index: number, item: IHall) {
    return item.id;
  }

  trackReservationById(index: number, item: IReservation) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
