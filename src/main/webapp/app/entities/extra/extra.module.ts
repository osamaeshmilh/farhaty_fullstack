import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared/shared.module';
import { ExtraComponent } from './extra.component';
import { ExtraDetailComponent } from './extra-detail.component';
import { ExtraUpdateComponent } from './extra-update.component';
import { ExtraDeletePopupComponent, ExtraDeleteDialogComponent } from './extra-delete-dialog.component';
import { extraRoute, extraPopupRoute } from './extra.route';

const ENTITY_STATES = [...extraRoute, ...extraPopupRoute];

@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [ExtraComponent, ExtraDetailComponent, ExtraUpdateComponent, ExtraDeleteDialogComponent, ExtraDeletePopupComponent],
  entryComponents: [ExtraDeleteDialogComponent]
})
export class FarhatyExtraModule {}
