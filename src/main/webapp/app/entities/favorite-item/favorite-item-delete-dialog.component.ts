import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFavoriteItem } from 'app/shared/model/favorite-item.model';
import { FavoriteItemService } from './favorite-item.service';

@Component({
  selector: 'jhi-favorite-item-delete-dialog',
  templateUrl: './favorite-item-delete-dialog.component.html'
})
export class FavoriteItemDeleteDialogComponent {
  favoriteItem: IFavoriteItem;

  constructor(
    protected favoriteItemService: FavoriteItemService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.favoriteItemService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'favoriteItemListModification',
        content: 'Deleted an favoriteItem'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-favorite-item-delete-popup',
  template: ''
})
export class FavoriteItemDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ favoriteItem }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(FavoriteItemDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.favoriteItem = favoriteItem;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/favorite-item', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/favorite-item', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
