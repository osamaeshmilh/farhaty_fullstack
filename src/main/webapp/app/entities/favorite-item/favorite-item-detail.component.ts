import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFavoriteItem } from 'app/shared/model/favorite-item.model';

@Component({
  selector: 'jhi-favorite-item-detail',
  templateUrl: './favorite-item-detail.component.html'
})
export class FavoriteItemDetailComponent implements OnInit {
  favoriteItem: IFavoriteItem;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ favoriteItem }) => {
      this.favoriteItem = favoriteItem;
    });
  }

  previousState() {
    window.history.back();
  }
}
