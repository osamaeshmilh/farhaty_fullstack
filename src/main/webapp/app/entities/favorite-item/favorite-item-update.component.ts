import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IFavoriteItem, FavoriteItem } from 'app/shared/model/favorite-item.model';
import { FavoriteItemService } from './favorite-item.service';
import { IItem } from 'app/shared/model/item.model';
import { ItemService } from 'app/entities/item/item.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-favorite-item-update',
  templateUrl: './favorite-item-update.component.html'
})
export class FavoriteItemUpdateComponent implements OnInit {
  isSaving: boolean;

  items: IItem[];

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    itemId: [],
    userId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected favoriteItemService: FavoriteItemService,
    protected itemService: ItemService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ favoriteItem }) => {
      this.updateForm(favoriteItem);
    });
    this.itemService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IItem[]>) => mayBeOk.ok),
        map((response: HttpResponse<IItem[]>) => response.body)
      )
      .subscribe((res: IItem[]) => (this.items = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(favoriteItem: IFavoriteItem) {
    this.editForm.patchValue({
      id: favoriteItem.id,
      itemId: favoriteItem.itemId,
      userId: favoriteItem.userId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const favoriteItem = this.createFromForm();
    if (favoriteItem.id !== undefined) {
      this.subscribeToSaveResponse(this.favoriteItemService.update(favoriteItem));
    } else {
      this.subscribeToSaveResponse(this.favoriteItemService.create(favoriteItem));
    }
  }

  private createFromForm(): IFavoriteItem {
    return {
      ...new FavoriteItem(),
      id: this.editForm.get(['id']).value,
      itemId: this.editForm.get(['itemId']).value,
      userId: this.editForm.get(['userId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFavoriteItem>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackItemById(index: number, item: IItem) {
    return item.id;
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
