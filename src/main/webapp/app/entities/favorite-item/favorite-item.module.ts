import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared/shared.module';
import { FavoriteItemComponent } from './favorite-item.component';
import { FavoriteItemDetailComponent } from './favorite-item-detail.component';
import { FavoriteItemUpdateComponent } from './favorite-item-update.component';
import { FavoriteItemDeletePopupComponent, FavoriteItemDeleteDialogComponent } from './favorite-item-delete-dialog.component';
import { favoriteItemRoute, favoriteItemPopupRoute } from './favorite-item.route';

const ENTITY_STATES = [...favoriteItemRoute, ...favoriteItemPopupRoute];

@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    FavoriteItemComponent,
    FavoriteItemDetailComponent,
    FavoriteItemUpdateComponent,
    FavoriteItemDeleteDialogComponent,
    FavoriteItemDeletePopupComponent
  ],
  entryComponents: [FavoriteItemDeleteDialogComponent]
})
export class FarhatyFavoriteItemModule {}
