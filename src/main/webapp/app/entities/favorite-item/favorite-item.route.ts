import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { FavoriteItem } from 'app/shared/model/favorite-item.model';
import { FavoriteItemService } from './favorite-item.service';
import { FavoriteItemComponent } from './favorite-item.component';
import { FavoriteItemDetailComponent } from './favorite-item-detail.component';
import { FavoriteItemUpdateComponent } from './favorite-item-update.component';
import { FavoriteItemDeletePopupComponent } from './favorite-item-delete-dialog.component';
import { IFavoriteItem } from 'app/shared/model/favorite-item.model';

@Injectable({ providedIn: 'root' })
export class FavoriteItemResolve implements Resolve<IFavoriteItem> {
  constructor(private service: FavoriteItemService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IFavoriteItem> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<FavoriteItem>) => response.ok),
        map((favoriteItem: HttpResponse<FavoriteItem>) => favoriteItem.body)
      );
    }
    return of(new FavoriteItem());
  }
}

export const favoriteItemRoute: Routes = [
  {
    path: '',
    component: FavoriteItemComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'FavoriteItems'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: FavoriteItemDetailComponent,
    resolve: {
      favoriteItem: FavoriteItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'FavoriteItems'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: FavoriteItemUpdateComponent,
    resolve: {
      favoriteItem: FavoriteItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'FavoriteItems'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: FavoriteItemUpdateComponent,
    resolve: {
      favoriteItem: FavoriteItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'FavoriteItems'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const favoriteItemPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: FavoriteItemDeletePopupComponent,
    resolve: {
      favoriteItem: FavoriteItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'FavoriteItems'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
