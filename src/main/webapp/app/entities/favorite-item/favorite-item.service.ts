import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFavoriteItem } from 'app/shared/model/favorite-item.model';

type EntityResponseType = HttpResponse<IFavoriteItem>;
type EntityArrayResponseType = HttpResponse<IFavoriteItem[]>;

@Injectable({ providedIn: 'root' })
export class FavoriteItemService {
  public resourceUrl = SERVER_API_URL + 'api/favorite-items';

  constructor(protected http: HttpClient) {}

  create(favoriteItem: IFavoriteItem): Observable<EntityResponseType> {
    return this.http.post<IFavoriteItem>(this.resourceUrl, favoriteItem, { observe: 'response' });
  }

  update(favoriteItem: IFavoriteItem): Observable<EntityResponseType> {
    return this.http.put<IFavoriteItem>(this.resourceUrl, favoriteItem, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFavoriteItem>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFavoriteItem[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
