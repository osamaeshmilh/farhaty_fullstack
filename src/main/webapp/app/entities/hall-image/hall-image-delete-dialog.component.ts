import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHallImage } from 'app/shared/model/hall-image.model';
import { HallImageService } from './hall-image.service';

@Component({
  selector: 'jhi-hall-image-delete-dialog',
  templateUrl: './hall-image-delete-dialog.component.html'
})
export class HallImageDeleteDialogComponent {
  hallImage: IHallImage;

  constructor(protected hallImageService: HallImageService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.hallImageService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'hallImageListModification',
        content: 'Deleted an hallImage'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-hall-image-delete-popup',
  template: ''
})
export class HallImageDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ hallImage }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(HallImageDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.hallImage = hallImage;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/hall-image', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/hall-image', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
