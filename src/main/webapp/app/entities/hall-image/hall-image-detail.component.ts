import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IHallImage } from 'app/shared/model/hall-image.model';

@Component({
  selector: 'jhi-hall-image-detail',
  templateUrl: './hall-image-detail.component.html'
})
export class HallImageDetailComponent implements OnInit {
  hallImage: IHallImage;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ hallImage }) => {
      this.hallImage = hallImage;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
