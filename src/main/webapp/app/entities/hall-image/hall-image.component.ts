import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiDataUtils } from 'ng-jhipster-material';

import { IHallImage } from 'app/shared/model/hall-image.model';
import { AccountService } from 'app/core/auth/account.service';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HallImageService } from './hall-image.service';
import { PageEvent } from '@angular/material/paginator';
import { AlertDialog } from 'app/shared/alert-dialog/alert-dialog.component';

@Component({
  selector: 'jhi-hall-image',
  templateUrl: './hall-image.component.html'
})
export class HallImageComponent implements OnInit, OnDestroy {
  currentAccount: any;
  hallImages: IHallImage[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;

  direction = 'asc';
  currentSearch: string;
  isLoading = true;

  constructor(
    protected hallImageService: HallImageService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected router: Router,
    protected eventManager: JhiEventManager,
    private alertDialog: AlertDialog
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.hallImageService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IHallImage[]>) => this.paginateHallImages(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.itemsPerPage = event.pageSize;
    this.previousPage = this.page;
    this.transition();
  }

  transition() {
    this.router.navigate(['/hall-image'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/hall-image',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.page = 0;
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInHallImages();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IHallImage) {
    return item.id;
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  registerChangeInHallImages() {
    this.eventSubscriber = this.eventManager.subscribe('hallImageListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateHallImages(data: IHallImage[], headers: HttpHeaders) {
    this.isLoading = false;
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.hallImages = data;
  }

  protected onError(errorMessage: string) {
    this.isLoading = false;
    this.jhiAlertService.error(errorMessage, null, null);
  }

  delete(id: number) {
    this.alertDialog.open({ message: 'سيتم حذف هذا العنصر نهائيا' }).subscribe(confimred => {
      if (confimred) {
        this.hallImageService.delete(id).subscribe(res => {
          this.loadAll();
        });
      }
    });
  }
}
