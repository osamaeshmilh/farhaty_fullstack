import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared/shared.module';
import { HallImageComponent } from './hall-image.component';
import { HallImageDetailComponent } from './hall-image-detail.component';
import { HallImageUpdateComponent } from './hall-image-update.component';
import { HallImageDeletePopupComponent, HallImageDeleteDialogComponent } from './hall-image-delete-dialog.component';
import { hallImageRoute, hallImagePopupRoute } from './hall-image.route';
import { RoomImageUpdateComponent } from 'app/entities/hall-image/room-image-update.component';

const ENTITY_STATES = [...hallImageRoute, ...hallImagePopupRoute];

@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    HallImageComponent,
    HallImageDetailComponent,
    HallImageUpdateComponent,
    HallImageDeleteDialogComponent,
    HallImageDeletePopupComponent,
    RoomImageUpdateComponent
  ],
  entryComponents: [HallImageDeleteDialogComponent]
})
export class FarhatyHallImageModule {}
