import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster-material';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HallImage } from 'app/shared/model/hall-image.model';
import { HallImageService } from './hall-image.service';
import { HallImageComponent } from './hall-image.component';
import { HallImageDetailComponent } from './hall-image-detail.component';
import { HallImageUpdateComponent } from './hall-image-update.component';
import { HallImageDeletePopupComponent } from './hall-image-delete-dialog.component';
import { IHallImage } from 'app/shared/model/hall-image.model';
import { Room } from 'app/shared/model/room.model';
import { RoomImageUpdateComponent } from 'app/entities/hall-image/room-image-update.component';

@Injectable({ providedIn: 'root' })
export class HallImageResolve implements Resolve<IHallImage> {
  constructor(private service: HallImageService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IHallImage> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<HallImage>) => response.ok),
        map((hallImage: HttpResponse<HallImage>) => hallImage.body)
      );
    }
    return of(new HallImage());
  }
}

export const hallImageRoute: Routes = [
  {
    path: '',
    component: HallImageComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'HallImages'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: HallImageDetailComponent,
    resolve: {
      hallImage: HallImageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HallImages'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: HallImageUpdateComponent,
    resolve: {
      hallImage: HallImageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HallImages'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: HallImageUpdateComponent,
    resolve: {
      hallImage: HallImageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HallImages'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/room-image',
    component: RoomImageUpdateComponent,
    resolve: {
      hallImage: HallImageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HallImages'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const hallImagePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: HallImageDeletePopupComponent,
    resolve: {
      hallImage: HallImageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'HallImages'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
