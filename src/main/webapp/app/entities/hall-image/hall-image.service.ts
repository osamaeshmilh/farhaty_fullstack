import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IHallImage } from 'app/shared/model/hall-image.model';

type EntityResponseType = HttpResponse<IHallImage>;
type EntityArrayResponseType = HttpResponse<IHallImage[]>;

@Injectable({ providedIn: 'root' })
export class HallImageService {
  public resourceUrl = SERVER_API_URL + 'api/hall-images';

  constructor(protected http: HttpClient) {}

  create(hallImage: IHallImage): Observable<EntityResponseType> {
    return this.http.post<IHallImage>(this.resourceUrl, hallImage, { observe: 'response' });
  }

  update(hallImage: IHallImage): Observable<EntityResponseType> {
    return this.http.put<IHallImage>(this.resourceUrl, hallImage, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IHallImage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IHallImage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
