import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster-material';

import { IHall } from 'app/shared/model/hall.model';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { IRoom } from 'app/shared/model/room.model';
import { RoomService } from 'app/entities/room/room.service';
import { IType } from 'app/shared/model/type.model';
import { TypeService } from 'app/entities/type/type.service';
import { IExtra } from 'app/shared/model/extra.model';
import { ExtraService } from 'app/entities/extra/extra.service';
import { HallImage, IHallImage } from 'app/shared/model/hall-image.model';
import { IStoreImage } from 'app/shared/model/store-image.model';
import { HallImageService } from 'app/entities/hall-image/hall-image.service';
import { StoreImageDialogComponent } from 'app/entities/store/store-image-dialog/store-image-dialog.component';
import { filter, map } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { HallImageDialogComponent } from 'app/entities/hall/hall-image-dialog/hall-image-dialog.component';
import { IRevenue } from 'app/shared/model/revenue.model';
import { IExpense } from 'app/shared/model/expense.model';
import { RevenueService } from 'app/entities/revenue/revenue.service';
import { ExpenseService } from 'app/entities/expense/expense.service';

@Component({
  selector: 'jhi-hall-detail',
  templateUrl: './hall-detail.component.html'
})
export class HallDetailComponent implements OnInit {
  hall: IHall;
  rooms: IRoom[];
  types: IType[];
  extras: IExtra[];
  hallImages: HallImage[];
  revenues: IRevenue[];
  expenses: IExpense[];

  constructor(
    protected dataUtils: JhiDataUtils,
    protected activatedRoute: ActivatedRoute,
    protected roomService: RoomService,
    protected typeService: TypeService,
    protected extraService: ExtraService,
    protected hallImageService: HallImageService,
    protected revenueService: RevenueService,
    protected expenseService: ExpenseService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ hall }) => {
      this.hall = hall;
    });

    this.roomService
      .query({
        'hallId.equals': this.hall.id
      })
      .subscribe(
        (res: HttpResponse<IRoom[]>) => {
          this.rooms = res.body;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    this.typeService
      .query({
        'hallId.equals': this.hall.id
      })
      .subscribe(
        (res: HttpResponse<IType[]>) => {
          this.types = res.body;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    this.extraService
      .query({
        'hallId.equals': this.hall.id
      })
      .subscribe(
        (res: HttpResponse<IExtra[]>) => {
          this.extras = res.body;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    this.hallImageService
      .query({
        'hallId.equals': this.hall.id
      })
      .subscribe(
        (res: HttpResponse<IHallImage[]>) => {
          this.hallImages = res.body;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    this.revenueService
      .query({
        'hallId.equals': this.hall.id
      })
      .subscribe(
        (res: HttpResponse<IRevenue[]>) => {
          this.revenues = res.body;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    this.expenseService
      .query({
        'hallId.equals': this.hall.id
      })
      .subscribe(
        (res: HttpResponse<IExpense[]>) => {
          this.expenses = res.body;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }

  private onError(message: string) {}

  openImagesDialog(): void {
    const dialogRef = this.dialog.open(HallImageDialogComponent, {
      width: '650px',
      data: this.hall.id
    });

    dialogRef.afterClosed().subscribe(attribute => {
      this.hallImageService
        .query({
          'hallId.equals': this.hall.id
        })
        .pipe(
          filter((mayBeOk: HttpResponse<IHallImage[]>) => mayBeOk.ok),
          map((response: HttpResponse<IHallImage[]>) => response.body)
        )
        .subscribe((res: IHallImage[]) => (this.hallImages = res), (res: HttpErrorResponse) => this.onError(res.message));
    });
  }
}
