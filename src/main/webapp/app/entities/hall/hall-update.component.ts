import { Component, ElementRef, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster-material';
import { IHall, Hall } from 'app/shared/model/hall.model';
import { HallService } from './hall.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IMarker } from 'app/shared/model/marker.model';

@Component({
  selector: 'jhi-hall-update',
  templateUrl: './hall-update.component.html'
})
export class HallUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  lat = 32.87849;
  lng = 13.187364;

  markers: IMarker[] = [
    {
      lat: null,
      lng: null,
      label: '',
      draggable: true
    }
  ];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    logo: [],
    logoUrl: [],
    logoContentType: [],
    address: [null, [Validators.required]],
    phone: [null, [Validators.required]],
    email: [null, [Validators.required, Validators.email]],
    lat: [],
    lng: [],
    rules: [],
    vip: [],
    userId: [],
    availableFrom: [null, [Validators.required]],
    availableTo: [null, [Validators.required]],
    pendingAppointmentPeriod: []
  });
  minDate = new Date();

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected hallService: HallService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected elementRef: ElementRef
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ hall }) => {
      this.updateForm(hall);
      this.markers = [
        {
          lat: hall.lat,
          lng: hall.lng,
          label: hall.name,
          draggable: false
        }
      ];
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(hall: IHall) {
    this.editForm.patchValue({
      id: hall.id,
      name: hall.name,
      logo: hall.logo,
      logoUrl: hall.logoUrl,
      logoContentType: hall.logoContentType,
      address: hall.address,
      phone: hall.phone,
      email: hall.email,
      lat: hall.lat,
      lng: hall.lng,
      rules: hall.rules,
      vip: hall.vip,
      userId: hall.userId,
      availableFrom: hall.availableFrom,
      availableTo: hall.availableTo,
      pendingAppointmentPeriod: hall.pendingAppointmentPeriod
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const hall = this.createFromForm();
    hall.lat = this.markers[0].lat;
    hall.lng = this.markers[0].lng;
    if (hall.id !== undefined) {
      this.subscribeToSaveResponse(this.hallService.update(hall));
    } else {
      this.subscribeToSaveResponse(this.hallService.create(hall));
    }
  }

  private createFromForm(): IHall {
    return {
      ...new Hall(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      logoContentType: this.editForm.get(['logoContentType']).value,
      logo: this.editForm.get(['logo']).value,
      logoUrl: this.editForm.get(['logoUrl']).value,
      address: this.editForm.get(['address']).value,
      phone: this.editForm.get(['phone']).value,
      email: this.editForm.get(['email']).value,
      lat: this.editForm.get(['lat']).value,
      lng: this.editForm.get(['lng']).value,
      rules: this.editForm.get(['rules']).value,
      vip: this.editForm.get(['vip']).value,
      userId: this.editForm.get(['userId']).value,
      availableFrom: this.editForm.get(['availableFrom']).value,
      availableTo: this.editForm.get(['availableTo']).value,
      pendingAppointmentPeriod: this.editForm.get(['pendingAppointmentPeriod']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHall>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  mapClicked($event: any) {
    console.log($event.coords.lat, $event.coords.lng);
    this.markers[0].lat = $event.coords.lat;
    this.markers[0].lng = $event.coords.lng;
  }

  markerDragEnd(m: IMarker, $event: any) {
    console.log('dragEnd', $event);
    this.markers[0].lat = $event.coords.lat;
    this.markers[0].lng = $event.coords.lng;
  }
}
