import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiDataUtils } from 'ng-jhipster-material';

import { IHall } from 'app/shared/model/hall.model';
import { AccountService } from 'app/core/auth/account.service';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HallService } from './hall.service';
import { IInvoice } from 'app/shared/model/invoice.model';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { IReservation, Reservation } from 'app/shared/model/reservation.model';
import { ReservationService } from 'app/entities/reservation/reservation.service';
import { AlertDialog } from 'app/shared/alert-dialog/alert-dialog.component';

@Component({
  selector: 'jhi-hall',
  templateUrl: './hall.component.html'
})
export class HallComponent implements OnInit, OnDestroy {
  currentAccount: any;
  halls: IHall[];

  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;

  direction = 'asc';
  currentSearch: string;
  isLoading = true;
  reservations: IReservation[];

  constructor(
    protected hallService: HallService,
    protected reservationService: ReservationService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected router: Router,
    protected eventManager: JhiEventManager,
    private alertDialog: AlertDialog
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.isLoading = true;
    if (this.currentSearch) {
      //search
      this.hallService
        .query({
          'name.contains': this.currentSearch
        })
        .pipe(
          filter((res: HttpResponse<IHall[]>) => res.ok),
          map((res: HttpResponse<IHall[]>) => res.body)
        )

        .subscribe(
          (res: IHall[]) => {
            this.isLoading = false;
            this.halls = res;
          },
          (err: HttpErrorResponse) => this.onError(err.message)
        );
      return;
    }

    this.hallService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IHall[]>) => this.paginateHalls(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.itemsPerPage = event.pageSize;
    this.previousPage = this.page;
    this.transition();
  }

  transition() {
    this.router.navigate(['/hall'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.currentSearch = '';
    this.page = 0;
    this.router.navigate([
      '/hall',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.currentSearch = '';
    this.page = 0;
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInHalls();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IHall) {
    return item.id;
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  registerChangeInHalls() {
    this.eventSubscriber = this.eventManager.subscribe('hallListModification', response => this.loadAll());
  }

  sort() {
    return `${this.predicate},${this.direction}`;
  }

  sortData(sort: Sort) {
    this.predicate = sort.active;
    this.direction = sort.direction;
    this.transition();
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.loadAll();
  }

  protected paginateHalls(data: IHall[], headers: HttpHeaders) {
    this.isLoading = false;
    this.currentSearch = '';
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.halls = data;
  }

  protected onError(errorMessage: string) {
    this.isLoading = false;
    this.jhiAlertService.error(errorMessage, null, null);
  }

  getReservations(hallId: number): IReservation[] {
    this.reservationService
      .query({
        'hallId.equals': hallId
      })
      .pipe(
        filter((res: HttpResponse<IReservation[]>) => res.ok),
        map((res: HttpResponse<IReservation[]>) => res.body)
      )

      .subscribe(
        (res: IReservation[]) => {
          this.reservations = res;
        },
        (err: HttpErrorResponse) => this.onError(err.message)
      );

    return this.reservations;
  }

  delete(id: number) {
    this.alertDialog.open({ message: 'سيتم حذف هذا العنصر نهائيا' }).subscribe(confimred => {
      if (confimred) {
        this.hallService.delete(id).subscribe(res => {
          this.loadAll();
        });
      }
    });
  }
}
