import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared/shared.module';
import { HallComponent } from './hall.component';
import { HallDetailComponent } from './hall-detail.component';
import { HallUpdateComponent } from './hall-update.component';
import { HallDeletePopupComponent, HallDeleteDialogComponent } from './hall-delete-dialog.component';
import { hallRoute, hallPopupRoute } from './hall.route';
import { HallImageDialogComponent } from 'app/entities/hall/hall-image-dialog/hall-image-dialog.component';
import { AgmCoreModule } from '@agm/core';

const ENTITY_STATES = [...hallRoute, ...hallPopupRoute];

@NgModule({
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAncdi41FFxPV6NUgpgZ8U_0CMgVgBlNHU'
    }),
    FarhatySharedModule,
    RouterModule.forChild(ENTITY_STATES)
  ],
  declarations: [
    HallComponent,
    HallDetailComponent,
    HallUpdateComponent,
    HallDeleteDialogComponent,
    HallDeletePopupComponent,
    HallImageDialogComponent
  ],
  entryComponents: [HallDeleteDialogComponent, HallImageDialogComponent]
})
export class FarhatyHallModule {}
