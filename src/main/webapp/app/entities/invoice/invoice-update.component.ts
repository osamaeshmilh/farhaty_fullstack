import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster-material';
import { IInvoice, Invoice } from 'app/shared/model/invoice.model';
import { InvoiceService } from './invoice.service';
import { IReservation } from 'app/shared/model/reservation.model';
import { ReservationService } from 'app/entities/reservation/reservation.service';
import { InvoiceType } from 'app/shared/model/enumerations/invoice-type.model';
import { AlertDialog } from 'app/shared/alert-dialog/alert-dialog.component';

@Component({
  selector: 'jhi-invoice-update',
  templateUrl: './invoice-update.component.html'
})
export class InvoiceUpdateComponent implements OnInit {
  isSaving: boolean;

  reservations: IReservation[];
  invoiceDateDp: any;
  reservationId: any;
  reservation: IReservation;

  editForm = this.fb.group({
    id: [],
    invoiceDate: [null, [Validators.required]],
    amount: [null, [Validators.required]],
    details: [],
    invoiceType: [null, [Validators.required]],
    reservationId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected invoiceService: InvoiceService,
    protected reservationService: ReservationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private alertDialog: AlertDialog
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ invoice }) => {
      this.updateForm(invoice);

      this.reservationId =
        this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['reservationId']
          ? this.activatedRoute.snapshot.queryParams['reservationId']
          : invoice.reservationId;

      this.reservationService
        .find(this.reservationId)
        .subscribe(
          (res: HttpResponse<IReservation>) => (this.reservation = res.body),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
    });
  }

  updateForm(invoice: IInvoice) {
    this.editForm.patchValue({
      id: invoice.id,
      invoiceDate: invoice.invoiceDate,
      amount: invoice.amount,
      details: invoice.details,
      invoiceType: invoice.invoiceType,
      reservationId: invoice.reservationId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const invoice = this.createFromForm();
    if (
      this.getTotalPayed(this.reservation) + invoice.amount > this.reservation.total &&
      invoice.invoiceType == InvoiceType.PARTIAL_PAYMENT
    ) {
      this.isSaving = false;
      this.alertDialog.open({ message: 'خطأ! اجمالي المبلغ المدفوع اكثر من قيمة الحجز !' }).subscribe(confimred => {});
    } else if (invoice.amount > this.getTotalPayed(this.reservation) && invoice.invoiceType == InvoiceType.RETURN) {
      this.isSaving = false;
      this.alertDialog.open({ message: 'خطأ! اجمالي المبلغ المسترد اكثر من قيمة المدفوع !' }).subscribe(confimred => {});
    } else {
      if (invoice.id !== undefined) {
        this.subscribeToSaveResponse(this.invoiceService.update(invoice));
      } else {
        this.subscribeToSaveResponse(this.invoiceService.create(invoice));
      }
    }
  }

  private createFromForm(): IInvoice {
    return {
      ...new Invoice(),
      id: this.editForm.get(['id']).value,
      invoiceDate: this.editForm.get(['invoiceDate']).value,
      amount: this.editForm.get(['amount']).value,
      details: this.editForm.get(['details']).value,
      invoiceType: this.editForm.get(['invoiceType']).value,
      reservationId: this.reservationId
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInvoice>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackReservationById(index: number, item: IReservation) {
    return item.id;
  }

  getTotalPayed(reservation: IReservation) {
    var total = 0;
    reservation.invoices.forEach(invoice => {
      if (invoice.invoiceType == InvoiceType.PARTIAL_PAYMENT) {
        total += invoice.amount;
      } else if (invoice.invoiceType == InvoiceType.RETURN) {
        total -= invoice.amount;
      }
    });
    return total;
  }

  getTotalLeft(reservation: IReservation) {
    var total = 0;
    reservation.invoices.forEach(invoice => {
      if (invoice.invoiceType == InvoiceType.PARTIAL_PAYMENT) {
        total += invoice.amount;
      } else if (invoice.invoiceType == InvoiceType.RETURN) {
        total -= invoice.amount;
      }
    });
    return reservation.total - total;
  }
}
