import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared/shared.module';
import { InvoiceComponent } from './invoice.component';
import { InvoiceDetailComponent } from './invoice-detail.component';
import { InvoiceUpdateComponent } from './invoice-update.component';
import { InvoiceDeletePopupComponent, InvoiceDeleteDialogComponent } from './invoice-delete-dialog.component';
import { invoiceRoute, invoicePopupRoute } from './invoice.route';
import { InvoicePayComponent } from 'app/entities/invoice/invoice-pay.component';
import { NgxPrintModule } from 'ngx-print';

const ENTITY_STATES = [...invoiceRoute, ...invoicePopupRoute];

@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(ENTITY_STATES), NgxPrintModule],
  declarations: [
    InvoiceComponent,
    InvoiceDetailComponent,
    InvoiceUpdateComponent,
    InvoiceDeleteDialogComponent,
    InvoiceDeletePopupComponent,
    InvoicePayComponent
  ],
  entryComponents: [InvoiceDeleteDialogComponent]
})
export class FarhatyInvoiceModule {}
