import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'app/entities/notification/notification.service';
import { Notification } from 'app/shared/model/notification.model';

@Component({
  selector: 'jhi-notification-update',
  templateUrl: './notification-update.component.html'
})
export class NotificationUpdateComponent implements OnInit {
  isLoading = true;
  title = '';
  description = '';
  toggled: boolean = false;

  constructor(protected notificationService: NotificationService) {}

  ngOnInit(): void {}

  sendNotification() {
    const notification = new Notification();
    notification.description = this.description;
    notification.title = this.title;

    const body = {
      notification: {
        body: `${this.description}`,
        title: `${this.title}`,
        sound: 'default'
      },
      priority: 'high',
      data: { click_action: 'FLUTTER_NOTIFICATION_CLICK', id: '1', status: 'done' },
      to: '/topics/all'
    };
    this.notificationService.sendNotification(body).subscribe(value => {
      this.notificationService.create(notification).subscribe((res1: any) => {
        this.title = '';
        this.description = '';
        this.previousState();
      });
    });
  }

  handleSelection(event) {
    this.description += event.char;
  }

  previousState() {
    window.history.back();
  }
}
