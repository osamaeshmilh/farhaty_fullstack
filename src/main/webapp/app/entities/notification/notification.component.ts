import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster-material';

import { INotification } from 'app/shared/model/notification.model';
import { AccountService } from 'app/core/auth/account.service';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { NotificationService } from './notification.service';
import { IReservation } from 'app/shared/model/reservation.model';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { AlertDialog } from 'app/shared/alert-dialog/alert-dialog.component';

@Component({
  selector: 'jhi-notification',
  templateUrl: './notification.component.html'
})
export class NotificationComponent implements OnInit, OnDestroy {
  currentAccount: any;
  notifications: INotification[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;

  direction = 'asc';
  currentSearch: string;
  isLoading = true;

  constructor(
    protected notificationService: NotificationService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    private alertDialog: AlertDialog
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.isLoading = true;
    if (this.currentSearch) {
      //search
      this.notificationService
        .query({
          'title.contains': this.currentSearch
        })
        .pipe(
          filter((res: HttpResponse<INotification[]>) => res.ok),
          map((res: HttpResponse<INotification[]>) => res.body)
        )

        .subscribe(
          (res: INotification[]) => {
            this.isLoading = false;
            this.notifications = res;
          },
          (err: HttpErrorResponse) => this.onError(err.message)
        );
      return;
    }

    this.notificationService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<INotification[]>) => this.paginateNotifications(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.itemsPerPage = event.pageSize;
    this.previousPage = this.page;
    this.transition();
  }

  transition() {
    this.router.navigate(['/notification'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.currentSearch = '';
    this.page = 0;
    this.router.navigate([
      '/notification',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.currentSearch = '';
    this.page = 0;
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInNotifications();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: INotification) {
    return item.id;
  }

  registerChangeInNotifications() {
    this.eventSubscriber = this.eventManager.subscribe('notificationListModification', response => this.loadAll());
  }

  sort() {
    return `${this.predicate},${this.direction}`;
  }

  sortData(sort: Sort) {
    this.predicate = sort.active;
    this.direction = sort.direction;
    this.transition();
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.loadAll();
  }

  protected paginateNotifications(data: INotification[], headers: HttpHeaders) {
    this.isLoading = false;
    this.currentSearch = '';
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.notifications = data;
  }

  protected onError(errorMessage: string) {
    this.isLoading = false;
    this.jhiAlertService.error(errorMessage, null, null);
  }

  delete(id: number) {
    this.alertDialog.open({ message: 'سيتم حذف هذا العنصر نهائيا' }).subscribe(confimred => {
      if (confimred) {
        this.notificationService.delete(id).subscribe(res => {
          this.loadAll();
        });
      }
    });
  }
}
