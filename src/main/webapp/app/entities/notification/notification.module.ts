import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared/shared.module';
import { NotificationComponent } from './notification.component';
import { NotificationDetailComponent } from './notification-detail.component';
import { NotificationUpdateComponent } from './notification-update.component';
import { NotificationDeletePopupComponent, NotificationDeleteDialogComponent } from './notification-delete-dialog.component';
import { notificationRoute, notificationPopupRoute } from './notification.route';
import { NgxEmojiPickerModule } from 'ngx-emoji-picker';

const ENTITY_STATES = [...notificationRoute, ...notificationPopupRoute];

@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(ENTITY_STATES), NgxEmojiPickerModule],
  declarations: [
    NotificationComponent,
    NotificationDetailComponent,
    NotificationUpdateComponent,
    NotificationDeleteDialogComponent,
    NotificationDeletePopupComponent
  ],
  entryComponents: [NotificationDeleteDialogComponent]
})
export class FarhatyNotificationModule {}
