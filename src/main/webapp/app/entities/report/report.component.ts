import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AccountService } from 'app/core';
import { formatDate } from '@angular/common';
import { IReport } from 'app/shared/model/report.model';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { IHall } from 'app/shared/model/hall.model';
import { HallService } from 'app/entities/hall/hall.service';
import { MatOptionSelectionChange } from '@angular/material/core';

@Component({
  selector: 'jhi-report',
  templateUrl: './report.component.html'
})
export class ReportComponent implements OnInit {
  currentAccount: any;
  eventSubscriber: Subscription;
  from: any;
  to: any;
  private user: any;
  format = 'yyyy-MM-dd';
  locale = 'en-US';
  report: IReport;
  ordersCount: number = 0;
  isLoading: boolean = false;
  hallId: any;
  halls: IHall[];

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService,
    private hallService: HallService
  ) {}

  ngOnInit() {
    this.hallService.query().subscribe(
      (res: HttpResponse<IHall[]>) => {
        this.halls = res.body;
      },
      (res: HttpErrorResponse) => {
        console.log(res.message);
      }
    );
  }

  getXslx() {
    let url;
    if (this.from == null && this.to == null) {
      this.from = '2021-01-01';
      this.to = '2021-01-01';
      url =
        '/api/public/reservations/xlsx/' +
        this.hallId +
        '/' +
        formatDate(this.from, this.format, this.locale) +
        '/' +
        formatDate(this.to, this.format, this.locale) +
        '/1';
    } else {
      url =
        '/api/public/reservations/xlsx/' +
        this.hallId +
        '/' +
        formatDate(this.from, this.format, this.locale) +
        '/' +
        formatDate(this.to, this.format, this.locale) +
        '/2';
    }
    window.open(url, '_blank');
  }

  getPdf() {
    let url;
    if (this.from == null && this.to == null) {
      this.from = '2021-01-01';
      this.to = '2021-01-01';
      url =
        '/api/public/reservations/pdf/' +
        this.hallId +
        '/' +
        formatDate(this.from, this.format, this.locale) +
        '/' +
        formatDate(this.to, this.format, this.locale) +
        '/1';
    } else {
      url =
        '/api/public/reservations/pdf/' +
        this.hallId +
        '/' +
        formatDate(this.from, this.format, this.locale) +
        '/' +
        formatDate(this.to, this.format, this.locale) +
        '/2';
    }
    window.open(url, '_blank');
  }

  selectHall($event: MatOptionSelectionChange, hall: IHall) {
    this.hallId = hall.id;
  }
}
