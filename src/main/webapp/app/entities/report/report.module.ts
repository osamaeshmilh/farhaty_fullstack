import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared';
import { ReportComponent, reportRoute } from './';

const ENTITY_STATES = [...reportRoute];

@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [ReportComponent],
  entryComponents: [ReportComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FarhatyReportModule {}
