import { Injectable } from '@angular/core';
import { Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { ReportComponent } from './report.component';

@Injectable({ providedIn: 'root' })
export class ReportResolve {}

export const reportRoute: Routes = [
  {
    path: '',
    component: ReportComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Reports'
    },
    canActivate: [UserRouteAccessService]
  }
];
