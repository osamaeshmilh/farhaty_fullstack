import { Component, TemplateRef, ViewChild } from '@angular/core';
import { CalendarDateFormatter, CalendarEvent, CalendarView, DAYS_OF_WEEK } from 'angular-calendar';
import { CustomDateFormatter } from './custom-date-formatter.provider';
import { Subject } from 'rxjs';
import { isSameDay, isSameMonth } from 'date-fns';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IHall } from 'app/shared/model/hall.model';
import { HallService } from 'app/entities/hall/hall.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { IReservation } from 'app/shared/model/reservation.model';
import { ReservationService } from 'app/entities/reservation/reservation.service';
import * as moment from 'moment';
import { ReservationStatus } from 'app/shared/model/enumerations/reservation-status.model';
import { ActivatedRoute, Router } from '@angular/router';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  green: {
    primary: '#14bf1c',
    secondary: '#d8ffd1'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'jhi-reservation-calender',
  templateUrl: './reservation-calender.component.html',
  styleUrls: ['./reservation-calender.component.scss'],
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    }
  ]
})
export class ReservationCalenderComponent {
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  viewDate: Date = new Date();

  locale: string = 'ar';

  weekStartsOn: number = DAYS_OF_WEEK.SATURDAY;

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  refresh: Subject<any> = new Subject();

  events: any[] = [];

  activeDayIsOpen: boolean = true;
  halls: IHall[];
  reservations: IReservation[] = null;
  selectedHall: any;

  constructor(
    private modal: NgbModal,
    private hallService: HallService,
    private reservationService: ReservationService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  handleEvent(action: string, event: Event): void {
    this.router.navigate([`../reservation/${event.reservationId}/view`], { relativeTo: this.route });
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  ngOnInit(): void {
    this.hallService.query().subscribe(
      (res: HttpResponse<IHall[]>) => {
        this.halls = res.body;
      },
      (res: HttpErrorResponse) => {
        console.log(res.message);
      }
    );
    this.reservationService.query({ size: 500 }).subscribe(
      (res: HttpResponse<IReservation[]>) => {
        this.reservations = res.body;

        for (let reservation of this.reservations) {
          console.log(moment().toDate());
          this.events.push(
            new Event(
              reservation.id,
              reservation.id + ' - ' + this.getStatus(reservation.reservationStatus),
              moment(reservation.reservationDate).toDate(),
              moment(reservation.reservationDate).toDate(),
              this.getColor(reservation.reservationStatus)
            )
          );
        }
      },
      (res: HttpErrorResponse) => {
        console.log(res.message);
      }
    );
  }

  loadAppointments(event, hall: IHall) {
    if (event.isUserInput) {
      this.events = [];
      this.reservations = null;
      this.reservationService
        .query({
          size: 500,
          'hallId.equals': hall.id
        })
        .subscribe(
          (res: HttpResponse<IReservation[]>) => {
            this.reservations = res.body;
            for (let reservation of this.reservations) {
              this.events.push(
                new Event(
                  reservation.id,
                  reservation.id + ' - ' + this.getStatus(reservation.reservationStatus),
                  moment(reservation.reservationDate).toDate(),
                  moment(reservation.reservationDate).toDate(),
                  this.getColor(reservation.reservationStatus)
                )
              );
            }
          },
          (res: HttpErrorResponse) => {
            console.log(res.message);
          }
        );
    }
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  private getStatus(reservationStatus: ReservationStatus) {
    if (reservationStatus == ReservationStatus.APPROVED) {
      return 'مقبول';
    } else if (reservationStatus == ReservationStatus.PENDING) {
      return 'قيد التأكيد';
    } else if (reservationStatus == ReservationStatus.DONE) {
      return 'تم الحجز';
    } else if (reservationStatus == ReservationStatus.CANCELED_BY_HALL) {
      return 'ملغي من الصالة';
    } else if (reservationStatus == ReservationStatus.CANCELED_BY_USER) {
      return 'ملغي من الزبون';
    } else {
      return 'غير محدد';
    }
  }

  private getColor(reservationStatus: ReservationStatus) {
    if (reservationStatus == ReservationStatus.APPROVED) {
      return colors.green;
    } else if (reservationStatus == ReservationStatus.PENDING) {
      return colors.yellow;
    } else if (reservationStatus == ReservationStatus.DONE) {
      return colors.green;
    } else if (reservationStatus == ReservationStatus.CANCELED_BY_HALL) {
      return colors.red;
    } else if (reservationStatus == ReservationStatus.CANCELED_BY_USER) {
      return colors.red;
    } else {
      return colors.yellow;
    }
  }
}

class Event {
  public reservationId: number;
  public title: string;
  public start: Date;
  public end: Date;
  public color: string;

  constructor(reservationId: number, title: string, start: Date, end: Date, color: string) {
    this.reservationId = reservationId;
    this.title = title;
    this.start = start;
    this.end = end;
    this.color = color;
  }
}
