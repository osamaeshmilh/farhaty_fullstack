import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgxLoadingModule } from 'ngx-loading';
import { FarhatySharedModule } from 'app/shared';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { registerLocaleData } from '@angular/common';
import localeAr from '@angular/common/locales/ar';
import { reservationCalenderRoute } from 'app/entities/reservation-calender/reservation-calender.route';
import { ReservationCalenderComponent } from 'app/entities/reservation-calender/reservation-calender.component';

registerLocaleData(localeAr);

const ENTITY_STATES = [...reservationCalenderRoute];

@NgModule({
  imports: [
    FarhatySharedModule,
    RouterModule.forChild(ENTITY_STATES),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    NgxLoadingModule.forRoot({})
  ],
  declarations: [ReservationCalenderComponent],
  entryComponents: [ReservationCalenderComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReservationCalenderModule {}
