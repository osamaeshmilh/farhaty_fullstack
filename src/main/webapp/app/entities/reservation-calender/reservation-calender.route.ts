import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { ReservationCalenderComponent } from 'app/entities/reservation-calender/reservation-calender.component';

export const reservationCalenderRoute: Routes = [
  {
    path: '',
    component: ReservationCalenderComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'الرئيسية'
    },
    canActivate: [UserRouteAccessService]
  }
];
