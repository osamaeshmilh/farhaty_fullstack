import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IReservation } from 'app/shared/model/reservation.model';
import { InvoiceType } from 'app/shared/model/enumerations/invoice-type.model';

@Component({
  selector: 'jhi-reservation-detail',
  templateUrl: './reservation-detail.component.html'
})
export class ReservationDetailComponent implements OnInit {
  reservation: IReservation;
  isLoading: any;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ reservation }) => {
      this.reservation = reservation;
    });
  }

  getTotalPayed(reservation: IReservation) {
    var total = 0;
    reservation.invoices.forEach(invoice => {
      if (invoice.invoiceType == InvoiceType.PARTIAL_PAYMENT) {
        total += invoice.amount;
      } else if (invoice.invoiceType == InvoiceType.RETURN) {
        total -= invoice.amount;
      }
    });
    return total;
  }

  getTotalLeft(reservation: IReservation) {
    var total = 0;
    reservation.invoices.forEach(invoice => {
      if (invoice.invoiceType == InvoiceType.PARTIAL_PAYMENT) {
        total += invoice.amount;
      } else if (invoice.invoiceType == InvoiceType.RETURN) {
        total -= invoice.amount;
      }
    });
    return reservation.total - total;
  }

  previousState() {
    window.history.back();
  }
}
