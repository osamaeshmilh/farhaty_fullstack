import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster-material';
import { IReservation, Reservation } from 'app/shared/model/reservation.model';
import { ReservationService } from './reservation.service';
import { IAppointment } from 'app/shared/model/appointment.model';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IExtra } from 'app/shared/model/extra.model';
import { ExtraService } from 'app/entities/extra/extra.service';
import { IType } from 'app/shared/model/type.model';
import { TypeService } from 'app/entities/type/type.service';
import { IHall } from 'app/shared/model/hall.model';
import { HallService } from 'app/entities/hall/hall.service';
import { RoomService } from 'app/entities/room/room.service';
import { IRoom } from 'app/shared/model/room.model';
import { ReservationStatus } from 'app/shared/model/enumerations/reservation-status.model';

@Component({
  selector: 'jhi-reservation-new',
  templateUrl: './reservation-new.component.html'
})
export class ReservationNewComponent implements OnInit {
  isSaving: boolean;

  appointments: IAppointment[];

  users: IUser[];

  extras: IExtra[];

  types: IType[];

  halls: IHall[];

  rooms: IRoom[];

  minDate = new Date();

  editForm = this.fb.group({
    id: [],
    attendeesType: [null, [Validators.required]],
    reservationStatus: [],
    attendeesNo: [null, [Validators.required]],
    total: [],
    notes: [],
    customerName: [],
    customerPhone: [],
    appointmentId: [],
    userId: [],
    extras: [],
    typeId: [null, Validators.required],
    hallId: [],
    roomId: [null, Validators.required],
    reservationDate: [null, Validators.required],
    period: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected reservationService: ReservationService,
    protected userService: UserService,
    protected extraService: ExtraService,
    protected typeService: TypeService,
    protected activatedRoute: ActivatedRoute,
    protected hallService: HallService,
    protected roomService: RoomService,

    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ reservation }) => {
      this.updateForm(reservation);
    });
    this.hallService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IHall[]>) => mayBeOk.ok),
        map((response: HttpResponse<IHall[]>) => response.body)
      )
      .subscribe((res: IHall[]) => (this.halls = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.roomService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRoom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRoom[]>) => response.body)
      )
      .subscribe((res: IRoom[]) => (this.rooms = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.extraService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtra[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtra[]>) => response.body)
      )
      .subscribe((res: IExtra[]) => (this.extras = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.typeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IType[]>) => mayBeOk.ok),
        map((response: HttpResponse<IType[]>) => response.body)
      )
      .subscribe((res: IType[]) => (this.types = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(reservation: IReservation) {
    this.editForm.patchValue({
      id: reservation.id,
      attendeesType: reservation.attendeesType,
      reservationStatus: reservation.reservationStatus,
      attendeesNo: reservation.attendeesNo,
      total: reservation.total,
      notes: reservation.notes,
      customerName: reservation.customerName,
      customerPhone: reservation.customerPhone,
      appointmentId: reservation.appointmentId,
      userId: reservation.userId,
      extras: reservation.extras,
      typeId: reservation.typeId,
      hallId: reservation.hallId,
      roomId: reservation.roomId,
      reservationDate: reservation.reservationDate,
      period: reservation.period
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const reservation = this.createFromForm();
    reservation.reservationDate.add(1, 'days');
    if (reservation.id !== undefined) {
      this.subscribeToSaveResponse(this.reservationService.update(reservation));
    } else {
      reservation.reservationStatus = ReservationStatus.APPROVED;
      this.subscribeToSaveResponse(this.reservationService.create(reservation));
    }
  }

  private createFromForm(): IReservation {
    return {
      ...new Reservation(),
      id: this.editForm.get(['id']).value,
      attendeesType: this.editForm.get(['attendeesType']).value,
      reservationStatus: this.editForm.get(['reservationStatus']).value,
      attendeesNo: this.editForm.get(['attendeesNo']).value,
      total: this.editForm.get(['total']).value,
      notes: this.editForm.get(['notes']).value,
      customerName: this.editForm.get(['customerName']).value,
      customerPhone: this.editForm.get(['customerPhone']).value,
      appointmentId: this.editForm.get(['appointmentId']).value,
      userId: this.editForm.get(['userId']).value,
      extras: this.editForm.get(['extras']).value,
      typeId: this.editForm.get(['typeId']).value,
      hallId: this.editForm.get(['hallId']).value,
      roomId: this.editForm.get(['roomId']).value,
      reservationDate: this.editForm.get(['reservationDate']).value,
      period: this.editForm.get(['period']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReservation>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackHallById(index: number, item: IHall) {
    return item.id;
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackExtraById(index: number, item: IExtra) {
    return item.id;
  }

  trackTypeById(index: number, item: IType) {
    return item.id;
  }
  trackRoomById(index: number, item: IRoom) {
    return item.id;
  }
  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
