import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster-material';

import { IReservation } from 'app/shared/model/reservation.model';
import { AccountService } from 'app/core/auth/account.service';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ReservationService } from './reservation.service';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { InvoiceType } from 'app/shared/model/enumerations/invoice-type.model';
import { IHall } from 'app/shared/model/hall.model';
import { HallService } from 'app/entities/hall/hall.service';
import { AlertDialog } from 'app/shared/alert-dialog/alert-dialog.component';
import { ReservationStatus } from 'app/shared/model/enumerations/reservation-status.model';

@Component({
  selector: 'jhi-reservation',
  templateUrl: './reservation.component.html'
})
export class ReservationComponent implements OnInit, OnDestroy {
  currentAccount: any;
  reservations: IReservation[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate = 'reservationDate';
  previousPage: any;
  reverse: any;

  direction = 'desc';
  currentSearch: string;
  isLoading = true;
  halls: IHall[];

  selectedHallId: any;
  selectedReservationStatus: any;

  constructor(
    protected reservationService: ReservationService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    private hallService: HallService,
    protected alertDialog: AlertDialog
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.isLoading = true;
    if (this.currentSearch) {
      //search
      this.reservationService
        .query({
          'id.equals': this.currentSearch
        })
        .pipe(
          filter((res: HttpResponse<IReservation[]>) => res.ok),
          map((res: HttpResponse<IReservation[]>) => res.body)
        )

        .subscribe(
          (res: IReservation[]) => {
            this.isLoading = false;
            this.reservations = res;
          },
          (err: HttpErrorResponse) => this.onError(err.message)
        );
      return;
    }
    this.hallService.query().subscribe(
      (res: HttpResponse<IHall[]>) => {
        this.halls = res.body;
      },
      (res: HttpErrorResponse) => {
        console.log(res.message);
      }
    );

    this.reservationService
      .query({
        'hallId.equals': this.selectedHallId,
        'reservationStatus.equals': this.selectedReservationStatus,
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IReservation[]>) => this.paginateReservations(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadReservations(event, hall: IHall) {
    this.selectedHallId = hall.id;
    if (event.isUserInput) {
      this.reservations = null;
      this.reservationService
        .query({
          'hallId.equals': this.selectedHallId,
          'reservationStatus.equals': this.selectedReservationStatus
        })
        .subscribe(
          (res: HttpResponse<IReservation[]>) => {
            this.reservations = res.body;
          },
          (res: HttpErrorResponse) => {
            console.log(res.message);
          }
        );
    }
  }

  loadPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.itemsPerPage = event.pageSize;
    this.previousPage = this.page;
    this.transition();
  }

  transition() {
    this.router.navigate(['/reservation'], {
      queryParams: {
        'hallId.equals': this.selectedHallId,
        'reservationStatus.equals': this.selectedReservationStatus,
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.currentSearch = '';
    this.page = 0;
    this.router.navigate([
      '/reservation',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.currentSearch = '';
    this.page = 0;
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInReservations();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IReservation) {
    return item.id;
  }

  registerChangeInReservations() {
    this.eventSubscriber = this.eventManager.subscribe('reservationListModification', response => this.loadAll());
  }

  sort() {
    return `${this.predicate},${this.direction}`;
  }

  sortData(sort: Sort) {
    this.predicate = sort.active;
    this.direction = sort.direction;
    this.transition();
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.loadAll();
  }

  protected paginateReservations(data: IReservation[], headers: HttpHeaders) {
    this.isLoading = false;
    this.currentSearch = '';
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.reservations = data;
  }

  protected onError(errorMessage: string) {
    this.isLoading = false;
    this.jhiAlertService.error(errorMessage, null, null);
  }

  getTotalPayed(reservation: IReservation) {
    var total = 0;
    reservation.invoices.forEach(invoice => {
      if (invoice.invoiceType == InvoiceType.FULL_PAYMENT || invoice.invoiceType == InvoiceType.PARTIAL_PAYMENT) {
        total += invoice.amount;
      }
    });
    return total;
  }

  loadReservationsStatus(event, status: string) {
    this.selectedReservationStatus = status;
    if (event.isUserInput) {
      this.reservations = null;
      this.reservationService
        .query({
          'hallId.equals': this.selectedHallId,
          'reservationStatus.equals': this.selectedReservationStatus
        })
        .subscribe(
          (res: HttpResponse<IReservation[]>) => {
            this.reservations = res.body;
          },
          (res: HttpErrorResponse) => {
            console.log(res.message);
          }
        );
    }
  }

  delete(reservation: IReservation) {
    this.alertDialog.open({ message: 'سيتم حذف هذا العنصر نهائيا' }).subscribe(confimred => {
      if (confimred) {
        this.reservationService.delete(reservation.id).subscribe(res => {
          this.loadAll();
        });
      }
    });
  }

  approve(reservation: IReservation) {}

  cancel(reservation: IReservation) {
    this.alertDialog.open({ message: 'سيتم الغاء هذا الحجز ؟' }).subscribe(confimred => {
      if (confimred) {
        reservation.reservationStatus = ReservationStatus.CANCELED_BY_HALL;
        this.reservationService.update(reservation).subscribe(res => {
          this.loadAll();
        });
      }
    });
  }
}
