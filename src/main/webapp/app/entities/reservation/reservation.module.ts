import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared/shared.module';
import { ReservationComponent } from './reservation.component';
import { ReservationDetailComponent } from './reservation-detail.component';
import { ReservationUpdateComponent } from './reservation-update.component';
import { ReservationDeletePopupComponent, ReservationDeleteDialogComponent } from './reservation-delete-dialog.component';
import { reservationRoute, reservationPopupRoute } from './reservation.route';
import { NgxPrintModule } from 'ngx-print';
import { ReservationNewComponent } from 'app/entities/reservation/reservation-new.component';

const ENTITY_STATES = [...reservationRoute, ...reservationPopupRoute];

@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(ENTITY_STATES), NgxPrintModule],
  declarations: [
    ReservationComponent,
    ReservationDetailComponent,
    ReservationUpdateComponent,
    ReservationDeleteDialogComponent,
    ReservationDeletePopupComponent,
    ReservationNewComponent
  ],
  entryComponents: [ReservationDeleteDialogComponent]
})
export class FarhatyReservationModule {}
