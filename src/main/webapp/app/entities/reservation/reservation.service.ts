import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IReservation } from 'app/shared/model/reservation.model';
import { IReport } from 'app/shared/model/report.model';

type EntityResponseType = HttpResponse<IReservation>;
type EntityArrayResponseType = HttpResponse<IReservation[]>;

@Injectable({ providedIn: 'root' })
export class ReservationService {
  public resourceUrl = SERVER_API_URL + 'api/reservations';
  public resourceUrlPublic = SERVER_API_URL + 'api/public/reservations';

  constructor(protected http: HttpClient) {}

  create(reservation: IReservation): Observable<EntityResponseType> {
    return this.http.post<IReservation>(this.resourceUrl, reservation, { observe: 'response' });
  }

  update(reservation: IReservation): Observable<EntityResponseType> {
    return this.http.put<IReservation>(this.resourceUrl, reservation, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IReservation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IReservation[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  count(req?: any): Observable<HttpResponse<any>> {
    const options = createRequestOption(req);
    return this.http.get<Number>(this.resourceUrl + '/count', { params: options, observe: 'response' });
  }

  reportAdmin(from: string, to: string, all: number, req?: any): Observable<HttpResponse<IReport>> {
    const options = createRequestOption(req);
    return this.http.get<IReport>(this.resourceUrlPublic + '/report/' + from + '/' + to + '/' + all, {
      params: options,
      observe: 'response'
    });
  }

  reportHall(hallId: number, from: string, to: string, all: number, req?: any): Observable<HttpResponse<IReport>> {
    const options = createRequestOption(req);
    return this.http.get<IReport>(this.resourceUrlPublic + '/report/' + hallId + '/' + from + '/' + to + '/' + all, {
      params: options,
      observe: 'response'
    });
  }
}
