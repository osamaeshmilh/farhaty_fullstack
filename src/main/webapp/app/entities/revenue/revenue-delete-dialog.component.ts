import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRevenue } from 'app/shared/model/revenue.model';
import { RevenueService } from './revenue.service';

@Component({
  selector: 'jhi-revenue-delete-dialog',
  templateUrl: './revenue-delete-dialog.component.html'
})
export class RevenueDeleteDialogComponent {
  revenue: IRevenue;

  constructor(protected revenueService: RevenueService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.revenueService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'revenueListModification',
        content: 'Deleted an revenue'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-revenue-delete-popup',
  template: ''
})
export class RevenueDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ revenue }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(RevenueDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.revenue = revenue;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/revenue', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/revenue', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
