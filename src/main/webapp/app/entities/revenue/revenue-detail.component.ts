import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRevenue } from 'app/shared/model/revenue.model';

@Component({
  selector: 'jhi-revenue-detail',
  templateUrl: './revenue-detail.component.html'
})
export class RevenueDetailComponent implements OnInit {
  revenue: IRevenue;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ revenue }) => {
      this.revenue = revenue;
    });
  }

  previousState() {
    window.history.back();
  }
}
