import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IRevenue, Revenue } from 'app/shared/model/revenue.model';
import { RevenueService } from './revenue.service';
import { IHall } from 'app/shared/model/hall.model';
import { HallService } from 'app/entities/hall/hall.service';

@Component({
  selector: 'jhi-revenue-update',
  templateUrl: './revenue-update.component.html'
})
export class RevenueUpdateComponent implements OnInit {
  isSaving: boolean;

  halls: IHall[];

  editForm = this.fb.group({
    id: [],
    details: [],
    total: [],
    hallId: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected revenueService: RevenueService,
    protected hallService: HallService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ revenue }) => {
      this.updateForm(revenue);
    });
    this.hallService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IHall[]>) => mayBeOk.ok),
        map((response: HttpResponse<IHall[]>) => response.body)
      )
      .subscribe((res: IHall[]) => (this.halls = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(revenue: IRevenue) {
    this.editForm.patchValue({
      id: revenue.id,
      details: revenue.details,
      total: revenue.total,
      hallId: revenue.hallId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const revenue = this.createFromForm();
    if (revenue.id !== undefined) {
      this.subscribeToSaveResponse(this.revenueService.update(revenue));
    } else {
      this.subscribeToSaveResponse(this.revenueService.create(revenue));
    }
  }

  private createFromForm(): IRevenue {
    return {
      ...new Revenue(),
      id: this.editForm.get(['id']).value,
      details: this.editForm.get(['details']).value,
      total: this.editForm.get(['total']).value,
      hallId: this.editForm.get(['hallId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRevenue>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackHallById(index: number, item: IHall) {
    return item.id;
  }
}
