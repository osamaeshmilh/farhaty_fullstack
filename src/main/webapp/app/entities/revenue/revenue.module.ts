import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared/shared.module';
import { RevenueComponent } from './revenue.component';
import { RevenueDetailComponent } from './revenue-detail.component';
import { RevenueUpdateComponent } from './revenue-update.component';
import { RevenueDeletePopupComponent, RevenueDeleteDialogComponent } from './revenue-delete-dialog.component';
import { revenueRoute, revenuePopupRoute } from './revenue.route';

const ENTITY_STATES = [...revenueRoute, ...revenuePopupRoute];

@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    RevenueComponent,
    RevenueDetailComponent,
    RevenueUpdateComponent,
    RevenueDeleteDialogComponent,
    RevenueDeletePopupComponent
  ],
  entryComponents: [RevenueDeleteDialogComponent]
})
export class FarhatyRevenueModule {}
