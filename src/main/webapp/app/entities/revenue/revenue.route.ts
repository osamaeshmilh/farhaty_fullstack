import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Revenue } from 'app/shared/model/revenue.model';
import { RevenueService } from './revenue.service';
import { RevenueComponent } from './revenue.component';
import { RevenueDetailComponent } from './revenue-detail.component';
import { RevenueUpdateComponent } from './revenue-update.component';
import { RevenueDeletePopupComponent } from './revenue-delete-dialog.component';
import { IRevenue } from 'app/shared/model/revenue.model';

@Injectable({ providedIn: 'root' })
export class RevenueResolve implements Resolve<IRevenue> {
  constructor(private service: RevenueService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRevenue> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Revenue>) => response.ok),
        map((revenue: HttpResponse<Revenue>) => revenue.body)
      );
    }
    return of(new Revenue());
  }
}

export const revenueRoute: Routes = [
  {
    path: '',
    component: RevenueComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Revenues'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RevenueDetailComponent,
    resolve: {
      revenue: RevenueResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Revenues'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RevenueUpdateComponent,
    resolve: {
      revenue: RevenueResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Revenues'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RevenueUpdateComponent,
    resolve: {
      revenue: RevenueResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Revenues'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const revenuePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: RevenueDeletePopupComponent,
    resolve: {
      revenue: RevenueResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Revenues'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
