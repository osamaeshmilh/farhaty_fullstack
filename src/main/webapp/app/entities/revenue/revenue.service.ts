import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRevenue } from 'app/shared/model/revenue.model';

type EntityResponseType = HttpResponse<IRevenue>;
type EntityArrayResponseType = HttpResponse<IRevenue[]>;

@Injectable({ providedIn: 'root' })
export class RevenueService {
  public resourceUrl = SERVER_API_URL + 'api/revenues';

  constructor(protected http: HttpClient) {}

  create(revenue: IRevenue): Observable<EntityResponseType> {
    return this.http.post<IRevenue>(this.resourceUrl, revenue, { observe: 'response' });
  }

  update(revenue: IRevenue): Observable<EntityResponseType> {
    return this.http.put<IRevenue>(this.resourceUrl, revenue, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRevenue>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRevenue[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
