import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISetting } from 'app/shared/model/setting.model';
import { AccountService } from 'app/core/auth/account.service';
import { SettingService } from './setting.service';

@Component({
  selector: 'jhi-setting',
  templateUrl: './setting.component.html'
})
export class SettingComponent implements OnInit, OnDestroy {
  settings: ISetting[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected settingService: SettingService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.settingService
      .query()
      .pipe(
        filter((res: HttpResponse<ISetting[]>) => res.ok),
        map((res: HttpResponse<ISetting[]>) => res.body)
      )
      .subscribe(
        (res: ISetting[]) => {
          this.settings = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInSettings();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ISetting) {
    return item.id;
  }

  registerChangeInSettings() {
    this.eventSubscriber = this.eventManager.subscribe('settingListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
