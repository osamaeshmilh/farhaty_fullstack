import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster-material';

import { IStoreImage } from 'app/shared/model/store-image.model';
import { StoreImageService } from './store-image.service';

@Component({
  selector: 'jhi-store-image-delete-dialog',
  templateUrl: './store-image-delete-dialog.component.html'
})
export class StoreImageDeleteDialogComponent {
  storeImage: IStoreImage;

  constructor(
    protected storeImageService: StoreImageService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.storeImageService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'storeImageListModification',
        content: 'Deleted an storeImage'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-store-image-delete-popup',
  template: ''
})
export class StoreImageDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ storeImage }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(StoreImageDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.storeImage = storeImage;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/store-image', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/store-image', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
