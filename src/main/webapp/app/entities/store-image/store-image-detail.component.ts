import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster-material';

import { IStoreImage } from 'app/shared/model/store-image.model';

@Component({
  selector: 'jhi-store-image-detail',
  templateUrl: './store-image-detail.component.html'
})
export class StoreImageDetailComponent implements OnInit {
  storeImage: IStoreImage;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ storeImage }) => {
      this.storeImage = storeImage;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
