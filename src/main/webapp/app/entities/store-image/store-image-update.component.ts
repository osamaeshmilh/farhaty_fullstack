import { Component, OnInit, ElementRef } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster-material';
import { IStoreImage, StoreImage } from 'app/shared/model/store-image.model';
import { StoreImageService } from './store-image.service';
import { IStore } from 'app/shared/model/store.model';
import { StoreService } from 'app/entities/store/store.service';

@Component({
  selector: 'jhi-store-image-update',
  templateUrl: './store-image-update.component.html'
})
export class StoreImageUpdateComponent implements OnInit {
  isSaving: boolean;

  stores: IStore[];

  editForm = this.fb.group({
    id: [],
    image: [],
    imageContentType: [],
    storeId: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected storeImageService: StoreImageService,
    protected storeService: StoreService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ storeImage }) => {
      this.updateForm(storeImage);
    });
    this.storeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IStore[]>) => mayBeOk.ok),
        map((response: HttpResponse<IStore[]>) => response.body)
      )
      .subscribe((res: IStore[]) => (this.stores = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(storeImage: IStoreImage) {
    this.editForm.patchValue({
      id: storeImage.id,
      image: storeImage.image,
      imageContentType: storeImage.imageContentType,
      storeId: storeImage.storeId
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const storeImage = this.createFromForm();
    if (storeImage.id !== undefined) {
      this.subscribeToSaveResponse(this.storeImageService.update(storeImage));
    } else {
      this.subscribeToSaveResponse(this.storeImageService.create(storeImage));
    }
  }

  private createFromForm(): IStoreImage {
    return {
      ...new StoreImage(),
      id: this.editForm.get(['id']).value,
      imageContentType: this.editForm.get(['imageContentType']).value,
      image: this.editForm.get(['image']).value,
      storeId: this.editForm.get(['storeId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStoreImage>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackStoreById(index: number, item: IStore) {
    return item.id;
  }
}
