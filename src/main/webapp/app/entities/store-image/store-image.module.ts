import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared/shared.module';
import { StoreImageComponent } from './store-image.component';
import { StoreImageDetailComponent } from './store-image-detail.component';
import { StoreImageUpdateComponent } from './store-image-update.component';
import { StoreImageDeletePopupComponent, StoreImageDeleteDialogComponent } from './store-image-delete-dialog.component';
import { storeImageRoute, storeImagePopupRoute } from './store-image.route';

const ENTITY_STATES = [...storeImageRoute, ...storeImagePopupRoute];

@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    StoreImageComponent,
    StoreImageDetailComponent,
    StoreImageUpdateComponent,
    StoreImageDeleteDialogComponent,
    StoreImageDeletePopupComponent
  ],
  entryComponents: [StoreImageDeleteDialogComponent]
})
export class FarhatyStoreImageModule {}
