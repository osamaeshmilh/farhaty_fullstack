import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster-material';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StoreImage } from 'app/shared/model/store-image.model';
import { StoreImageService } from './store-image.service';
import { StoreImageComponent } from './store-image.component';
import { StoreImageDetailComponent } from './store-image-detail.component';
import { StoreImageUpdateComponent } from './store-image-update.component';
import { StoreImageDeletePopupComponent } from './store-image-delete-dialog.component';
import { IStoreImage } from 'app/shared/model/store-image.model';

@Injectable({ providedIn: 'root' })
export class StoreImageResolve implements Resolve<IStoreImage> {
  constructor(private service: StoreImageService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IStoreImage> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<StoreImage>) => response.ok),
        map((storeImage: HttpResponse<StoreImage>) => storeImage.body)
      );
    }
    return of(new StoreImage());
  }
}

export const storeImageRoute: Routes = [
  {
    path: '',
    component: StoreImageComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'StoreImages'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: StoreImageDetailComponent,
    resolve: {
      storeImage: StoreImageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'StoreImages'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: StoreImageUpdateComponent,
    resolve: {
      storeImage: StoreImageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'StoreImages'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: StoreImageUpdateComponent,
    resolve: {
      storeImage: StoreImageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'StoreImages'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const storeImagePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: StoreImageDeletePopupComponent,
    resolve: {
      storeImage: StoreImageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'StoreImages'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
