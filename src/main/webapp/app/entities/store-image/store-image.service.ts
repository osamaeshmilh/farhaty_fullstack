import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IStoreImage } from 'app/shared/model/store-image.model';

type EntityResponseType = HttpResponse<IStoreImage>;
type EntityArrayResponseType = HttpResponse<IStoreImage[]>;

@Injectable({ providedIn: 'root' })
export class StoreImageService {
  public resourceUrl = SERVER_API_URL + 'api/store-images';

  constructor(protected http: HttpClient) {}

  create(storeImage: IStoreImage): Observable<EntityResponseType> {
    return this.http.post<IStoreImage>(this.resourceUrl, storeImage, { observe: 'response' });
  }

  update(storeImage: IStoreImage): Observable<EntityResponseType> {
    return this.http.put<IStoreImage>(this.resourceUrl, storeImage, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IStoreImage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IStoreImage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
