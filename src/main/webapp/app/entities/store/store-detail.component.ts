import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster-material';

import { IStore } from 'app/shared/model/store.model';
import { IStoreImage } from 'app/shared/model/store-image.model';
import { StoreImageService } from 'app/entities/store-image/store-image.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';
import { StoreImageDialogComponent } from 'app/entities/store/store-image-dialog/store-image-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { IItem } from 'app/shared/model/item.model';
import { ItemService } from 'app/entities/item/item.service';
import { IChat } from 'app/shared/model/chat.model';
import { ChatService } from 'app/entities/chat/chat.service';

@Component({
  selector: 'jhi-store-detail',
  templateUrl: './store-detail.component.html'
})
export class StoreDetailComponent implements OnInit {
  store: IStore;
  storeImages: IStoreImage[];
  items: IItem[];
  chats: IChat[];

  constructor(
    protected dataUtils: JhiDataUtils,
    protected activatedRoute: ActivatedRoute,
    protected storeImageService: StoreImageService,
    protected itemService: ItemService,
    protected chatService: ChatService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ store }) => {
      this.store = store;
    });

    this.storeImageService
      .query({
        'storeId.equals': this.store.id
      })
      .subscribe(
        (res: HttpResponse<IStoreImage[]>) => {
          this.storeImages = res.body;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    this.itemService
      .query({
        'storeId.equals': this.store.id
      })
      .subscribe(
        (res: HttpResponse<IItem[]>) => {
          this.items = res.body;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    this.chatService
      .query({
        'storeId.equals': this.store.id
      })
      .subscribe(
        (res: HttpResponse<IChat[]>) => {
          this.chats = res.body;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }

  private onError(message: string) {}

  openImagesDialog(): void {
    const dialogRef = this.dialog.open(StoreImageDialogComponent, {
      width: '650px',
      data: this.store.id
    });

    dialogRef.afterClosed().subscribe(attribute => {
      this.storeImageService
        .query({
          'storeId.equals': this.store.id
        })
        .pipe(
          filter((mayBeOk: HttpResponse<IStoreImage[]>) => mayBeOk.ok),
          map((response: HttpResponse<IStoreImage[]>) => response.body)
        )
        .subscribe((res: IStoreImage[]) => (this.storeImages = res), (res: HttpErrorResponse) => this.onError(res.message));
    });
  }
}
