import { Component, ElementRef, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StoreImageService } from 'app/entities/store-image/store-image.service';
import { IStoreImage, StoreImage } from 'app/shared/model/store-image.model';

@Component({
  selector: 'jhi-store-image-dialog',
  templateUrl: './store-image-dialog.component.html'
})
export class StoreImageDialogComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    storeId: [],
    image: [],
    imageContentType: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected storeImageService: StoreImageService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<StoreImageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ image }) => {
      this.updateForm(image);
    });
  }

  updateForm(image: IStoreImage) {
    this.editForm.patchValue({
      id: image.id,
      storeId: image.storeId,
      image: image.image,
      imageContentType: image.imageContentType
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file = event.target.files[0];

        const filedContentType: string = field + 'ContentType';
        this.dataUtils.toBase64(file, base64Data => {
          this.editForm.patchValue({
            [field]: base64Data,
            [filedContentType]: file.type
          });
        });
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      () => {}, // sucess
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    this.dialogRef.close();
  }

  save() {
    this.isSaving = true;
    const image = this.createFromForm();
    this.subscribeToSaveResponse(this.storeImageService.create(image));
  }

  private createFromForm(): IStoreImage {
    return {
      ...new StoreImage(),
      id: this.editForm.get(['id']).value,
      storeId: this.data,
      image: this.editForm.get(['image']).value,
      imageContentType: this.editForm.get(['imageContentType']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStoreImage>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
