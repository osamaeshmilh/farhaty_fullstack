import { Component, OnInit, ElementRef } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster-material';
import { IStore, Store } from 'app/shared/model/store.model';
import { StoreService } from './store.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';
import { IMarker } from 'app/shared/model/marker.model';

@Component({
  selector: 'jhi-store-update',
  templateUrl: './store-update.component.html'
})
export class StoreUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  categories: ICategory[];

  lat = 32.87849;
  lng = 13.187364;

  markers: IMarker[] = [
    {
      lat: null,
      lng: null,
      label: '',
      draggable: true
    }
  ];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    description: [],
    address: [null, [Validators.required]],
    lat: [],
    lng: [],
    logo: [],
    logoUrl: [],
    logoContentType: [],
    email: [null, [Validators.required, Validators.email]],
    phone: [null, [Validators.required]],
    facebook: [],
    whatsup: [],
    viper: [],
    twitter: [],
    instagram: [],
    snapchat: [],
    vip: [],
    userId: [],
    categoryId: [null, Validators.required]
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected storeService: StoreService,
    protected userService: UserService,
    protected categoryService: CategoryService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ store }) => {
      this.markers = [
        {
          lat: store.lat,
          lng: store.lng,
          label: store.name,
          draggable: false
        }
      ];
      this.updateForm(store);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.categoryService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICategory[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICategory[]>) => response.body)
      )
      .subscribe((res: ICategory[]) => (this.categories = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(store: IStore) {
    this.editForm.patchValue({
      id: store.id,
      name: store.name,
      description: store.description,
      address: store.address,
      lat: store.lat,
      lng: store.lng,
      logo: store.logo,
      logoUrl: store.logoUrl,
      logoContentType: store.logoContentType,
      email: store.email,
      phone: store.phone,
      facebook: store.facebook,
      whatsup: store.whatsup,
      viper: store.viper,
      twitter: store.twitter,
      instagram: store.instagram,
      snapchat: store.snapchat,
      vip: store.vip,
      userId: store.userId,
      categoryId: store.categoryId
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const store = this.createFromForm();
    store.lat = this.markers[0].lat;
    store.lng = this.markers[0].lng;
    if (store.id !== undefined) {
      this.subscribeToSaveResponse(this.storeService.update(store));
    } else {
      this.subscribeToSaveResponse(this.storeService.create(store));
    }
  }

  private createFromForm(): IStore {
    return {
      ...new Store(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      description: this.editForm.get(['description']).value,
      address: this.editForm.get(['address']).value,
      lat: this.editForm.get(['lat']).value,
      lng: this.editForm.get(['lng']).value,
      logoContentType: this.editForm.get(['logoContentType']).value,
      logo: this.editForm.get(['logo']).value,
      logoUrl: this.editForm.get(['logoUrl']).value,
      email: this.editForm.get(['email']).value,
      phone: this.editForm.get(['phone']).value,
      facebook: this.editForm.get(['facebook']).value,
      whatsup: this.editForm.get(['whatsup']).value,
      viper: this.editForm.get(['viper']).value,
      twitter: this.editForm.get(['twitter']).value,
      instagram: this.editForm.get(['instagram']).value,
      snapchat: this.editForm.get(['snapchat']).value,
      vip: this.editForm.get(['vip']).value,
      userId: this.editForm.get(['userId']).value,
      categoryId: this.editForm.get(['categoryId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStore>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackCategoryById(index: number, item: ICategory) {
    return item.id;
  }

  mapClicked($event: any) {
    console.log($event.coords.lat, $event.coords.lng);
    this.markers[0].lat = $event.coords.lat;
    this.markers[0].lng = $event.coords.lng;
  }

  markerDragEnd(m: IMarker, $event: any) {
    console.log('dragEnd', $event);
    this.markers[0].lat = $event.coords.lat;
    this.markers[0].lng = $event.coords.lng;
  }
}
