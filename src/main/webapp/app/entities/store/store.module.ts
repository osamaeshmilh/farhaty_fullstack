import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared/shared.module';
import { StoreComponent } from './store.component';
import { StoreDetailComponent } from './store-detail.component';
import { StoreUpdateComponent } from './store-update.component';
import { StoreDeletePopupComponent, StoreDeleteDialogComponent } from './store-delete-dialog.component';
import { storeRoute, storePopupRoute } from './store.route';
import { StoreImageDialogComponent } from 'app/entities/store/store-image-dialog/store-image-dialog.component';
import { AgmCoreModule } from '@agm/core';

const ENTITY_STATES = [...storeRoute, ...storePopupRoute];

@NgModule({
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAncdi41FFxPV6NUgpgZ8U_0CMgVgBlNHU'
    }),
    FarhatySharedModule,
    RouterModule.forChild(ENTITY_STATES)
  ],
  declarations: [
    StoreComponent,
    StoreDetailComponent,
    StoreUpdateComponent,
    StoreDeleteDialogComponent,
    StoreDeletePopupComponent,
    StoreImageDialogComponent
  ],
  entryComponents: [StoreDeleteDialogComponent, StoreImageDialogComponent]
})
export class FarhatyStoreModule {}
