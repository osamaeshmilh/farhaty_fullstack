import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster-material';
import { IType, Type } from 'app/shared/model/type.model';
import { TypeService } from './type.service';
import { IHall } from 'app/shared/model/hall.model';
import { HallService } from 'app/entities/hall/hall.service';

@Component({
  selector: 'jhi-type-update',
  templateUrl: './type-update.component.html'
})
export class TypeUpdateComponent implements OnInit {
  isSaving: boolean;

  halls: IHall[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    satPrice: [],
    sunPrice: [],
    monPrice: [],
    tuePrice: [],
    wedPrice: [],
    thrPrice: [],
    friPrice: [],
    active: [null, [Validators.required]],
    hallId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected typeService: TypeService,
    protected hallService: HallService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ type }) => {
      this.updateForm(type);
    });
    this.hallService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IHall[]>) => mayBeOk.ok),
        map((response: HttpResponse<IHall[]>) => response.body)
      )
      .subscribe((res: IHall[]) => (this.halls = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(type: IType) {
    this.editForm.patchValue({
      id: type.id,
      name: type.name,
      satPrice: type.satPrice,
      sunPrice: type.sunPrice,
      monPrice: type.monPrice,
      tuePrice: type.tuePrice,
      wedPrice: type.wedPrice,
      thrPrice: type.thrPrice,
      friPrice: type.friPrice,
      active: type.active,
      hallId: type.hallId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const type = this.createFromForm();
    if (type.id !== undefined) {
      this.subscribeToSaveResponse(this.typeService.update(type));
    } else {
      this.subscribeToSaveResponse(this.typeService.create(type));
    }
  }

  private createFromForm(): IType {
    return {
      ...new Type(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      satPrice: this.editForm.get(['satPrice']).value,
      sunPrice: this.editForm.get(['sunPrice']).value,
      monPrice: this.editForm.get(['monPrice']).value,
      tuePrice: this.editForm.get(['tuePrice']).value,
      wedPrice: this.editForm.get(['wedPrice']).value,
      thrPrice: this.editForm.get(['thrPrice']).value,
      friPrice: this.editForm.get(['friPrice']).value,
      active: this.editForm.get(['active']).value,
      hallId: this.editForm.get(['hallId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IType>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackHallById(index: number, item: IHall) {
    return item.id;
  }
}
