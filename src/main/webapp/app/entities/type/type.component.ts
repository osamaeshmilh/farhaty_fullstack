import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster-material';

import { IType } from 'app/shared/model/type.model';
import { AccountService } from 'app/core/auth/account.service';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TypeService } from './type.service';
import { Sort } from '@angular/material/sort';
import { PageEvent } from '@angular/material/paginator';
import { AlertDialog } from 'app/shared/alert-dialog/alert-dialog.component';

@Component({
  selector: 'jhi-type',
  templateUrl: './type.component.html'
})
export class TypeComponent implements OnInit, OnDestroy {
  currentAccount: any;
  types: IType[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;

  direction = 'asc';
  currentSearch: string;
  isLoading = true;

  constructor(
    protected typeService: TypeService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    private alertDialog: AlertDialog
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.isLoading = true;
    if (this.currentSearch) {
      //search
      this.typeService
        .query({
          'name.contains': this.currentSearch
        })
        .pipe(
          filter((res: HttpResponse<IType[]>) => res.ok),
          map((res: HttpResponse<IType[]>) => res.body)
        )

        .subscribe(
          (res: IType[]) => {
            this.isLoading = false;
            this.types = res;
          },
          (err: HttpErrorResponse) => this.onError(err.message)
        );
      return;
    }
    this.typeService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IType[]>) => this.paginateTypes(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.itemsPerPage = event.pageSize;
    this.previousPage = this.page;
    this.transition();
  }

  transition() {
    this.router.navigate(['/type'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.currentSearch = '';
    this.page = 0;
    this.router.navigate([
      '/type',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.currentSearch = '';
    this.page = 0;
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInTypes();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IType) {
    return item.id;
  }

  registerChangeInTypes() {
    this.eventSubscriber = this.eventManager.subscribe('typeListModification', response => this.loadAll());
  }

  sort() {
    return `${this.predicate},${this.direction}`;
  }

  sortData(sort: Sort) {
    this.predicate = sort.active;
    this.direction = sort.direction;
    this.transition();
  }

  protected paginateTypes(data: IType[], headers: HttpHeaders) {
    this.isLoading = false;
    this.currentSearch = '';
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.types = data;
  }

  protected onError(errorMessage: string) {
    this.isLoading = false;
    this.jhiAlertService.error(errorMessage, null, null);
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.loadAll();
  }

  delete(id: number) {
    this.alertDialog.open({ message: 'سيتم حذف هذا العنصر نهائيا' }).subscribe(confimred => {
      if (confimred) {
        this.typeService.delete(id).subscribe(res => {
          this.loadAll();
        });
      }
    });
  }
}
