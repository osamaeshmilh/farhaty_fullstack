import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FarhatySharedModule } from 'app/shared/shared.module';
import { TypeComponent } from './type.component';
import { TypeDetailComponent } from './type-detail.component';
import { TypeUpdateComponent } from './type-update.component';
import { TypeDeletePopupComponent, TypeDeleteDialogComponent } from './type-delete-dialog.component';
import { typeRoute, typePopupRoute } from './type.route';

const ENTITY_STATES = [...typeRoute, ...typePopupRoute];

@NgModule({
  imports: [FarhatySharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [TypeComponent, TypeDetailComponent, TypeUpdateComponent, TypeDeleteDialogComponent, TypeDeletePopupComponent],
  entryComponents: [TypeDeleteDialogComponent]
})
export class FarhatyTypeModule {}
