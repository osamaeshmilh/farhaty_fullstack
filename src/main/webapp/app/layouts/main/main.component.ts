import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRouteSnapshot, NavigationError } from '@angular/router';

import { AccountService, LoginService } from 'app/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'jhi-main',
  templateUrl: './main.component.html',
  styleUrls: ['main.scss']
})
export class JhiMainComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSidenav, { static: true })
  sidenav!: MatSidenav;

  inProduction: boolean;
  isNavbarCollapsed: boolean;
  languages: any[];
  swaggerEnabled: boolean;
  version: string;
  currentSearch: String;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private accountService: AccountService,
    private observer: BreakpointObserver
  ) {}

  ngAfterViewInit() {
    this.observer.observe(['(max-width: 800px)']).subscribe(res => {
      if (res.matches) {
        this.sidenav.mode = 'over';
        this.sidenav.close();
      } else {
        this.sidenav.mode = 'side';
        this.sidenav.open();
      }
    });
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationError && event.error.status === 404) {
        this.router.navigate(['/404']);
      }
    });
  }

  private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
    let title: string = routeSnapshot.data && routeSnapshot.data['pageTitle'] ? routeSnapshot.data['pageTitle'] : 'Bridal Ly';
    if (routeSnapshot.firstChild) {
      title = this.getPageTitle(routeSnapshot.firstChild) || title;
    }
    return title;
  }

  isAuthenticated() {
    return this.accountService.isAuthenticated();
  }

  clear() {
    this.currentSearch = undefined;
    this.search();
  }

  search() {
    this.router.navigate(['/posts'], { queryParams: { search: this.currentSearch } });
  }

  getLoginName(): string {
    return this.accountService.getLoginName();
  }

  login() {
    this.router.navigate(['/login']);
  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['/']);
  }

  getImageUrl() {
    return this.isAuthenticated() ? this.accountService.getImageUrl() : null;
  }
}
