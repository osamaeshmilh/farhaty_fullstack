import { Component, Inject, OnInit, Injectable } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Observable } from 'rxjs';

@Component({
  selector: 'jhi-alert-dialog',
  templateUrl: './alert-dialog.component.html'
})

// tslint:disable-next-line:no-unused-expression
export class AlertDialogComponent {
  constructor(public dialogRef: MatDialogRef<AlertDialogComponent>, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data) {
    this.data.message = this.data.message || '';
    this.data.mode = this.data.mode || 'confirm';
    this.data.cancelLabel = this.data.cancelLabel || 'إلغاء';
    console.log(this.data);
  }

  clear() {
    this.dialogRef.close(false);
  }

  confirm() {
    this.dialogRef.close(true);
  }
}

@Injectable({ providedIn: 'root' })
export class AlertDialog {
  constructor(private dialog: MatDialog) {}

  open(data: any): Observable<boolean> {
    return this.dialog.open(AlertDialogComponent, { width: '400px', data }).afterClosed();
  }
}
