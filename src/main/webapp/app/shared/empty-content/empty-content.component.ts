import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'jhi-empty-content',
  templateUrl: './empty-content.component.html'
})
export class EmptyContentComponent implements OnInit {
  @Input() isLoading = false;
  constructor() {}

  ngOnInit() {}
}
