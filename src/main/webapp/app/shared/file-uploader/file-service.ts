import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';

@Injectable({ providedIn: 'root' })
export class FileService {
  public uploadUrl = SERVER_API_URL + 'api/file/upload';

  constructor(protected http: HttpClient) {}

  upload(image: any): Observable<any> {
    return this.http.post<any>(this.uploadUrl, image);
  }
}
