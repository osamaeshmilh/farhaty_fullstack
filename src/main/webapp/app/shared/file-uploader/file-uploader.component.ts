import { Component, Input, Output, EventEmitter } from '@angular/core';
import { JhiDataUtils } from 'ng-jhipster-material';
import { DomSanitizer } from '@angular/platform-browser';
import { FileService } from './file-service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'jhi-file-uploader',
  templateUrl: './file-uploader.component.html'
})
export class FileUploaderComponent {
  base64File: any;
  exceedsLimit = false;
  file: File;
  @Input() label = 'إرفاق ملف';
  @Input() accept = '.jpg,.png';
  @Output() onFilePicked: EventEmitter<any> = new EventEmitter();
  @Output() onDelete: EventEmitter<any> = new EventEmitter();
  @Input() previewUrl;
  isLoading = false;
  constructor(private dataUtils: JhiDataUtils, private sanitizer: DomSanitizer) {}

  setFileData(event) {
    this.exceedsLimit = false;
    this.file = event.target.files[0];
    this.previewUrl = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(this.file));
    console.log(this.previewUrl);
    if (this.file.size / 1000 > 1500) {
      this.exceedsLimit = true;
      this.file = null;
      return;
    }
    this.dataUtils.toBase64(this.file, base64Data => this.onFilePicked.emit(base64Data));
  }
}

// deleteFile() {
//     if (this.attachment.id) {
//         this.alertDialog.open({ message: 'هل تريد حذف  هذا المستند؟' }).subscribe(confirmed => {
//             if (confirmed) {
//                 this.onDelete.emit(this);
//             }
//         });
//     } else {
//         this.attachment.binary = undefined;
//         this.attachment.id = undefined;
//     }
// }
