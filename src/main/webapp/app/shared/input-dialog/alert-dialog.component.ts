import { Component, Inject, OnInit, Injectable } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Observable } from 'rxjs';

@Component({
  selector: 'jhi-input-dialog',
  templateUrl: './input-dialog.component.html'
})

// tslint:disable-next-line:no-unused-expression
export class InputDialogComponent {
  input = '';
  constructor(public dialogRef: MatDialogRef<InputDialogComponent>, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data) {
    this.data.title = this.data.title || 'إضافة عنصر جديد';
    this.input = this.data.value || '';
  }

  clear() {
    this.dialogRef.close(null);
  }

  confirm() {
    this.dialogRef.close(this.input);
  }
}

@Injectable({ providedIn: 'root' })
export class InputDialog {
  constructor(private dialog: MatDialog) {}

  open(data: any): Observable<string> {
    return this.dialog.open(InputDialogComponent, { width: '400px', data }).afterClosed();
  }
}
