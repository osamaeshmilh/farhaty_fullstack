import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MAT_DIALOG_DATA,
  MatAutocompleteModule,
  MatBadgeModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDateFormats,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatIconRegistry,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorIntl,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSidenavModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
  MatSlideToggleModule,
  MatRadioModule
} from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { CMatPaginatorIntl } from './cmat.paginator';

export const MOMENT_DATE_FORMATS: MatDateFormats = {
  parse: {
    dateInput: 'YYYY-MM-DD'
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMMM Y',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM Y'
  }
};

@NgModule({
  imports: [
    CommonModule,
    MatSidenavModule,
    MatListModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatMenuModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatButtonModule,
    MatTreeModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatDividerModule,
    MatTableModule,
    MatDialogModule,
    MatSortModule,
    MatPaginatorModule,
    MatMenuModule,
    MatSelectModule,
    MatProgressBarModule,
    MatExpansionModule,
    MatTabsModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatBadgeModule,
    OverlayModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,
    MatChipsModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatRadioModule
  ],
  exports: [
    CommonModule,
    MatSidenavModule,
    MatListModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatMenuModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatButtonModule,
    MatTreeModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatDividerModule,
    MatTableModule,
    MatDialogModule,
    MatSortModule,
    MatPaginatorModule,
    MatMenuModule,
    MatSelectModule,
    MatProgressBarModule,
    MatExpansionModule,
    MatTabsModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatBadgeModule,
    OverlayModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,
    MatChipsModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatRadioModule
  ],
  declarations: [],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatPaginatorIntl, useClass: CMatPaginatorIntl },
    { provide: MAT_DATE_LOCALE, useValue: 'ar-ly' },
    { provide: MAT_DATE_FORMATS, useValue: MOMENT_DATE_FORMATS }
  ]
})
export class MatModule {
  constructor(private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer) {
    this.iconRegistry.addSvgIcon('google', sanitizer.bypassSecurityTrustResourceUrl('content/images/google.svg'));
    this.iconRegistry.addSvgIcon('facebook', sanitizer.bypassSecurityTrustResourceUrl('content/images/facebook.svg'));

    this.iconRegistry.addSvgIcon('facebook_big', sanitizer.bypassSecurityTrustResourceUrl('content/images/facebook-big.svg'));

    this.iconRegistry.addSvgIcon('twitter', sanitizer.bypassSecurityTrustResourceUrl('content/images/twitter.svg'));
    this.iconRegistry.addSvgIcon('linkedin', sanitizer.bypassSecurityTrustResourceUrl('content/images/linkedin.svg'));
    this.iconRegistry.addSvgIcon('tap', sanitizer.bypassSecurityTrustResourceUrl('content/images/tap.svg'));
    this.iconRegistry.addSvgIcon('error-404', sanitizer.bypassSecurityTrustResourceUrl('content/images/error-404.svg'));

    this.iconRegistry.addSvgIcon('no-results', sanitizer.bypassSecurityTrustResourceUrl('content/images/no-results.svg'));

    this.iconRegistry.addSvgIcon('cancel', sanitizer.bypassSecurityTrustResourceUrl('content/images/cancel.svg'));
    this.iconRegistry.addSvgIcon('clock', sanitizer.bypassSecurityTrustResourceUrl('content/images/clock.svg'));
    this.iconRegistry.addSvgIcon('done', sanitizer.bypassSecurityTrustResourceUrl('content/images/done.svg'));
  }
}
