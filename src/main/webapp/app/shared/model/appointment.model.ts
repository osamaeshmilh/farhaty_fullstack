import { Moment } from 'moment';
import { Period } from 'app/shared/model/enumerations/period.model';

export interface IAppointment {
  id?: number;
  appointmentDate?: Moment;
  period?: Period;
  price?: number;
  notes?: string;
  reservationId?: number;
  hallName?: string;
  hallId?: number;
  roomName?: string;
  roomId?: number;
}

export class Appointment implements IAppointment {
  constructor(
    public id?: number,
    public appointmentDate?: Moment,
    public period?: Period,
    public price?: number,
    public notes?: string,
    public reservationId?: number,
    public hallName?: string,
    public hallId?: number,
    public roomName?: string,
    public roomId?: number
  ) {}
}
