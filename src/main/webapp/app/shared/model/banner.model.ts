export interface IBanner {
  id?: number;
  title?: string;
  imageUrl?: string;
  imageContentType?: string;
  image?: any;
  active?: boolean;
  storeName?: string;
  storeId?: number;
  hallId?: number;
}

export class Banner implements IBanner {
  constructor(
    public id?: number,
    public title?: string,
    public imageUrl?: string,
    public imageContentType?: string,
    public image?: any,
    public active?: boolean,
    public storeName?: string,
    public storeId?: number,
    public hallId?: number
  ) {
    this.active = this.active || false;
  }
}
