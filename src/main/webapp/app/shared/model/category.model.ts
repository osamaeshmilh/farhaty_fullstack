import { IStore } from 'app/shared/model/store.model';

export interface ICategory {
  id?: number;
  name?: string;
  imageUrl?: string;
  imageContentType?: string;
  image?: any;
  active?: boolean;
  stores?: IStore[];
}

export class Category implements ICategory {
  constructor(
    public id?: number,
    public name?: string,
    public imageUrl?: string,
    public imageContentType?: string,
    public image?: any,
    public active?: boolean,
    public stores?: IStore[]
  ) {
    this.active = this.active || false;
  }
}
