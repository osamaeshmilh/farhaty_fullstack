export interface IChat {
  id?: number;
  details?: string;
  storeName?: string;
  storeId?: number;
  userLogin?: string;
  userId?: number;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export class Chat implements IChat {
  constructor(
    public id?: number,
    public details?: string,
    public storeName?: string,
    public storeId?: number,
    public userLogin?: string,
    public userId?: number,
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date
  ) {}
}
