export const enum InvoiceType {
  FULL_PAYMENT = 'FULL_PAYMENT',
  PARTIAL_PAYMENT = 'PARTIAL_PAYMENT',
  RETURN = 'RETURN',
  GUARANTEE = 'GUARANTEE'
}
