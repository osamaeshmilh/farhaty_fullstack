export const enum ReservationStatus {
  PENDING = 'PENDING',
  APPROVED = 'APPROVED',
  DONE = 'DONE',
  CANCELED_BY_USER = 'CANCELED_BY_USER',
  CANCELED_BY_HALL = 'CANCELED_BY_HALL'
}
