export interface IExpense {
  id?: number;
  details?: string;
  total?: number;
  hallName?: string;
  hallId?: number;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export class Expense implements IExpense {
  constructor(
    public id?: number,
    public details?: string,
    public total?: number,
    public hallName?: string,
    public hallId?: number,
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date
  ) {}
}
