import { IReservation } from 'app/shared/model/reservation.model';

export interface IExtra {
  id?: number;
  name?: string;
  price?: number;
  active?: boolean;
  hallName?: string;
  hallId?: number;
  extras?: IReservation[];
}

export class Extra implements IExtra {
  constructor(
    public id?: number,
    public name?: string,
    public price?: number,
    public active?: boolean,
    public hallName?: string,
    public hallId?: number,
    public extras?: IReservation[]
  ) {
    this.active = this.active || false;
  }
}
