export interface IFavoriteItem {
  id?: number;
  itemName?: string;
  itemId?: number;
  userId?: number;
}

export class FavoriteItem implements IFavoriteItem {
  constructor(public id?: number, public itemName?: string, public itemId?: number, public userId?: number) {}
}
