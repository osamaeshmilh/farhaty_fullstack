export interface IHallImage {
  id?: number;
  title?: string;
  imageContentType?: string;
  image?: any;
  hallId?: number;
  roomId?: number;
}

export class HallImage implements IHallImage {
  constructor(
    public id?: number,
    public title?: string,
    public imageContentType?: string,
    public image?: any,
    public hallId?: number,
    public roomId?: number
  ) {}
}
