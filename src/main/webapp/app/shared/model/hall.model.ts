import { IAppointment } from 'app/shared/model/appointment.model';
import { IType } from 'app/shared/model/type.model';
import { IRoom } from 'app/shared/model/room.model';
import { IExtra } from 'app/shared/model/extra.model';
import { Moment } from 'moment';

export interface IHall {
  id?: number;
  name?: string;
  description?: string;
  address?: string;
  phone?: string;
  email?: string;
  facebook?: string;
  whatsup?: string;
  viper?: string;
  twitter?: string;
  instagram?: string;
  snapchat?: string;
  lat?: number;
  lng?: number;
  rules?: any;
  vip?: boolean;
  active?: boolean;
  userId?: number;
  appointments?: IAppointment[];
  types?: IType[];
  rooms?: IRoom[];
  extras?: IExtra[];
  logoContentType?: string;
  logo?: any;
  logoUrl?: string;
  availableFrom?: Moment;
  availableTo?: Moment;
  pendingAppointmentPeriod?: number;
  viewCount?: number;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
  reservationsTotal?: number;
  reservationCount?: number;
}

export class Hall implements IHall {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public address?: string,
    public phone?: string,
    public email?: string,
    public facebook?: string,
    public whatsup?: string,
    public viper?: string,
    public twitter?: string,
    public instagram?: string,
    public snapchat?: string,
    public lat?: number,
    public lng?: number,
    public rules?: any,
    public vip?: boolean,
    public active?: boolean,
    public userId?: number,
    public appointments?: IAppointment[],
    public types?: IType[],
    public rooms?: IRoom[],
    public extras?: IExtra[],
    public logoContentType?: string,
    public logo?: any,
    public logoUrl?: string,
    public availableFrom?: Moment,
    public availableTo?: Moment,
    public pendingAppointmentPeriod?: number,
    public viewCount?: number,
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date,
    public reservationsTotal?: number,
    public reservationCount?: number
  ) {
    this.vip = this.vip || false;
  }
}
