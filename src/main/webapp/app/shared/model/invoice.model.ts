import { Moment } from 'moment';
import { InvoiceType } from 'app/shared/model/enumerations/invoice-type.model';
import { IReservation } from 'app/shared/model/reservation.model';

export interface IInvoice {
  hallId?: number;
  hallName?: string;
  id?: number;
  invoiceDate?: Moment;
  amount?: number;
  details?: string;
  invoiceType?: InvoiceType;
  status?: number;
  reservationId?: number;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
  reservationTotal?: number;
  extrasTotal?: number;
  reservationCustomerName?: string;
  reservationCustomerPhone?: string;
  reservationReservationDate?: Date;
  reservationUserFirstName?: string;
  reservationUserPhone?: string;
}

export class Invoice implements IInvoice {
  constructor(
    public hallId?: number,
    public hallName?: string,
    public id?: number,
    public invoiceDate?: Moment,
    public amount?: number,
    public details?: string,
    public invoiceType?: InvoiceType,
    public status?: number,
    public reservationId?: number,
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date,
    public reservationTotal?: number,
    public extrasTotal?: number,
    public reservationCustomerName?: string,
    public reservationCustomerPhone?: string,
    public reservationReservationDate?: Date,
    public reservationUserFirstName?: string,
    public reservationUserPhone?: string
  ) {}
}
