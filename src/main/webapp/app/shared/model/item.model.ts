import { Currency } from 'app/shared/model/enumerations/currency.model';

export interface IItem {
  id?: number;
  name?: string;
  description?: string;
  imageUrl?: string;
  imageContentType?: string;
  image?: any;
  price?: number;
  likeCount?: number;
  currency?: Currency;
  active?: boolean;
  storeName?: string;
  storeId?: number;
  favoriteCount?: number;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export class Item implements IItem {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public imageUrl?: string,
    public imageContentType?: string,
    public image?: any,
    public price?: number,
    public likeCount?: number,
    public currency?: Currency,
    public active?: boolean,
    public storeName?: string,
    public storeId?: number,
    public favoriteCount?: number,
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date
  ) {
    this.active = this.active || false;
  }
}
