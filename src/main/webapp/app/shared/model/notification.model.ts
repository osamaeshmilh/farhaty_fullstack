export interface INotification {
  id?: number;
  title?: string;
  description?: string;
  from?: string;
  userLogin?: string;
  userId?: number;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export class Notification implements INotification {
  constructor(
    public id?: number,
    public title?: string,
    public description?: string,
    public from?: string,
    public userLogin?: string,
    public userId?: number,
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date
  ) {}
}
