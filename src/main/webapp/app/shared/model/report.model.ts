export interface IReport {
  salesTotal?: number;
  incomeTotal?: number;
  shippingTotal?: number;
  returnTotal?: number;
  profitTotal?: number;
}

export class Report implements IReport {
  constructor(
    public salesTotal?: number,
    public incomeTotal?: number,
    public shippingTotal?: number,
    public returnTotal?: number,
    public profitTotal?: number
  ) {}
}
