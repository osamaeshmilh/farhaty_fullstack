import { IInvoice } from 'app/shared/model/invoice.model';
import { IExtra } from 'app/shared/model/extra.model';
import { AttendeesType } from 'app/shared/model/enumerations/attendees-type.model';
import { ReservationStatus } from 'app/shared/model/enumerations/reservation-status.model';
import { IAppointment } from 'app/shared/model/appointment.model';
import { Moment } from 'moment';
import { Period } from 'app/shared/model/enumerations/period.model';

export interface IReservation {
  id?: number;
  attendeesType?: AttendeesType;
  reservationStatus?: ReservationStatus;
  period?: Period;
  attendeesNo?: number;
  total?: number;
  roomTotal?: number;
  typeTotal?: number;
  extrasTotal?: number;
  notes?: string;
  customerName?: string;
  customerPhone?: string;
  appointmentId?: number;
  invoices?: IInvoice[];
  userLogin?: string;
  userId?: number;
  extras?: IExtra[];
  typeName?: string;
  typeId?: number;
  reservationDate?: Moment;
  appointment?: IAppointment;
  hallName?: string;
  hallId?: number;
  roomName?: string;
  roomId?: number;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
  userFirstName?: string;
  userPhone?: string;
}

export class Reservation implements IReservation {
  constructor(
    public id?: number,
    public attendeesType?: AttendeesType,
    public reservationStatus?: ReservationStatus,
    public period?: Period,
    public attendeesNo?: number,
    public total?: number,
    public roomTotal?: number,
    public typeTotal?: number,
    public extrasTotal?: number,
    public notes?: string,
    public customerName?: string,
    public customerPhone?: string,
    public appointmentId?: number,
    public invoices?: IInvoice[],
    public userLogin?: string,
    public userId?: number,
    public extras?: IExtra[],
    public typeName?: string,
    public typeId?: number,
    public reservationDate?: Moment,
    public appointment?: IAppointment,
    public hallName?: string,
    public hallId?: number,
    public roomName?: string,
    public roomId?: number,
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date,
    public userFirstName?: string,
    public userPhone?: string
  ) {}
}
