import { IAppointment } from 'app/shared/model/appointment.model';

export interface IRoom {
  id?: number;
  name?: string;
  maxAttendees?: number;
  price?: number;
  active?: boolean;
  appointments?: IAppointment[];
  hallName?: string;
  hallId?: number;
}

export class Room implements IRoom {
  constructor(
    public id?: number,
    public name?: string,
    public maxAttendees?: number,
    public price?: number,
    public active?: boolean,
    public appointments?: IAppointment[],
    public hallName?: string,
    public hallId?: number
  ) {
    this.active = this.active || false;
  }
}
