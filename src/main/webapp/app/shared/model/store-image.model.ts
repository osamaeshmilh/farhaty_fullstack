export interface IStoreImage {
  id?: number;
  title?: string;
  imageContentType?: string;
  image?: any;
  storeName?: string;
  storeId?: number;
}

export class StoreImage implements IStoreImage {
  constructor(
    public id?: number,
    public title?: string,
    public imageContentType?: string,
    public image?: any,
    public storeName?: string,
    public storeId?: number
  ) {}
}
