import { IStoreImage } from 'app/shared/model/store-image.model';

export interface IStore {
  id?: number;
  name?: string;
  description?: string;
  address?: string;
  lat?: number;
  lng?: number;
  logoContentType?: string;
  logo?: any;
  logoUrl?: string;
  email?: string;
  phone?: string;
  facebook?: string;
  whatsup?: string;
  viper?: string;
  twitter?: string;
  instagram?: string;
  snapchat?: string;
  vip?: boolean;
  active?: boolean;
  viewCount?: number;
  userId?: number;
  storeImages?: IStoreImage[];
  categoryName?: string;
  categoryId?: number;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export class Store implements IStore {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public address?: string,
    public lat?: number,
    public lng?: number,
    public logoContentType?: string,
    public logo?: any,
    public logoUrl?: string,
    public email?: string,
    public phone?: string,
    public facebook?: string,
    public whatsup?: string,
    public viper?: string,
    public twitter?: string,
    public instagram?: string,
    public snapchat?: string,
    public vip?: boolean,
    public active?: boolean,
    public viewCount?: number,
    public userId?: number,
    public storeImages?: IStoreImage[],
    public categoryName?: string,
    public categoryId?: number,
    public createdBy?: string,
    public createdDate?: Date,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Date
  ) {
    this.vip = this.vip || false;
  }
}
