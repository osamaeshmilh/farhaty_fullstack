import { IReservation } from 'app/shared/model/reservation.model';

export interface IType {
  id?: number;
  name?: string;
  satPrice?: number;
  sunPrice?: number;
  monPrice?: number;
  tuePrice?: number;
  wedPrice?: number;
  thrPrice?: number;
  friPrice?: number;
  active?: boolean;
  reservations?: IReservation[];
  hallName?: string;
  hallId?: number;
}

export class Type implements IType {
  constructor(
    public id?: number,
    public name?: string,
    public satPrice?: number,
    public sunPrice?: number,
    public monPrice?: number,
    public tuePrice?: number,
    public wedPrice?: number,
    public thrPrice?: number,
    public friPrice?: number,
    public active?: boolean,
    public reservations?: IReservation[],
    public hallName?: string,
    public hallId?: number
  ) {
    this.active = this.active || false;
  }
}
