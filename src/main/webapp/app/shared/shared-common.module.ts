import { NgModule } from '@angular/core';

import { FarhatySharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';
import { EmptyContentComponent } from './empty-content/empty-content.component';

@NgModule({
  imports: [FarhatySharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent, EmptyContentComponent],
  exports: [FarhatySharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent, EmptyContentComponent]
})
export class FarhatySharedCommonModule {}
