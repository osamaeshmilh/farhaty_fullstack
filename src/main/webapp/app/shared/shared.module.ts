import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { FarhatySharedLibsModule, FarhatySharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';
import { JhiSocialComponent } from './social/social.component';
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { TranslateMePipe, GetColorPipe } from './util/mapper.pip';
import { InputDialogComponent } from './input-dialog/alert-dialog.component';
import { TranslateModule } from '@ngx-translate/core';
import { FileUploaderComponent } from 'app/shared/file-uploader/file-uploader.component';

@NgModule({
  imports: [FarhatySharedLibsModule, FarhatySharedCommonModule],
  declarations: [
    JhiLoginModalComponent,
    HasAnyAuthorityDirective,
    JhiSocialComponent,
    AlertDialogComponent,
    InputDialogComponent,
    TranslateMePipe,
    GetColorPipe,
    FileUploaderComponent
  ],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
  entryComponents: [JhiLoginModalComponent, AlertDialogComponent, InputDialogComponent],
  exports: [FarhatySharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective, JhiSocialComponent, TranslateMePipe, GetColorPipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FarhatySharedModule {
  static forRoot() {
    return {
      ngModule: FarhatySharedModule
    };
  }
}
