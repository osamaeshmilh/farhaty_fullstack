import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'translateMe'
})
export class TranslateMePipe implements PipeTransform {
  translations;
  constructor() {
    this.translations = new Map<string, string>();
    this.translations.set('ROLE_ADMIN', 'مدير نظام');
    this.translations.set('ROLE_MANAGER', 'مشرف');
    this.translations.set('ROLE_USER', 'صلاحيات مستخدم');
    this.translations.set('ROLE_HALL', 'صالة أفراح');
    this.translations.set('ROLE_ADD_HALL', 'اضافة صالة أفراح');
    this.translations.set('ROLE_ADD_STORE', 'اضافة متجر');
    this.translations.set('ROLE_STORE', 'متجر');

    this.translations.set('MEN', 'رجال');
    this.translations.set('WOMEN', 'نساء');
    this.translations.set('ALL', 'الكل');

    this.translations.set('FULL_PAYMENT', 'دفع كامل');
    this.translations.set('PARTIAL_PAYMENT', 'دفع من الزبون');
    this.translations.set('RETURN', 'ارجاع الى الزبون');
    this.translations.set('GUARANTEE', 'ضمان');

    this.translations.set('MORNING', 'الفترة الصباحية');
    this.translations.set('EVENING', 'الفترة المسائية');
    this.translations.set('FULL_DAY', 'يوم كامل');

    this.translations.set('PENDING', 'مبدئي');
    this.translations.set('APPROVED', 'مؤكد');
    this.translations.set('DONE', 'منتهي');
    this.translations.set('CANCELED_BY_HALL', 'ملغي');
    this.translations.set('CANCELED_BY_USER', 'ملغي');
  }
  transform(value: any, args?: any): string {
    return this.translations.get(value) || value;
  }
}

@Pipe({
  name: 'getColor'
})
export class GetColorPipe implements PipeTransform {
  colors;
  constructor() {
    this.colors = new Map<string, string>();
    this.colors.set('PENDING', 'accent');
    this.colors.set('APPROVED', 'primary');
    this.colors.set('CANCELED_BY_HALL', 'warn');
    this.colors.set('CANCELED_BY_USER', 'warn');

    this.colors.set('FULL_PAYMENT', 'primary');
    this.colors.set('GUARANTEE', 'primary');
    this.colors.set('PARTIAL_PAYMENT', 'primary');
    this.colors.set('RETURN', 'warn');
  }
  transform(value: any, args?: any): string {
    return this.colors.get(value) || value;
  }
}
